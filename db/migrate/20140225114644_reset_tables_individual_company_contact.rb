class ResetTablesIndividualCompanyContact < ActiveRecord::Migration
  def change
  	drop_table :individuals
  	drop_table :companies
  	drop_table :contacts

  	create_table "contacts" do |t|
      t.string   "name"
      t.text     "about"
      t.string   "subtype",        :null => false
      t.datetime "created_at",     :null => false
      t.datetime "updated_at",     :null => false
      t.string   "contactible"
      t.integer  "contactible_id"
      t.string   "token"
      t.integer  "tenant_id"
    end
    create_table :companies, :inherits => :contact do |t|
      t.string  "company_name",                     :null => false
      t.text    "description",                      :null => false
      t.string  "timezone"
      t.string  "website"
      t.boolean "twenty_four_ops"
      t.string  "company_type"
      t.integer "individuals_count", :default => 0
      t.integer "aircrafts_count",   :default => 0
      t.string  "nationality"
    end
    create_table :individuals, :inherits => :contact do |t|
     t.string  "first_name"
     t.string  "last_name"
     t.text    "description"
     t.string  "designation"
     t.string  "salutation"
     t.integer "company_id"
     t.integer "user_id"
     t.string  "cv"
   end
 end
end
