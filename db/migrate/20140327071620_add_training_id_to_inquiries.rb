class AddTrainingIdToInquiries < ActiveRecord::Migration
  def up
  	add_column :inquiries, :training_id, :integer
	end
	def down
		remove_column :inquiries, :training_id
	end
end
