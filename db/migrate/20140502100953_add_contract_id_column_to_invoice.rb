class AddContractIdColumnToInvoice < ActiveRecord::Migration
  def up
    add_column :invoices, :contract_id, :integer
  end
  def down
    remove_column :invoices, :contract_id
  end
end
