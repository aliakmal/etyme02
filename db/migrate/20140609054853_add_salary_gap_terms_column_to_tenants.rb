class AddSalaryGapTermsColumnToTenants < ActiveRecord::Migration
  def change
    add_column :tenants, :salary_gap_terms, :integer
    add_column :tenants, :salary_payment_day, :integer
  end
end
