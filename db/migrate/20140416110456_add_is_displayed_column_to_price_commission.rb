class AddIsDisplayedColumnToPriceCommission < ActiveRecord::Migration
  def up
  	add_column :prices, :is_price_displayed, :boolean, :default => false
  	add_column :commissions, :is_commission_displayed, :boolean, :default => false
  end
  def down
  	remove_column :prices, :is_price_displayed
  	remove_column :commissions, :is_commission_displayed
  end
end
