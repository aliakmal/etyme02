class AddDueInvoiceDayColumnToTenant < ActiveRecord::Migration
  def up
  	add_column :tenants, :due_invoice_day, :integer
  end
  def down
  	remove_column :tenants, :due_invoice_day
  end
end
