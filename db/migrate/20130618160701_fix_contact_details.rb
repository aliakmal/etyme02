class FixContactDetails < ActiveRecord::Migration
  def change
  	remove_column :contact_details, :contact_id
  	change_table :contact_details do |t|
		t.references :contactable, :polymorphic => true
  	end
  end
end
