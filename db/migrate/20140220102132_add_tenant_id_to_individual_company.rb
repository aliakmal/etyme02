class AddTenantIdToIndividualCompany < ActiveRecord::Migration
  def change
  	  	drop_citier_view(Individual)
  	#create_citier_view(Individual)
  	drop_citier_view(Company)
  	#create_citier_view(Company)
  	remove_column :contacts, :tenant_id
  	add_column :individuals, :tenant_id, :integer
  	add_column :companies, :tenant_id, :integer

  	#drop_citier_view(Individual)
  	create_citier_view(Individual)
  	#drop_citier_view(Company)
  	create_citier_view(Company)
  end
end
