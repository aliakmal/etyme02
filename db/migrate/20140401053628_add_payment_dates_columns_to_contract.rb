class AddPaymentDatesColumnsToContract < ActiveRecord::Migration
  def up
  	add_column :billable_periods, :last_payment_date, :datetime
  	add_column :billable_periods, :due_payment_date, :datetime
  end
  def down
  	remove_column :billable_periods, :last_payment_date
  	remove_column :billable_periods, :due_payment_date
  end
end
