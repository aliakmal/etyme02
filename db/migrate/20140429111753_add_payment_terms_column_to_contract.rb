class AddPaymentTermsColumnToContract < ActiveRecord::Migration
  def up
  	add_column :contracts, :payment_terms, :integer
  end
  def down
  	remove_column :contracts, :payment_terms
  end
end
