class AddEmailColumnToIndividual < ActiveRecord::Migration
  def up
  	add_column :individuals, :ind_email, :string
  	drop_citier_view(Individual)
  	create_citier_view(Individual)
  end
  def down
  	remove_column :individuals, :ind_email
  	drop_citier_view(Individual)
  	create_citier_view(Individual)
  end
end
