class AddSalarytoBillablePeriodFields < ActiveRecord::Migration
  def change
  	add_column :contracts, :employee_type, :string
  	add_column :billable_periods, :salary, :integer

  end
end
