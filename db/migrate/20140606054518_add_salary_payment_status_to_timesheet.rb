class AddSalaryPaymentStatusToTimesheet < ActiveRecord::Migration
  def change
    add_column :timesheets, :salary_payment_status, :string, :default => 'unpaid'
    add_column :invoices, :due_date, :date
    add_column :receipts, :expenses, :float
  end
end
