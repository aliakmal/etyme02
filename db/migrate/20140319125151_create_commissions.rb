class CreateCommissions < ActiveRecord::Migration
  def change
    create_table :commissions do |t|
      t.string :commission_type
      t.belongs_to :user
      t.integer :fixed_commission
      t.float :hourly_commission

      t.timestamps
    end
    add_index :commissions, :user_id
  end
end
