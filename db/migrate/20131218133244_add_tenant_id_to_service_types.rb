class AddTenantIdToServiceTypes < ActiveRecord::Migration
  def change
    add_column :service_types, :tenant_id, :integer
  end
end
