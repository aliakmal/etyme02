class AddCompanyIndividualIdsToPrices < ActiveRecord::Migration
  def up
  	add_column :prices, :company_id, :integer, references: :companies
  	add_column :prices, :individual_id, :integer, references: :individuals
  	add_column :prices, :user_id, :integer, references: :users
  	add_column :prices, :tenant_id, :integer
  end
  def down
  	remove_column :prices, :company_id
  	remove_column :prices, :individual_id
  	remove_column :prices, :user_id
  	remove_column :prices, :tenant_id
  end
end
