class AddTenantIdToCommissions < ActiveRecord::Migration
  def up
  	add_column :commissions, :tenant_id, :integer
  end
  def down
  	remove_column :commissions, :tenant_id
  end
end
