class AddSubsriptionColumnsToUser < ActiveRecord::Migration
  def up
  	add_column :users, :subscription_status, :boolean
  	add_column :users, :subscription_end_date, :datetime
  end
  def down
  	remove_column :users, :subscription_status
  	remove_column :users, :subscription_end_date
  end
end
