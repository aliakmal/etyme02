class ResetBillablePeriodFields < ActiveRecord::Migration
  def change
  	remove_column :billable_periods, :end_date
  	remove_column :contracts, :start_date
  	remove_column :contracts, :end_date
  end
end
