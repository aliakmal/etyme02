class AddSalaryTermsColumnToTenants < ActiveRecord::Migration
  def up
  	add_column :tenants, :salary_terms, :string
  end
  def down
  	remove_column :tenants, :salary_terms
  end
end
