class AddInvoiceDayColumnToContract < ActiveRecord::Migration
  def change
    add_column :contracts, :due_invoice_day, :integer
    remove_column :tenants, :due_invoice_day
  end
end
