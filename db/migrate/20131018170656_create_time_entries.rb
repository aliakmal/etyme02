class CreateTimeEntries < ActiveRecord::Migration
  def change
    #drop_table :time_entries

    create_table :time_entries do |t|
      t.integer :contract_id
      t.string :desc
      t.datetime :start_at
      t.datetime :end_at
      t.integer :candidate_id
      t.integer :total_minutes

      t.timestamps
    end
  end
end
