class CreateTenants < ActiveRecord::Migration
  def change
    create_table :tenants do |t|
      t.references :tenant
      t.string :cname, limit: 80, null: false
      t.string :company, limit: 50
      t.timestamps
    end
    add_index :tenants, :cname
    add_index :tenants, :company
  end
end
