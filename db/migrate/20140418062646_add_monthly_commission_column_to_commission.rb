class AddMonthlyCommissionColumnToCommission < ActiveRecord::Migration
  def up
  	add_column :commissions, :monthly_commission, :float
  end
  def down
  	remove_column :commissions, :monthly_commission
  end
end
