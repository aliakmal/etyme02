class AddAttachMentToTimeSheets < ActiveRecord::Migration
  def change
    add_column :time_entries, :attachment, :string
  end
end
