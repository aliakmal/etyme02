class AddColumnsToCommissionsTimesheets < ActiveRecord::Migration
  def up
  	add_column :commissions, :status, :string, :default => 'unpaid'
  	add_column :commissions, :total_salary_of_employee, :float
  	add_column :commissions, :total_commission, :float
  	add_column :timesheets, :commission_id, :integer
  end
  def down
  	remove_column :commissions, :status
  	remove_column :commissions, :total_salary_of_employee
  	remove_column :commissions, :total_commission
  	remove_column :timesheets, :commission_id
  end
end
