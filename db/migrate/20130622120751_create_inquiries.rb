class CreateInquiries < ActiveRecord::Migration
  def change
    create_table :inquiries do |t|
      t.string :best_time_to_call
      t.string :website
      t.string :current_employer
      t.date :date_available
      t.string :current_pay
      t.string :desired_pay
      t.boolean :can_relocate
      t.string :cv 
      t.references :jobs
      t.references :candidates
      t.timestamps
    end
  end
end
