class Fixtimeentries < ActiveRecord::Migration
  def change
    add_column :time_entries, :timesheet_id, :integer
    remove_column :time_entries, :candidate_id
    remove_column :time_entries, :contract_id
    remove_column :time_entries, :end_at
    remove_column :time_entries, :desc
    remove_column :time_entries, :start_at

  end
end
