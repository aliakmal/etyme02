class CreatePrices < ActiveRecord::Migration
  def change
    create_table :prices do |t|
      t.string :price_type
      t.float :cost_price
      t.float :sale_price
      t.datetime :start_date
      t.datetime :end_date
      t.string :master_services_agreement
      t.string :purchase_orders
      t.string :non_compete_agreement
      t.references :contract

      t.timestamps
    end
    add_index :prices, :contract_id
  end
end
