class AddFieldsToTimeentries < ActiveRecord::Migration
  def change
    add_column :time_entries, :rate, :float
    add_column :time_entries, :salary, :float

  end
end
