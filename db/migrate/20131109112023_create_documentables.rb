class CreateDocumentables < ActiveRecord::Migration
  def change
    create_table :documentables do |t|
      t.integer :documentable_id
      t.string :documentable_type

      t.timestamps
    end
  end
end
