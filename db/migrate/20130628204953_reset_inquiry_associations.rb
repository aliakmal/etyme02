class ResetInquiryAssociations < ActiveRecord::Migration
  def change
  	remove_column :inquiries, :jobs_id

 	add_column :inquiries, :job_id, :integer
  end
end
