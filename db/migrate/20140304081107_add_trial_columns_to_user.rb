class AddTrialColumnsToUser < ActiveRecord::Migration
  def up
  	add_column :users, :trial_period_active, :boolean
  	add_column :users, :trial_period_days, :integer
  end
  def down
  	remove_column :users, :trial_period_active
  	remove_column :users, :trial_period_days
  end
end
