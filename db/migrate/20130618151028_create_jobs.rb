class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :title
      t.string :city
      t.string :state
      t.string :zip
      t.string :salary
      t.date :start_date
      t.string :duration
      t.string :maximum_rate
      t.string :job_type
      t.integer :openings
      t.string :company_job_id

      t.timestamps
    end
  end
end
