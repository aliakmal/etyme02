class AddCompanyIdToJobs < ActiveRecord::Migration
  def up
  	add_column :jobs, :company_id, :integer, references: :companies
  end
  def down
  	remove_column :jobs, :company_id
  end
end
