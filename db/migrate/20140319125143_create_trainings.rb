class CreateTrainings < ActiveRecord::Migration
  def change
    create_table :trainings do |t|
      t.references :user
      t.string :title
      t.text :description
      t.string :duration
      t.string :location
      t.date :start_date
      t.date :end_date
      t.boolean :full_time
      t.timestamps
    end
  end
end
