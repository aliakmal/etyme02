class AddTenantIdToTables < ActiveRecord::Migration
  def change
    add_column :address_details, :tenant_id, :integer
    add_column :billable_periods, :tenant_id, :integer
    add_column :comments, :tenant_id, :integer
    add_column :contacts, :tenant_id, :integer
    add_column :contact_details, :tenant_id, :integer
    add_column :contracts, :tenant_id, :integer
    add_column :documents, :tenant_id, :integer
    add_column :documentables, :tenant_id, :integer
    #add_column :emailers, :tenant_id, :integer
    add_column :inquiries, :tenant_id, :integer
    add_column :invoices, :tenant_id, :integer
    add_column :jobs, :tenant_id, :integer
    #add_column :job_service_types, :tenant_id, :integer
    add_column :time_entries, :tenant_id, :integer
    add_column :timesheets, :tenant_id, :integer
    add_column :roles, :tenant_id, :integer
  end
end
