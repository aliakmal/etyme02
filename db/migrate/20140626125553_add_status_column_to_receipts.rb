class AddStatusColumnToReceipts < ActiveRecord::Migration
  def change
    add_column :receipts, :status, :string, :default => 'unpaid'
  end
end
