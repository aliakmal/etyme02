class CreateInvoices < ActiveRecord::Migration
  def change
    create_table :invoices do |t|
      t.string :ref
      t.string :state
      t.float :total

      t.timestamps
    end
  end
end
