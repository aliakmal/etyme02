class UpdateSubdomainForTenants < ActiveRecord::Migration
  def up
    Tenant.all.each do |t|
      t.update_attributes(subdomain: t.company.parameterize)
    end
  end

  def down
    raise ActiveRecord::IrreversibleMigration
  end
end
