class AddTypeColumnToPrice < ActiveRecord::Migration
  def up
    add_column :prices, :type, :string
  end

  def down
    remove_column :prices, :type
  end
end
