class RemoveTrainingIdFromContracts < ActiveRecord::Migration
  def up
  	remove_column :contracts, :training_id
  end

  def down
  	add_column :contracts, :training_id, :integer
  end
end
