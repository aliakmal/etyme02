class AddInvoiceIdToTimesheet < ActiveRecord::Migration
  def change
    add_column :timesheets, :invoice_id, :integer
  end
end
