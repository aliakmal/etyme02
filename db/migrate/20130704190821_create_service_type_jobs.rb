class CreateServiceTypeJobs < ActiveRecord::Migration
  def change
    create_table :jobs_service_types do |t|
      t.belongs_to :service_type
      t.belongs_to :job
      t.timestamps
    end
  end
end
