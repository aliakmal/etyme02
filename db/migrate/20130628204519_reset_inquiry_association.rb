class ResetInquiryAssociation < ActiveRecord::Migration
  def change
  	remove_column :inquiries, :individuals_id

 	add_column :inquiries, :individual_id, :integer
  end
end
