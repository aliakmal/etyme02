class ResetCitierViewIndComp < ActiveRecord::Migration
  def change
  	drop_citier_view(Individual)
  	create_citier_view(Individual)
  	drop_citier_view(Company)
  	create_citier_view(Company)
  end
end
