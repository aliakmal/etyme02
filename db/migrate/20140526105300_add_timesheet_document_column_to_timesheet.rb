class AddTimesheetDocumentColumnToTimesheet < ActiveRecord::Migration
  def change
    add_column :timesheets, :timesheet_document, :string
  end
end
