class AddContractIdToCommissions < ActiveRecord::Migration
	def up
		add_column :commissions, :contract_id, :integer, references: :contracts
	end
	def down
		remove_column :commissions, :contract_id
	end
end
