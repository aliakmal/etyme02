class AddTenantIdToContacts < ActiveRecord::Migration
  def change
  	add_column :contacts, :tenant_id, :integer
  end
end
