class AddStateToTimeSheets < ActiveRecord::Migration
  def change
    add_column :time_entries, :state, :string, :default => 'unapproved'

  end
end
