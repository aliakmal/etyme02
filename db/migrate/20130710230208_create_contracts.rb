class CreateContracts < ActiveRecord::Migration
  def change
    create_table :contracts do |t|
      t.date :start_date
      t.date :end_date
      t.integer :candidate_id
      t.integer :job_id

      t.timestamps
    end
  end
end
