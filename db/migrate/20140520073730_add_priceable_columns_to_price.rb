class AddPriceableColumnsToPrice < ActiveRecord::Migration
  def up
    add_column :prices, :priceable_id, :integer
    add_column :prices, :priceable_type, :string
  end

  def down
    remove_column :prices, :priceable_id
    remove_column :prices, :priceable_type
  end
end
