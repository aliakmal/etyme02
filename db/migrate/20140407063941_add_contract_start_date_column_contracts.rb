class AddContractStartDateColumnContracts < ActiveRecord::Migration
  def up
  	add_column :contracts, :contract_start_date, :datetime
  end

  def down
  	remove_column :contracts, :contract_start_date
  end
end
