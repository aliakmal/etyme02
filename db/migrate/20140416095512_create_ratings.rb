class CreateRatings < ActiveRecord::Migration
  def self.up
    create_table :ratings do |t|
      t.integer :stars, :default => 0
      t.references :training
      t.references :user
    end
  end
  def self.down
    drop_table :ratings
  end
end
