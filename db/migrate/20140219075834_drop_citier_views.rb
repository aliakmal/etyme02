class DropCitierViews < ActiveRecord::Migration
  def change
  	drop_citier_view(Individual)
  	drop_citier_view(Company)
  end
end
