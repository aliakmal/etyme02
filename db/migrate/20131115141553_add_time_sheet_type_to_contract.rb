class AddTimeSheetTypeToContract < ActiveRecord::Migration
  def change
    add_column :contracts, :timesheet_type, :string
  end
end
