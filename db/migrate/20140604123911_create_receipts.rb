class CreateReceipts < ActiveRecord::Migration
  def change
    create_table :receipts do |t|
      t.float :total_salary
      t.integer :total_time
      t.references :contract
      t.integer :tenant_id
      t.float :profit
      t.float :rate

      t.timestamps
    end
    add_index :receipts, :contract_id
  end
end
