class CreateTimesheets < ActiveRecord::Migration
  def change
    create_table :timesheets do |t|
      t.integer :contract_id
      t.string :type
      t.date :starts
      t.date :ends
      t.integer :minutes
      t.string :state

      t.timestamps
    end
  end
end
