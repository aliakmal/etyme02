class NewResetCitier < ActiveRecord::Migration
  def up
  	drop_citier_view(Individual)
  	create_citier_view(Individual)
  	drop_citier_view(Company)
  	create_citier_view(Company)
  end

  def down
  	create_citier_view(Individual)
  	drop_citier_view(Individual)
  	create_citier_view(Company)
  	drop_citier_view(Company)
  end
end
