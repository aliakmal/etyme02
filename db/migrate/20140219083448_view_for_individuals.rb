class ViewForIndividuals < ActiveRecord::Migration
  def up
  	create_view :view_individuals, "SELECT contacts.id, contacts.name, contacts.about, contacts.type, contacts.created_at, contacts.updated_at, contacts.contactible, contacts.contactible_id, contacts.token, contacts.tenant_id, individuals.first_name, individuals.last_name, individuals.description, individuals.designation, individuals.salutation, individuals.company_id, individuals.user_id, individuals.cv FROM contacts, individuals WHERE (contacts.id = individuals.id)" do |v|
    v.column :id
    v.column :name
    v.column :about
    v.column :type
    v.column :created_at
    v.column :updated_at
    v.column :contactible
    v.column :contactible_id
    v.column :token
    v.column :tenant_id
    v.column :first_name
    v.column :last_name
    v.column :description
    v.column :designation
    v.column :salutation
    v.column :company_id
    v.column :user_id
    v.column :cv
  end
  end

  def down
  	drop_view :view_individuals
  end
end
