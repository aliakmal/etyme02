class ViewForCompanies < ActiveRecord::Migration
	def up
		create_view :view_companies, "SELECT contacts.id, contacts.name, contacts.about, contacts.type, contacts.created_at, contacts.updated_at, contacts.contactible, contacts.contactible_id, contacts.token, contacts.tenant_id, companies.company_name, companies.description, companies.timezone, companies.website, companies.twenty_four_ops, companies.company_type, companies.individuals_count, companies.aircrafts_count, companies.nationality FROM contacts, companies WHERE (contacts.id = companies.id)" do |v|
			v.column :id
			v.column :name
			v.column :about
			v.column :type
			v.column :created_at
			v.column :updated_at
			v.column :contactible
			v.column :contactible_id
			v.column :token
			v.column :tenant_id
			v.column :company_name
			v.column :description
			v.column :timezone
			v.column :website
			v.column :twenty_four_ops
			v.column :company_type
			v.column :individuals_count
			v.column :aircrafts_count
			v.column :nationality
		end
	end

	def down
		drop_view :view_companies
	end
end
