class AddIndividualIdToCommissions < ActiveRecord::Migration
	def up
		add_column :commissions, :individual_id, :integer, references: :individuals
	end
	def down
		remove_column :commissions, :individual_id
	end
end
