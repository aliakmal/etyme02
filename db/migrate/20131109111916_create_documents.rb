class CreateDocuments < ActiveRecord::Migration
  def change
    create_table :documents do |t|
      t.string :title
      t.string :filename
      t.text :desc
      t.date :issue_date
      t.date :expiry_date
      t.integer :filesize
      t.text :file

      t.timestamps
    end
  end
end
