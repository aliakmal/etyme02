class SetCitierForCandidates < ActiveRecord::Migration
  def change
#  	create_citier_view(Candidate)
    add_column :individuals, :user_id, :integer

    remove_column :inquiries, :candidates_id

    change_table :inquiries do |t|
      t.references :individuals
    end

  	drop_citier_view(Individual)
  	create_citier_view(Individual)

  	drop_table :candidates 

  end
end
