class AddStateToInquiries < ActiveRecord::Migration
  def up
  	add_column :inquiries, :state, :string
  end

  def down
  	remove_column :inquiries, :state
  end
end
