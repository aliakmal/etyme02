class AddDocumentIdToDocumentable < ActiveRecord::Migration
  def change
    add_column :documentables, :document_id, :integer
  end
end
