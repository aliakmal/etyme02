class AddColumnsToReceipt < ActiveRecord::Migration
  def change
    add_column :receipts, :salary_pending, :float
    add_column :receipts, :payment_amount, :float
    add_column :receipts, :non_contract_expenses, :float
  end
end
