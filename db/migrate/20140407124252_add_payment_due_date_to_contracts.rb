class AddPaymentDueDateToContracts < ActiveRecord::Migration
  def up
  	add_column :contracts, :last_payment_date, :datetime
  	add_column :contracts, :due_payment_date, :datetime
  end
  def down
  	remove_column :contracts, :last_payment_date
  	remove_column :contracts, :due_payment_date
  end
end
