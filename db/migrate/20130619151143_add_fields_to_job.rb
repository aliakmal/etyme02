class AddFieldsToJob < ActiveRecord::Migration
  def change
  	add_column :jobs, :recruiter_id, :integer, references: :users
  	add_column :jobs, :owner_id, :integer, references: :users
  	add_column :jobs, :description, :text
  	add_column :jobs, :status, :string

  end
end
