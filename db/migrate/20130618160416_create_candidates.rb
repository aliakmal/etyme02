class CreateCandidates < ActiveRecord::Migration
  def change
    create_table :candidates do |t|
      t.string :first_name
      t.string :last_name
      t.string :designation
      t.string :best_time_to_call
      t.string :website
      t.string :current_employer
      t.date :date_available
      t.string :current_pay
      t.string :desired_pay
      t.boolean :can_relocate

      t.timestamps
    end
  end
end
