class CreateAttachments < ActiveRecord::Migration
  def change
    create_table :attachments do |t|
      t.string :name
      t.string :attachment
      t.references :contract
      t.integer :tenant_id

      t.timestamps
    end
    add_index :attachments, :contract_id
  end
end
