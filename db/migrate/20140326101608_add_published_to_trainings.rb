class AddPublishedToTrainings < ActiveRecord::Migration
  def up
  	add_column :trainings, :published, :boolean
	end
	def down
		remove_column :trainings, :published
	end
end
