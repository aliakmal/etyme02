class AddTenantIdToUser < ActiveRecord::Migration
  def change
    add_column :users, :tenant_id, :integer
    add_column :roles, :tenant_id, :integer
  end
end
