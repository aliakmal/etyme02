class CreateBillablePeriods < ActiveRecord::Migration
  def change
    create_table :billable_periods do |t|
      t.datetime :start_date
      t.datetime :end_date
      t.float :rate
      t.integer :contract_id

      t.timestamps
    end
  end
end
