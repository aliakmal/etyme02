class AddTrainingIdToContracts < ActiveRecord::Migration
  def up
  	add_column :contracts, :training_id, :integer
	end
	def down
		remove_column :contracts, :training_id
	end
end
