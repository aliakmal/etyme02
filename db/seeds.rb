# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
# Environment variables (ENV['...']) are set in the file config/application.yml.
# See http://railsapps.github.com/rails-environment-variables.html

  
# puts 'ROLES'
# YAML.load(ENV['ROLES']).each do |role|
#   Role.find_or_create_by_name({ :name => role }, :without_protection => true)
#   puts 'role: ' << role
# end
# puts 'DEFAULT USERS'
# user = User.find_or_create_by_email :name=>ENV['ADMIN_NAME'].dup, :username => ENV['ADMIN_USERNAME'].dup, :email => ENV['ADMIN_EMAIL'].dup, :password => ENV['ADMIN_PASSWORD'].dup, :password_confirmation => ENV['ADMIN_PASSWORD'].dup
# individual = user.build_individual(:last_name=>'User', :first_name=>ENV['ADMIN_NAME'].dup)
# individual.save
# pp individual

# puts user.name
# user.add_role :admin
=begin
Request.all.each do |rq|
  rq.update_attribute :customer_name_cache, rq.customer_name
  rq.update_attribute :operator_name_cache, rq.operator_name
  rq.update_attribute :aircraft_reg_cache, rq.aircraft_reg
end
# duplicate ovf templates and change headers to urgent and assign to urgent ovf services
ovfs = ServiceType.where('service_types.desc like "%Overflight Permit"')
urgent_ovfs = ServiceType.where('service_types.desc like "%Urgent Overflight%"')

ovfs.zip(urgent_ovfs).each do |one_ovf, one_urgent_ovf|
	if !one_ovf.default_template_id.blank? then
		dplicate = one_ovf.default_template.duplicate
		dplicate.update_attribute :title, 'Urgent ' + one_ovf.default_template.title
		one_urgent_ovf.update_attribute :default_template_id, dplicate.id
	end
end


#VendorRequest.update_all(:last_emailed=>1.year.ago.to_date)
ServiceType.all.each do |st|
	tm = Template.where('? LIKE CONCAT(\'%\', templates.default_for_service_type) AND default_for_service_type <> \'\' ', st.desc.to_s).first()
	if !tm.blank? then
		st.update_attribute :fallback_template_id, tm.id.to_i
	end
end
=begin

# go through all the templates

Request.delete_all
ServiceType.delete_all
ServiceType.create(:account_ref=>"OA OVF",:desc=>"Afghanistan Overflight Permit Charge")
=end