# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended to check this file into your version control system.

ActiveRecord::Schema.define(:version => 20140626125553) do

  create_table "activities", :force => true do |t|
    t.integer  "trackable_id"
    t.string   "trackable_type"
    t.integer  "owner_id"
    t.string   "owner_type"
    t.string   "key"
    t.text     "parameters"
    t.integer  "recipient_id"
    t.string   "recipient_type"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
  end

  add_index "activities", ["owner_id", "owner_type"], :name => "index_activities_on_owner_id_and_owner_type"
  add_index "activities", ["recipient_id", "recipient_type"], :name => "index_activities_on_recipient_id_and_recipient_type"
  add_index "activities", ["trackable_id", "trackable_type"], :name => "index_activities_on_trackable_id_and_trackable_type"

  create_table "address_details", :force => true do |t|
    t.text     "address"
    t.string   "city"
    t.string   "state"
    t.string   "country"
    t.string   "zip"
    t.integer  "address_id"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
    t.integer  "tenant_id"
  end

  create_table "aircrafts", :force => true do |t|
    t.string   "ac_reg"
    t.string   "ac_type"
    t.string   "mtow"
    t.integer  "operator_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  create_table "attachments", :force => true do |t|
    t.string   "name"
    t.string   "attachment"
    t.integer  "contract_id"
    t.integer  "tenant_id"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
  end

  add_index "attachments", ["contract_id"], :name => "index_attachments_on_contract_id"

  create_table "audits", :force => true do |t|
    t.integer  "auditable_id"
    t.string   "auditable_type"
    t.integer  "associated_id"
    t.string   "associated_type"
    t.integer  "user_id"
    t.string   "user_type"
    t.string   "username"
    t.string   "action"
    t.text     "audited_changes"
    t.integer  "version",         :default => 0
    t.string   "comment"
    t.string   "remote_address"
    t.datetime "created_at"
  end

  add_index "audits", ["associated_id", "associated_type"], :name => "associated_index"
  add_index "audits", ["auditable_id", "auditable_type"], :name => "auditable_index"
  add_index "audits", ["created_at"], :name => "index_audits_on_created_at"
  add_index "audits", ["user_id", "user_type"], :name => "user_index"

  create_table "billable_periods", :force => true do |t|
    t.datetime "start_date"
    t.float    "rate"
    t.integer  "contract_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "salary"
    t.integer  "tenant_id"
    t.datetime "last_payment_date"
    t.datetime "due_payment_date"
  end

  create_table "bootsy_image_galleries", :force => true do |t|
    t.integer  "bootsy_resource_id"
    t.string   "bootsy_resource_type"
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
  end

  create_table "bootsy_images", :force => true do |t|
    t.string   "image_file"
    t.integer  "image_gallery_id"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
  end

  create_table "comments", :force => true do |t|
    t.string   "title",            :limit => 50, :default => ""
    t.text     "comment"
    t.integer  "commentable_id"
    t.string   "commentable_type"
    t.integer  "user_id"
    t.datetime "created_at",                                         :null => false
    t.datetime "updated_at",                                         :null => false
    t.string   "visible_to",                     :default => "team"
    t.integer  "tenant_id"
  end

  add_index "comments", ["commentable_id"], :name => "index_comments_on_commentable_id"
  add_index "comments", ["commentable_type"], :name => "index_comments_on_commentable_type"
  add_index "comments", ["user_id"], :name => "index_comments_on_user_id"

  create_table "commissions", :force => true do |t|
    t.string   "commission_type"
    t.integer  "user_id"
    t.integer  "fixed_commission"
    t.float    "hourly_commission"
    t.datetime "created_at",                                     :null => false
    t.datetime "updated_at",                                     :null => false
    t.integer  "contract_id"
    t.integer  "individual_id"
    t.integer  "tenant_id"
    t.string   "status",                   :default => "unpaid"
    t.float    "total_salary_of_employee"
    t.float    "total_commission"
    t.boolean  "is_commission_displayed",  :default => false
    t.float    "monthly_commission"
  end

  add_index "commissions", ["user_id"], :name => "index_commissions_on_user_id"

  create_table "companies", :force => true do |t|
    t.string  "company_name",                     :null => false
    t.text    "description",                      :null => false
    t.string  "timezone"
    t.string  "website"
    t.boolean "twenty_four_ops"
    t.string  "company_type"
    t.integer "individuals_count", :default => 0
    t.integer "aircrafts_count",   :default => 0
    t.string  "nationality"
  end

  create_table "contact_details", :force => true do |t|
    t.string   "type"
    t.string   "location"
    t.string   "details"
    t.datetime "created_at",       :null => false
    t.datetime "updated_at",       :null => false
    t.integer  "contactable_id"
    t.string   "contactable_type"
    t.integer  "tenant_id"
  end

  create_table "contacts", :force => true do |t|
    t.string   "name"
    t.text     "about"
    t.string   "type",           :null => false
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.string   "contactible"
    t.integer  "contactible_id"
    t.string   "token"
    t.integer  "tenant_id"
  end

  create_table "contacts_service_types", :force => true do |t|
    t.integer  "contact_id"
    t.integer  "service_type_id"
    t.integer  "template_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "contacts_service_types", ["template_id"], :name => "index_contacts_service_types_on_template_id"

  create_table "contracts", :force => true do |t|
    t.integer  "candidate_id"
    t.integer  "job_id"
    t.datetime "created_at",                                :null => false
    t.datetime "updated_at",                                :null => false
    t.string   "employee_type"
    t.string   "timesheet_type"
    t.integer  "tenant_id"
    t.datetime "contract_start_date"
    t.datetime "last_payment_date"
    t.datetime "due_payment_date"
    t.integer  "payment_terms"
    t.integer  "due_invoice_day"
    t.string   "status",              :default => "Active"
  end

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "delayed_jobs", :force => true do |t|
    t.integer  "priority",   :default => 0, :null => false
    t.integer  "attempts",   :default => 0, :null => false
    t.text     "handler",                   :null => false
    t.text     "last_error"
    t.datetime "run_at"
    t.datetime "locked_at"
    t.datetime "failed_at"
    t.string   "locked_by"
    t.string   "queue"
    t.datetime "created_at",                :null => false
    t.datetime "updated_at",                :null => false
  end

  add_index "delayed_jobs", ["priority", "run_at"], :name => "delayed_jobs_priority"

  create_table "documentables", :force => true do |t|
    t.integer  "documentable_id"
    t.string   "documentable_type"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "document_id"
    t.integer  "tenant_id"
  end

  create_table "documents", :force => true do |t|
    t.string   "title"
    t.string   "filename"
    t.text     "desc"
    t.date     "issue_date"
    t.date     "expiry_date"
    t.integer  "filesize"
    t.text     "file"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "tenant_id"
  end

  create_table "individuals", :force => true do |t|
    t.string  "first_name"
    t.string  "last_name"
    t.text    "description"
    t.string  "designation"
    t.string  "salutation"
    t.integer "company_id"
    t.integer "user_id"
    t.string  "cv"
    t.string  "ind_email"
  end

  create_table "inquiries", :force => true do |t|
    t.string   "best_time_to_call"
    t.string   "website"
    t.string   "current_employer"
    t.date     "date_available"
    t.string   "current_pay"
    t.string   "desired_pay"
    t.boolean  "can_relocate"
    t.string   "cv"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.integer  "individual_id"
    t.integer  "job_id"
    t.integer  "tenant_id"
    t.integer  "training_id"
    t.string   "state"
  end

  create_table "invoices", :force => true do |t|
    t.string   "ref"
    t.string   "state"
    t.float    "total"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.float    "total_rate"
    t.float    "total_salary"
    t.integer  "tenant_id"
    t.integer  "contract_id"
    t.date     "due_date"
  end

  create_table "jobs", :force => true do |t|
    t.string   "title"
    t.string   "city"
    t.string   "state"
    t.string   "zip"
    t.string   "salary"
    t.date     "start_date"
    t.string   "duration"
    t.string   "maximum_rate"
    t.string   "job_type"
    t.integer  "openings"
    t.string   "company_job_id"
    t.datetime "created_at",     :null => false
    t.datetime "updated_at",     :null => false
    t.integer  "recruiter_id"
    t.integer  "owner_id"
    t.text     "description"
    t.string   "status"
    t.boolean  "published"
    t.integer  "tenant_id"
    t.integer  "company_id"
  end

  create_table "jobs_service_types", :force => true do |t|
    t.integer  "service_type_id"
    t.integer  "job_id"
    t.datetime "created_at",      :null => false
    t.datetime "updated_at",      :null => false
  end

  create_table "operators", :force => true do |t|
    t.string "operator_name"
  end

  create_table "prices", :force => true do |t|
    t.string   "price_type"
    t.float    "cost_price"
    t.float    "sale_price"
    t.datetime "start_date"
    t.datetime "end_date"
    t.string   "master_services_agreement"
    t.string   "purchase_orders"
    t.string   "non_compete_agreement"
    t.integer  "contract_id"
    t.datetime "created_at",                                   :null => false
    t.datetime "updated_at",                                   :null => false
    t.integer  "company_id"
    t.integer  "individual_id"
    t.integer  "user_id"
    t.integer  "tenant_id"
    t.boolean  "is_price_displayed",        :default => false
    t.string   "type"
    t.integer  "priceable_id"
    t.string   "priceable_type"
  end

  add_index "prices", ["contract_id"], :name => "index_prices_on_contract_id"

  create_table "ratings", :force => true do |t|
    t.integer "stars",       :default => 0
    t.integer "training_id"
    t.integer "user_id"
  end

  create_table "receipts", :force => true do |t|
    t.float    "total_salary"
    t.integer  "total_time"
    t.integer  "contract_id"
    t.integer  "tenant_id"
    t.float    "profit"
    t.float    "rate"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.float    "expenses"
    t.float    "salary_pending"
    t.float    "payment_amount"
    t.float    "non_contract_expenses"
    t.string   "status",                :default => "unpaid"
  end

  add_index "receipts", ["contract_id"], :name => "index_receipts_on_contract_id"

  create_table "requests", :force => true do |t|
    t.string   "ref"
    t.integer  "customer_id"
    t.integer  "operator_id"
    t.integer  "aircraft_id"
    t.text     "schedule"
    t.string   "request_status"
    t.datetime "created_at",                           :null => false
    t.datetime "updated_at",                           :null => false
    t.datetime "date_of_request"
    t.datetime "date_due"
    t.string   "is_substitute"
    t.text     "original_request"
    t.datetime "customer_updated_at"
    t.integer  "counter_all_services",  :default => 0
    t.integer  "counter_done_services", :default => 0
    t.string   "customer_name_cache"
    t.string   "operator_name_cache"
    t.string   "aircraft_reg_cache"
  end

  create_table "roles", :force => true do |t|
    t.string   "name"
    t.integer  "resource_id"
    t.string   "resource_type"
    t.datetime "created_at",    :null => false
    t.datetime "updated_at",    :null => false
    t.integer  "tenant_id"
  end

  add_index "roles", ["name", "resource_type", "resource_id"], :name => "index_roles_on_name_and_resource_type_and_resource_id"
  add_index "roles", ["name"], :name => "index_roles_on_name"

  create_table "sectors", :force => true do |t|
    t.string   "desc"
    t.integer  "request_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.date     "date_on"
    t.string   "flt_no"
    t.string   "consignor"
    t.string   "consignee"
    t.string   "flt_load"
    t.string   "flt_purpose"
    t.string   "flt_route"
    t.string   "arrival"
    t.string   "departure"
    t.string   "eta"
    t.string   "etd"
    t.string   "flt_crew_concise"
    t.text     "flt_crew_detailed"
    t.string   "flt_total_pax"
    t.string   "lead_pax"
    t.text     "flt_plan"
    t.string   "entry_point"
    t.string   "entry_time"
    t.string   "exit_point"
    t.string   "exit_time"
  end

  create_table "service_types", :force => true do |t|
    t.datetime "created_at",           :null => false
    t.datetime "updated_at",           :null => false
    t.string   "account_ref"
    t.string   "desc"
    t.integer  "prefered_contact_id"
    t.integer  "default_template_id"
    t.integer  "fallback_template_id"
    t.integer  "tenant_id"
  end

  create_table "services", :force => true do |t|
    t.integer  "service_type_id"
    t.string   "location"
    t.string   "service_status"
    t.string   "notice"
    t.text     "notes"
    t.integer  "sector_id"
    t.datetime "created_at",        :null => false
    t.datetime "updated_at",        :null => false
    t.text     "details"
    t.integer  "chosen_vendor_id"
    t.text     "request_body"
    t.integer  "vendor_request_id"
  end

  create_table "sessions", :force => true do |t|
    t.string   "session_id", :null => false
    t.text     "data"
    t.datetime "created_at", :null => false
    t.datetime "updated_at", :null => false
  end

  add_index "sessions", ["session_id"], :name => "index_sessions_on_session_id"
  add_index "sessions", ["updated_at"], :name => "index_sessions_on_updated_at"

  create_table "taggings", :force => true do |t|
    t.integer  "tag_id"
    t.integer  "taggable_id"
    t.string   "taggable_type"
    t.integer  "tagger_id"
    t.string   "tagger_type"
    t.string   "context"
    t.datetime "created_at"
  end

  add_index "taggings", ["tag_id"], :name => "index_taggings_on_tag_id"
  add_index "taggings", ["taggable_id", "taggable_type", "context"], :name => "index_taggings_on_taggable_id_and_taggable_type_and_context"

  create_table "tags", :force => true do |t|
    t.string "name"
  end

  create_table "templates", :force => true do |t|
    t.string   "title"
    t.text     "details"
    t.datetime "created_at",               :null => false
    t.datetime "updated_at",               :null => false
    t.string   "default_for_service_type"
  end

  create_table "tenants", :force => true do |t|
    t.integer  "tenant_id"
    t.string   "cname",              :limit => 80, :null => false
    t.string   "company",            :limit => 50
    t.datetime "created_at",                       :null => false
    t.datetime "updated_at",                       :null => false
    t.string   "subdomain"
    t.string   "salary_terms"
    t.integer  "salary_gap_terms"
    t.integer  "salary_payment_day"
  end

  add_index "tenants", ["cname"], :name => "index_tenants_on_cname"
  add_index "tenants", ["company"], :name => "index_tenants_on_company"

  create_table "tenants_users", :id => false, :force => true do |t|
    t.integer "tenant_id"
    t.integer "user_id"
  end

  add_index "tenants_users", ["tenant_id"], :name => "index_tenants_users_on_tenant_id"
  add_index "tenants_users", ["user_id"], :name => "index_tenants_users_on_user_id"

  create_table "time_entries", :force => true do |t|
    t.integer  "total_minutes"
    t.datetime "created_at",                              :null => false
    t.datetime "updated_at",                              :null => false
    t.string   "attachment"
    t.string   "state",         :default => "unapproved"
    t.integer  "timesheet_id"
    t.date     "for_date"
    t.float    "rate"
    t.float    "salary"
    t.integer  "tenant_id"
  end

  create_table "timesheets", :force => true do |t|
    t.integer  "contract_id"
    t.date     "starts"
    t.date     "ends"
    t.integer  "minutes"
    t.string   "state"
    t.datetime "created_at",                                  :null => false
    t.datetime "updated_at",                                  :null => false
    t.string   "attachment"
    t.integer  "invoice_id"
    t.integer  "tenant_id"
    t.integer  "commission_id"
    t.string   "type"
    t.string   "timesheet_document"
    t.string   "salary_payment_status", :default => "unpaid"
  end

  create_table "trainings", :force => true do |t|
    t.integer  "user_id"
    t.string   "title"
    t.text     "description"
    t.string   "duration"
    t.string   "location"
    t.date     "start_date"
    t.date     "end_date"
    t.boolean  "full_time"
    t.datetime "created_at",  :null => false
    t.datetime "updated_at",  :null => false
    t.integer  "tenant_id"
    t.boolean  "published"
    t.string   "status"
  end

  create_table "users", :force => true do |t|
    t.string   "email",                  :default => "", :null => false
    t.string   "encrypted_password",     :default => "", :null => false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          :default => 0
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.string   "current_sign_in_ip"
    t.string   "last_sign_in_ip"
    t.datetime "created_at",                             :null => false
    t.datetime "updated_at",                             :null => false
    t.string   "name"
    t.string   "username"
    t.integer  "tenant_id"
    t.string   "stripe_customer_token"
    t.boolean  "trial_period_active"
    t.integer  "trial_period_days"
    t.boolean  "subscription_status"
    t.datetime "subscription_end_date"
    t.string   "confirmation_token"
    t.datetime "confirmed_at"
    t.datetime "confirmation_sent_at"
    t.string   "unconfirmed_email"
    t.string   "invitation_token"
    t.datetime "invitation_created_at"
    t.datetime "invitation_sent_at"
    t.datetime "invitation_accepted_at"
    t.integer  "invitation_limit"
    t.integer  "invited_by_id"
    t.string   "invited_by_type"
  end

  add_index "users", ["confirmation_token"], :name => "index_users_on_confirmation_token", :unique => true
  add_index "users", ["email"], :name => "index_users_on_email", :unique => true
  add_index "users", ["invitation_token"], :name => "index_users_on_invitation_token", :unique => true
  add_index "users", ["reset_password_token"], :name => "index_users_on_reset_password_token", :unique => true

  create_table "users_roles", :id => false, :force => true do |t|
    t.integer "user_id"
    t.integer "role_id"
  end

  add_index "users_roles", ["user_id", "role_id"], :name => "index_users_roles_on_user_id_and_role_id"

  create_table "vendor_requests", :force => true do |t|
    t.text     "body"
    t.datetime "created_at",   :null => false
    t.datetime "updated_at",   :null => false
    t.datetime "last_emailed"
    t.string   "title"
  end

  create_view "view_companies", "SELECT contacts.id, contacts.name, contacts.about, contacts.type, contacts.created_at, contacts.updated_at, contacts.contactible, contacts.contactible_id, contacts.token, contacts.tenant_id, companies.company_name, companies.description, companies.timezone, companies.website, companies.twenty_four_ops, companies.company_type, companies.individuals_count, companies.aircrafts_count, companies.nationality FROM contacts, companies WHERE (contacts.id = companies.id);", :force => true do |v|
    v.column :id
    v.column :name
    v.column :about
    v.column :type
    v.column :created_at
    v.column :updated_at
    v.column :contactible
    v.column :contactible_id
    v.column :token
    v.column :tenant_id
    v.column :company_name
    v.column :description
    v.column :timezone
    v.column :website
    v.column :twenty_four_ops
    v.column :company_type
    v.column :individuals_count
    v.column :aircrafts_count
    v.column :nationality
  end

  create_view "view_individuals", "SELECT contacts.id, contacts.name, contacts.about, contacts.type, contacts.created_at, contacts.updated_at, contacts.contactible, contacts.contactible_id, contacts.token, contacts.tenant_id, individuals.first_name, individuals.last_name, individuals.description, individuals.designation, individuals.salutation, individuals.company_id, individuals.user_id, individuals.cv, individuals.ind_email FROM contacts, individuals WHERE (contacts.id = individuals.id);", :force => true do |v|
    v.column :id
    v.column :name
    v.column :about
    v.column :type
    v.column :created_at
    v.column :updated_at
    v.column :contactible
    v.column :contactible_id
    v.column :token
    v.column :tenant_id
    v.column :first_name
    v.column :last_name
    v.column :description
    v.column :designation
    v.column :salutation
    v.column :company_id
    v.column :user_id
    v.column :cv
    v.column :ind_email
  end

end
