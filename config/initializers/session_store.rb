# Be sure to restart your server when you modify this file.
if Rails.env.development?
  Etyme::Application.config.session_store :cookie_store, key: '_etyme_session', domain: '.lvh.me'
else
  Etyme::Application.config.session_store :cookie_store, key: '_etyme_session', domain: '.etyme.com'
end
# Use the database for sessions instead of the cookie-based default,
# which shouldn't be used to store highly confidential information
# (create the session table with "rails generate session_migration")
# Etyme::Application.config.session_store :active_record_store
