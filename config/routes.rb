Etyme::Application.routes.draw do

  resources :messages

  mount Bootsy::Engine => '/bootsy', as: 'bootsy'

  get "payment/index"
  post "payment/processing"

  constraints(Subdomain) do
    match "/invoices/auto_invoicing" => "invoices#auto_invoicing", :as=>:auto_invoicing, :via=>:get
    match "/invoices/clear_invoices" => "invoices#clear_invoices", :as=>:clear_invoices, :via=>:get
    resources :invoices
    resources :receipts

    match "/timesheets/contract" => "timesheets#contract", :as=>:contract_timesheets, :via=>:get
    match "/timesheets/my_timesheets" =>"timesheets#my_timesheets", :as=>:my_timesheets, :via=>:get

    match "/timesheets/:id/unapproved_to_pending" => "timesheets#unapproved_to_pending", :as=>:unapproved_to_pending_timesheet, :via=>:get
    match "/timesheets/:id/pending_to_approved" => "timesheets#pending_to_approved", :as=>:pending_to_approved_timesheet, :via=>:get
    match "/timesheets/:id/approved_to_unapproved" => "timesheets#approved_to_unapproved", :as=>:approved_to_unapproved_timesheet, :via=>:get
    match "/timesheets/:id/pending_to_rejected" => "timesheets#pending_to_rejected", :as=>:pending_to_rejected_timesheet, :via=>:get
    match "/timesheets/:id/rejected_to_unapproved" => "timesheets#rejected_to_unapproved", :as=>:rejected_to_unapproved_timesheet, :via=>:get
    #match "/timesheets/:id/invoice" => "timesheets#invoice", :as=>:approved_to_invoiced_timesheet, :via=>:get


    resources :timesheets
    resources :monthly_timesheets, :controller => 'timesheets'
    resources :weekly_timesheets, :controller => 'timesheets'

    match "/documents/detach/:id" => "documents#detach", :as=>:detach_document, :via=>:get
    match "/documents/listings" => "documents#listings", :as=>:listing_document, :via=>:get
    match "/documents/attach" => "documents#attach", :as=>:attach_document, :via=>:post

    resources :documents
    match "commissions/commission_payment" => "commissions#commission_payment", :as=>:commission_payment, :via=>:get
    match "commissions/index" => "commissions#index", :as=>:commissions, :via=>:get
    resources :commissions

    match "/time_entries/:id/unapproved_to_pending" => "timesheets#unapproved_to_pending", :as=>:unapproved_to_pending_timesheet, :via=>:get
    match "/time_entries/:id/pending_to_approved" => "timesheets#pending_to_approved", :as=>:pending_to_approved_timesheet, :via=>:get
    match "/time_entries/:id/approved_to_unapproved" => "timesheets#approved_to_unapproved", :as=>:approved_to_unapproved_timesheet, :via=>:get
    match "/time_entries/:id/pending_to_rejected" => "timesheets#pending_to_rejected", :as=>:pending_to_rejected_timesheet, :via=>:get
    match "/time_entries/:id/rejected_to_unapproved" => "timesheets#rejected_to_unapproved", :as=>:rejected_to_unapproved_timesheet, :via=>:get

    match "/time_entries/get_monthly_view" => "time_entries#get_monthly_view", :as=>:get_monthly_view_time_entry, :via=>:get
    match "/time_entries/get_monthly_entry_form" => "time_entries#get_monthly_entry_form", :as=>:get_monthly_entry_form_time_entry, :via=>:get
    match "/time_entries/add_multiple" => "time_entries#add_multiple", :as=>:add_multiple_time_entry, :via=>:post


    resources :time_entries
    resources :billable_periods

    match "/contracts/cancel_contract/:id" =>"contracts#cancel_contract", :as=>:cancel_contract, :via=>:get
    match "/contracts/new_contract" =>"contracts#new_contract", :as=>:new_contract, :via=>:get
    match "/contracts/my" =>"contracts#my_contracts", :as=>:my_contracts, :via=>:get
    match "/contracts/get_candidates/:id" =>"contracts#get_candidates", :as=>:get_candidates, :via=>:get
    match "/contracts/get_customer/:id" =>"contracts#get_customer", :as=>:get_customer, :via=>:get
    match "/get_candidates/:id" =>"contracts#get_candidates", :as=>:get_candidates, :via=>:get
    match "/get_customer/:id" =>"contracts#get_customer", :as=>:get_customer, :via=>:get
    resources :contracts

    match "/inquiries/pending_to_accepted" => "inquiries#pending_to_accepted", :as=>:pending_to_accepted_inquiry , :via=>:get
    match "/inquiries/accepted_to_pending"=> "inquiries#accepted_to_pending", :as=>:accepted_to_pending_inquiry ,:via=>:get
    match "/inquiries/pending_to_rejected"=> "inquiries#pending_to_rejected", :as=>:pending_to_rejected_inquiry ,:via=>:get
    match "/inquiries/rejected_to_pending"=> "inquiries#rejected_to_pending", :as=>:rejected_to_pending_inquiry ,:via=>:get
    resources :inquiries
    match "/candidates/join" =>"candidates#join", :as=>:candidate_join, :via=>:get

    match "/candidates/register" =>"candidates#register", :as=>:candidate_registration, :via=>:post
    match "/candidates/list" =>"candidates#list", :as=>:candidate_list, :via=>:get


    match "/candidates/login" =>"candidates#login", :as=>:candidate_login, :via=>:get
    match "/candidates/authenticate" =>"candidates#authenticate", :as=>:candidate_authentication, :via=>:post
    match "/candidates/new_candidate_signup" =>"candidates#new_candidate_signup", :as=>:new_candidate_signup, :via=>:get

    resources :candidates
    match "/jobs/my" =>"jobs#my_jobs", :as=>:my_jobs, :via=>:get
    match "/jobs/:id/view" =>"jobs#view", :as=>:view_job, :via=>:get
    match "/jobs/:id/apply" =>"jobs#apply", :as=>:apply_job, :via=>:get
    match "/jobs/:id/applied" =>"jobs#applied", :as=>:applied_job, :via=>:post
    match "/jobs/portal" =>"jobs#portal", :as=>:portal_job, :via=>:get
    match "/jobs/feed" =>"jobs#feed", :as=>:jobs_feed, :via=>:get, :format => 'rss'

    match "jobs/add_candidates" => "jobs#add_candidates", :as =>:add_candidates_to_job, :via=>:post
    match "jobs/:id/set_job_status_to_active" => "jobs#set_job_status_to_active", :as => :set_job_status_to_active, :via => :get
    match "jobs/:id/set_job_status_to_inactive" => "jobs#set_job_status_to_inactive", :as => :set_job_status_to_inactive, :via => :get
    resources :jobs
    

    match "trainings/:id/set_training_status_to_active" => "trainings#set_training_status_to_active", :as => :set_training_status_to_active, :via => :get
    match "trainings/:id/set_training_status_to_inactive" => "trainings#set_training_status_to_inactive", :as => :set_training_status_to_inactive, :via => :get
    match "/trainings/my_trainings" =>"trainings#my_trainings", :as=>:my_trainings, :via=>:get
    match "/trainings/portal" =>"trainings#portal", :as=>:portal_training, :via=>:get
    match "/trainings/:id/view" =>"trainings#view", :as=>:view_training, :via=>:get
    match "/trainings/:id/apply" =>"trainings#apply", :as=>:apply_training, :via=>:get
    match "/trainings/:id/applied" =>"trainings#applied", :as=>:applied_training, :via=>:get
    # match "/trainings/:id/applied" =>"trainings#applied", :as=>:applied_training, :via=>:post
    match "trainings/add_candidates" => "trainings#add_candidates", :as =>:add_candidates_to_training, :via=>:post
    match "trainings/accepted_candidates" => "trainings#accepted_candidates", :as =>:accepted_candidates_training, :via=>:get
    match "/trainings/feed" =>"trainings#feed", :as=>:trainings_feed, :via=>:get, :format => 'rss'
    resources :trainings
    match "/ratings/:id.json/" => "ratings#update", :via=>:post
    resources :ratings
    resources :individuals
    resources :comments #, only: [:create,:new,:index]
    resources :service_types
    match "tenants/index" => "tenants#index", :as=>:tenants, :via=>:get
    match "tenants/:id" => "tenants#show", :as=>:tenants_show, :via=>:put
    resources :tenants

    match "/contacts/:id/send_form_for_requesting_update"=>"contacts#send_form_for_requesting_update", :as=>:send_form_for_requesting_update, :via=>:get
    match "/contacts/import/" => "contacts#import", :as=>:import_contacts, :via=>:post
    match "/contacts/search/" => "contacts#search", :as=>:search_contacts, :via=>:get
    match "/contacts/send_form_for_requesting_multiple_update" => "contacts#send_form_for_requesting_multiple_update", :as=>:send_form_for_requesting_multiple_update_contact, :via=>:post
    match "/contacts/:id/get_activity_stream" => "contacts#get_activity_stream", :as=>:get_activity_stream_contacts, :via=>:get
    # match "/contacts/:data" => "contacts#index" , :via=>:post
    get '/contacts/:data' => 'contacts#index'
    post '/contacts/send_email' => "contacts#send_email"

    resources :contacts
    resources :companies

    authenticated :user do
      root :to => 'home#index'
    end
    root :to => "jobs#portal"

    match "/home/search" => "home#search", :as=>:search_everything, :via=>:get
    #match "/users/sign_in" => "users#sign_in", :as=>:user_session, :via=>:post
    match "/users/:id/change_password" =>"users#change_password", :as=>:change_password_user, :via=>:get
    match "/users/:id/update_password" => "users#update_password", :as=>:update_password_user, :via=>:post
    match "/users/change_my_password" =>"users#change_my_password", :as=>:change_my_password_user, :via=>:get
    match "/users/update_my_password" => "users#update_my_password", :as=>:update_my_password_user, :via=>:post
    #devise_for :user
  end

  #match "/" => "payment#index", as: :payment_index
  match "/myaccounts" => "home#accounts", as: :user_accounts, via: :get
  match "/checkname" => "home#checkname", as: :check_name, via: :get

  # root :to => "home#index"
  authenticated :user do
    root :to => 'home#index'
  end
  root :to => "home#main"
  match "/home/send_email" => "home#send_email", :as=>:home_send_email, :via=>:post
  devise_for :users, controllers: {registrations: "milia/registrations" }
  # as :user do
  #   get 'users/edit' => 'devise/registrations#edit', :as => 'edit_user_registration'
  #   put 'users' => 'devise/registrations#update', :as => 'user_registration'
  # end
  resources :users
end
