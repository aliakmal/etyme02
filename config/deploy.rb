set :application, 'etyme'
set :repo_url, 'git@bitbucket.org:aliakmal/etyme02.git'

# ask :branch, proc { `git rev-parse --abbrev-ref HEAD`.chomp }
set :branch, 'develop'
set :deploy_to, '/home/deploy/apps/etyme'
set :deploy_via, :remote_cache
set :use_sudo, false
set :scm, :git


set :format, :pretty
set :log_level, :debug
set :pty, true

set :linked_files, %w{config/database.yml}
set :linked_dirs, %w{bin log tmp/pids tmp/cache tmp/sockets vendor/bundle public/system}

# set :default_env, { path: "/opt/ruby/bin:$PATH" }
set :keep_releases, 5

namespace :deploy do

  desc 'Schema load'
  task :schema do
    on roles(:db), in: :sequence do
      within release_path do
        execute 'rake db:schema:load'
      end
    end
  end

  desc 'Start application'
  task :start do
    on roles(:app), in: :sequence do
      # Your start mechanism here, for example:
      execute "echo cloud9tyme | sudo -S start etyme"
    end
  end

  desc 'Stop application'
  task :stop do
    on roles(:app), in: :sequence do
      # Your stop mechanism here, for example:
      execute "echo cloud9tyme | sudo -S stop etyme"
    end

  end

  desc 'Restart application'
  task :restart do
    on roles(:app), in: :sequence do
      # Your restart mechanism here, for example:
      execute "echo cloud9tyme | sudo -S restart etyme"
    end
  end
  
  #desc "Update the crontab file"
  #task :update_crontab do
    #on roles(:app), in: :sequence do
      #execute "cd #{current_release}; bundle exec whenever --update-crontab #{application}"
    #end
  #end
  
  after :restart, :clear_cache do
    on roles(:web), in: :groups, limit: 3, wait: 10 do
      # Here we can do anything such as:
      # within release_path do
      #   execute :rake, 'cache:clear'
      # end
    end
  end

  after :finishing, 'deploy:cleanup'
  after :published, 'deploy:restart'
end
