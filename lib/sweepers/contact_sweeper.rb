class ContactSweeper < ActionController::Caching::Sweeper
  observe Contact, Company, Individual

  def after_create(contact)
    expire_cache_for(contact)
  end

  def after_update(contact)
    expire_cache_for(contact)
  end

  private
  def expire_cache_for(contact)
    # Expire the index page now that we added a new product
    expire_page(:controller => 'contacts', :action => 'index')
    expire_action(:controller => 'contacts', :action => 'index')
  end
end
