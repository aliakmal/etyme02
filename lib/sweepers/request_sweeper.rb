class RequestSweeper < ActionController::Caching::Sweeper
  observe Request

  def after_create(request)
    expire_cache_for(request)
  end

  def after_update(request)
    expire_cache_for(request)
  end

  private
  def expire_cache_for(request)
    # Expire the index page now that we added a new product
    #expire_page(:controller => 'requests', :action => 'index', :cache_path => Proc.new { |c| c.params })
    expire_fragment(%r{requests?.*})  #(:controller => 'requests', :action => 'index', :cache_path => Proc.new { |c| c.params } )
  end
end