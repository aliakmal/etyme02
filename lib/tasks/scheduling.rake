namespace :scheduling do
  desc "Automatic Invoicing"
#   task :auto_invoicing => :environment do
#     puts "Starting invoicing"
#     tenants =Tenant.all
#     tenants.each do |tenant|
#       # contracts = Contract.find_by_sql "select * from contracts where tenant_id = \'#{tenant.id}\'"
#       contracts = Contract.unscoped.where(:tenant_id =>tenant.id)
#       prices = Price.unscoped.where(:tenant_id =>tenant.id)
#       puts "at contracts"
#       charge_customer = 0
#       pay_candidate = 0
#       invoiced_timesheets = Array.new
#       contracts.each do |contract|
#         timesheets= Timesheet.unscoped.where(:contract_id => contract.id, :state => 'approved' , :tenant_id => tenant.id)
#         if !timesheets.empty?
#           prices.each do |price|
#             if contract.id == price.priceable_id and (price.price_type == 'Employee' or price.price_type == 'Vendor') and Time.now <= price.end_date
#                 @cost_price =  price.cost_price
#             elsif contract.id == price.priceable_id and price.price_type == 'Customer' and Time.now <= price.end_date
#                 @sale_price = price.sale_price
#             else
#               next
#             end
#           end
#           timesheets.each do |timesheet|
#             time_entries = TimeEntry.unscoped.where( :timesheet_id => timesheet.id, :tenant_id =>tenant.id)
#             time_entries.each do |time_entry|
#               binding.pry
#               time_entry.rate = @sale_price
#               time_entry.salary = @cost_price
#               charge_customer += @sale_price
#               pay_candidate += @cost_price
#               # TimeEntry.unscoped.where(:tenant_id =>tenant.id).update(time_entry.id, :rate =>@sale_price)
#               # TimeEntry.unscoped.where(:id=>time_entry.id, :tenant_id =>tenant.id).first.save

#               binding.pry 
#               time_entry.save(:validate => false)
#             end
#             timesheet.invoiceit
#             invoiced_timesheets << timesheet
#             timesheet.save

#           end
#            invoice = Invoice.create(:total_rate => @charge_customer,:total_salary => @pay_candidate, :state => 'invoiced',:tenant_id => tenant.id)
#            # binding.pry
#            invoiced_timesheets.each do |itimesheet|
#             # binding.pry
#               itimesheet.invoice_id = invoice.id
#               # binding.pry
#               itimesheet.save
#               # binding.pry
#            end
#            # binding.pry
#            invoice.contract_id = contract.id
#            # binding.pry
#            invoice.save
#            charge_customer = 0
#            pay_candidate = 0
#            invoiced_timesheets.clear
#         else
#           next
#         end
#       end
#       puts "finished invoicing"
#   end
# end
task :auto_salary => :environment do
  tenants = Tenant.all
  tenants.each do |tenant|
    if tenant.salary_gap_terms != nil
      contracts = Contract.unscoped.where(:tenant_id =>tenant.id)
      total_mins = 0
      @paid_sheets = Array.new
      contracts.each do |contract|
        till_date_pay_salary = Time.now - tenant.salary_gap_terms.days
        timesheets = Timesheet.unscoped.find(:all, :conditions => ["ends <=\'#{till_date_pay_salary.to_date}\' AND tenant_id = #{tenant.id} AND contract_id = #{contract.id} AND salary_payment_status = \'unpaid\' AND (state = \'approved\' OR state = \'invoiced\')"] )
        prices = Price.unscoped.where(:tenant_id =>tenant.id,:priceable_id => contract.id)
            # timesheets= Timesheet.where(:contract_id => contract.id,:state => 'approved')
            if !timesheets.empty?
              timesheets.each do |timesheet|
                total_mins += timesheet.minutes
                @paid_sheets << timesheet
              end
              prices.each do |price|
                if contract.id == price.priceable_id and (price.price_type == 'Employee' or price.price_type == 'Vendor') and Time.now <= price.end_date
                    @total_salary = (total_mins.to_i / (60)) * price.cost_price
                elsif contract.id == price.priceable_id and price.price_type == 'Customer' and Time.now <= price.end_date
                    @total_rate = (total_mins.to_i / (60)) * price.sale_price
                else
                  next
                end
              end
              # if contract.employee_type == 'employee_w2'
              #   contract.employee_price.each do |price|
              #     if Time.now <= price.end_date
              #       @total_salary = (total_mins.to_i / (60)) * price.cost_price
              #     else
              #       next
              #     end
              #   end
              # else
              #   contract.vendor_price.each do |price|
              #     binding.pry
              #     if Time.now <= price.end_date
              #       @total_salary = (total_mins.to_i / (60)) * price.cost_price
              #     else
              #       next
              #     end
              #   end
              # end
              # contract.customer_price.each do |price|
              #   if Time.now <= price.end_date
              #     @total_rate = (total_mins.to_i / (60)) * price.sale_price
              #   else
              #     next
              #   end
              # end
              if @total_salary != nil and @total_rate != nil
                rate = @total_salary / (total_mins/60)
                profit = @total_rate - @total_salary
                ActiveRecord::Base.connection.execute("INSERT INTO receipts (total_salary,total_time,contract_id,tenant_id,profit,rate,created_at,updated_at) VALUES (#{@total_salary},#{total_mins},#{contract.id},#{tenant.id},#{profit},#{rate},\'#{Time.now}\',\'#{Time.now}\');")
                # Receipt.create(:total_salary =>@total_salary,:total_time => total_mins,:rate => rate,:profit => profit, :contract_id => contract.id)
                @paid_sheets.each do |psheet|
                  ActiveRecord::Base.connection.execute("UPDATE timesheets SET salary_payment_status='paid' where id=#{psheet.id};")
                end
              end
              @total_salary = nil
              @total_rate = nil
              total_mins = 0
              @paid_sheets.clear
            else
              next
            end
          end
        else
          next
        end
      end
      subject = "Testing Auto Salary Task"
      message = "All Salary calculations completed"
      name = "Etyme Support"
      email = "test@salary.co"
      ClientMailer.salary_email_admin(subject,message,name,email).deliver
      ClientMailer.salary_email_me(subject,message,name,email).deliver
    end

  end
