#require "bundler/capistrano"

set :application, "etyme.com"
set :scm, :git
set :scm_passphrase, "j7cqdtiz.5133"
# set :scm, :git # You can set :scm explicitly or Capistrano will make an intelligent guess based on known version control directory names
# Or: `accurev`, `bzr`, `cvs`, `darcs`, `git`, `mercurial`, `perforce`, `subversion` or `none`
# Define your server here

# Set application settings
#require '/config/deploy_credentials.rb'
set :user, "deploy" # As defined on your server
set :password, "admin123"

task :production do
  set :repository,  "git@bitbucket.org:aliakmal/etyme02.git"
  set :branch, fetch(:branch, "master")
  server "162.243.192.135", :web, :app, :db, primary: true
  set :deploy_to, "/home/etyme" # Directory in which the deployment will take place
  set :app_name, "etyme"
end

set :mysql_user, 'rails'
set :mysql_password, 'WajHGLzXnf'

set :deploy_via, :remote_cache
set :use_sudo, false
#ssh_options[:keys] = ["/home/ali/.ssh/id_rsa"]
#ssh_options[:forward_agent] = true
#default_run_options[:pty] = true

# if you want to clean up old releases on each deploy uncomment this:
# after "deploy:restart", "deploy:cleanup"

# if you're still using the script/reaper helper you will need
# these http://github.com/rails/irs_process_scripts

# If you are using Passenger mod_rails uncomment this:
namespace :deploy do
  task :start do ; end
  task :stop do ; end
  task :restart, :roles => :app, :except => { :no_release => true } do
    run "#{try_sudo} touch #{File.join(current_path,'tmp','restart.txt')}"
  end
end
  
namespace :deploy do  
  desc "Recreate symlink"
  task :resymlink, :roles => :app do
    run "#{try_sudo} rm -f #{current_path} && ln -s #{release_path} #{current_path}"
  end
    desc "Create database yaml in shared path"
  task :db_setup do
    db_config = ERB.new <<-EOF
    base: &base
      adapter: mysql
      socket: /var/run/mysqld/mysql.sock
      username: #{mysql_user}
      password: #{mysql_password}

    development:
      database: #{app_name}_dev
      <<: *base

    test:
      database: #{app_name}_test
      <<: *base

    production:
      database: #{app_name}_prod
      <<: *base
    EOF

    run "#{try_sudo} mkdir -p #{shared_path}/config"
    File.open("#{shared_path}/config/database.yml", "w") { |f| f.puts db_config.result }
    #put db_config.result, "#{shared_path}/config/database.yml"
  end
  
  desc "Make symlink for database yaml"
  task :create_symlink do
    run "ln -nfs #{shared_path}/config/database.yml #{release_path}/config/database.yml"
  end

end


require 'erb'

namespace :db do

end


before "deploy:setup", "deploy:db_setup"
#before "deploy:migrate", "deploy:set_database_symlink"

before "deploy:assets:precompile", "deploy:create_symlink"


after "deploy:create_symlink", "deploy:resymlink"
