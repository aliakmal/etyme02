require 'spec_helper'

describe ClientController do
  def valid_attributes
    FactoryGirl.attributes_for(:company)
  end
describe "POST update" do
    describe "with valid params" do
      it "updates the requested contact" do
        contact = Company.create! valid_attributes
        pp contact
        # Assuming there are no other contacts in the database, this
        # specifies that the Contact created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Company.any_instance.should_receive(:update_attributes).with({ "name" => "Acme Company" })
        post :update_contact, {:id => contact.id, :company => { "name" => "Acme Company" }}
      end

      it "assigns the requested contact as @contact" do
        contact = Company.create! valid_attributes
        post :update_contact, {:company => valid_attributes}
        assigns(:company).should eq(contact)
      end

      it "redirects to the contact" do
        contact = Company.create! valid_attributes
        
        post :update_contact, {:company => valid_attributes.merge(:id=>contact.id)}
        response.should redirect_to(:thank_you)
      end
    end

    describe "with invalid params" do
      it "assigns the contact as @company" do
        contact = Company.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Company.any_instance.stub(:save).and_return(false)
        post :update_contact, {:company => {:id => contact.id,  "name" => "" }}
        assigns(:company).should eq(contact)
      end

      it "re-renders the 'edit' template" do
        contact = Company.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Company.any_instance.stub(:save).and_return(false)
        post :update_contact, {:company => { :id => contact.id, "name" => "" }}
        response.should render_template("edit_contact")
      end
    end
  end
end
