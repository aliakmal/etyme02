require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe CommentsController do
  # This should return the minimal set of attributes required to create a valid
  # Comment. As you add validations to Comment, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    aircraft = FactoryGirl.create(:aircraft)
    {
      "comment"=>{"comment"=> "One comment"},
      "resource"=>{"id"=> aircraft.id , "type"=>"Aircraft"}
    }
  end

  before (:each) do
    @request.env['HTTP_REFERER'] = "http://test.com/aircraft/#{valid_attributes['resource']['id']}"
    @user = FactoryGirl.create(:admin)
    sign_in @user
    #controller.stub(:current_user).and_return(@user)
  end

  describe "GET index" do
    it "assigns needed comments as @comments" do
      comment = FactoryGirl.create(:comment)
      attributes = {
        resource_id: comment.commentable_id,
        resource_type: comment.commentable_type,
        after: 0
      }
      get :index,
        resource_id: comment.commentable_id,
        resource_type: comment.commentable_type,
        after: 0,
        format: :js
      assigns(:comments).should eq([comment])
    end

  end


  describe "POST create" do
    describe "with valid params" do
      it "creates a new Comment" do
        lambda {
          post :create, valid_attributes
        }.should change(Comment, :count).by(1)
      end

      it "assigns a newly created comment as @comment" do
        post :create, valid_attributes
        assigns(:comment).should be_a(Comment)
        assigns(:comment).should be_persisted
        assigns(:comment).user_id.should eq(@user.id)
      end

      it "redirects to the http_refferer" do
        post :create, valid_attributes
        response.should redirect_to(@request.env['HTTP_REFERER'])
      end
    end

    describe "with invalid params" do
      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Comment.any_instance.stub(:save).and_return(false)
        post :create, {:comment => {}}
        response.should redirect_to(@request.env['HTTP_REFERER'])
      end
    end
  end

end
