require 'spec_helper'

# This spec was generated by rspec-rails when you ran the scaffold generator.
# It demonstrates how one might use RSpec to specify the controller code that
# was generated by Rails when you ran the scaffold generator.
#
# It assumes that the implementation code is generated by the rails scaffold
# generator.  If you are using any extension libraries to generate different
# controller code, this generated spec may or may not pass.
#
# It only uses APIs available in rails and/or rspec-rails.  There are a number
# of tools you can use to make these specs even more expressive, but we're
# sticking to rails and rspec-rails APIs to keep things simple and stable.
#
# Compared to earlier versions of this generator, there is very limited use of
# stubs and message expectations in this spec.  Stubs are only used when there
# is no simpler way to get a handle on the object needed for the example.
# Message expectations are only used when there is no simpler way to specify
# that an instance is receiving a specific message.

describe ServicesController do

  # This should return the minimal set of attributes required to create a valid
  # Service. As you add validations to Service, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    @service_type_default = FactoryGirl.create(:service_type)
    
    @request_default = FactoryGirl.create(:request)
    @sector_default = FactoryGirl.create(:sector, :request_id=>@request_default.id)
    FactoryGirl.attributes_for(:service, :service_type_name=>@service_type_default.desc, :sector_id => @sector_default.id)
  end

  before(:each) do
    @user = FactoryGirl.create(:admin)
    sign_in @user
  end

  describe "GET index" do
    it "assigns all services as @services" do
      service = Service.create! valid_attributes
      get :index, {}
      assigns(:services).should eq([service])
    end
  end

  describe "GET show" do
    it "assigns the requested service as @service" do
      service = Service.create! valid_attributes
      get :show, {:id => service.to_param}
      assigns(:service).should eq(service)
    end
  end

  describe "GET new" do
    it "assigns a new service as @service when created from a sector" do
      sector = FactoryGirl.create(:sector)
      get :new, {:sector_id=>sector.id}
      assigns(:service).should be_a_new(Service)
    end
  end

  describe "GET edit_note" do
    it "assigns the requested service as @service" do
      service = Service.create! valid_attributes
      xhr :get, :edit_note, {:id => service.to_param}
      assigns(:service).should eq(service)
    end
  end
  
  describe "GET edit" do
    it "assigns the requested service as @service" do
      service = Service.create! valid_attributes
      get :edit, {:id => service.to_param}
      assigns(:service).should eq(service)
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Service" do
        lambda {
          xhr :post, :create, {:service => valid_attributes}
        }.should change(Service, :count).by(1)
      end

      it "assigns a newly created service as @service" do
        xhr :post, :create, {:service => valid_attributes}
        assigns(:service).should be_a(Service)
      end
    end

    describe "with invalid params" do
      it "assigns a newly created but unsaved service as @service" do
        # Trigger the behavior that occurs when invalid params are submitted
        Service.any_instance.stub(:save).and_return(false)
        post :create, {:service => { "service_type_id" => "" }}
        assigns(:service).should be_a_new(Service)
      end

      it "re-renders the 'new' template" do
        # Trigger the behavior that occurs when invalid params are submitted
        Service.any_instance.stub(:save).and_return(false)
        post :create, {:service => { "service_type_id" => "" }}
        response.should render_template("new")
      end
    end
  end

  describe "PUT update" do
    describe "with valid params" do
      it "updates the requested service" do
        service = Service.create! valid_attributes
        # Assuming there are no other services in the database, this
        # specifies that the Service created on the previous line
        # receives the :update_attributes message with whatever params are
        # submitted in the request.
        Service.any_instance.should_receive(:update_attributes).with({ "details" => "Do the work" })
        put :update, {:id => service.to_param, :service => { "details" => "Do the work" }}
      end

      it "assigns the requested service as @service" do
        service = Service.create! valid_attributes
        xhr :put, :update, {:id => service.to_param, :service => valid_attributes}
        assigns(:service).should eq(service)
      end

    end

    describe "with invalid params" do
      it "assigns the service as @service" do
        service = Service.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Service.any_instance.stub(:save).and_return(false)
        put :update, {:id => service.to_param, :service => { "service_type_name" => "" }}
        assigns(:service).should eq(service)
      end

      it "re-renders the 'edit' template" do
        service = Service.create! valid_attributes
        # Trigger the behavior that occurs when invalid params are submitted
        Service.any_instance.stub(:save).and_return(false)
        put :update, {:id => service.to_param, :service => { "service_type_name" => "" }}
        response.should render_template("edit")
      end
    end
  end

  describe "DELETE destroy" do
    it "destroys the requested service" do
      service = Service.create! valid_attributes
      expect {
        xhr :delete, :destroy, {:id => service.to_param}
      }.to change(Service, :count).by(-1)
    end

  end
  
  describe "PUT change of status" do
    it "new_to_pending should change status from new to pending" do
      service = FactoryGirl.create(:service)
      xhr :put, :new_to_pending, {:id => service.id}
      service.reload
      service.service_status.should == 'pending'
    end
    
    it "new_to_cancelled should change status from new to cancelled" do
      service = FactoryGirl.create(:service)
      xhr :put, :new_to_cancelled, {:id => service.id}
      service.reload
      service.service_status.should == 'cancelled'
    end
    
    it "pending_to_cancelled should change status from pending to cancelled" do
      
      service = FactoryGirl.create(:service, :service_status=>'pending')
      xhr :put, :pending_to_cancelled, {:id => service.id}
      service.reload
      service.service_status.should == 'cancelled'
      
    end
    
    it "pending_to_new should change status from pending to new" do
      
      service = FactoryGirl.create(:service, :service_status=>'pending')
      xhr :put, :pending_to_new, {:id => service.id}
      service.reload
      service.service_status.should == 'new'
      
    end
    
    it "pending_to_complete should change status from pending to done" do
      
      service = FactoryGirl.create(:service, :service_status=>'pending')
      xhr :put, :pending_to_complete, {:id => service.id}
      service.reload
      service.service_status.should == 'done'
      
    end
    
    it "cancelled_to_pending should change status from cancelled to pending" do
      service = FactoryGirl.create(:service, :service_status=>'cancelled')
      xhr :put, :cancelled_to_pending, {:id => service.id}
      service.reload
      service.service_status.should == 'pending'
    end
    
    it "new to stalled should change status from new to stalled" do
      service = FactoryGirl.create(:service, :service_status=>'new')
      xhr :put, :new_to_stalled, {:id => service.id}
      service.reload
      service.service_status.should == 'stalled'
    end

    it "pending to stalled should change status from pending to stalled" do
      service = FactoryGirl.create(:service, :service_status=>'pending')
      xhr :put, :pending_to_stalled, {:id => service.id}
      service.reload
      service.service_status.should == 'stalled'
    end
    
    it "complete_to_pending should change status from done to pending" do
      service = FactoryGirl.create(:service, :service_status=>'done')
      xhr :put, :complete_to_pending, {:id => service.id}
      service.reload
      service.service_status.should == 'pending'
    end
    
    it "stalled to new  should change status from stalled to new" do
      service = FactoryGirl.create(:service, :service_status=>'stalled')
      xhr :put, :stalled_to_new, {:id => service.id}
      service.reload
      service.service_status.should == 'new'
    end

    it "stalled to pending should change status from stalled to pending" do
      service = FactoryGirl.create(:service, :service_status=>'stalled')
      xhr :put, :stalled_to_pending, {:id => service.id}
      service.reload
      service.service_status.should == 'pending'
    end

  end

end
