require 'spec_helper'

describe SectorsController do

  # This should return the minimal set of attributes required to create a valid
  # Contact. As you add validations to Contact, be sure to
  # update the return value of this method accordingly.
  def valid_attributes
    FactoryGirl.attributes_for(:sector)
  end

  before(:each) do
    @user = FactoryGirl.create(:admin)
    sign_in @user
  end
  
  describe "DELETE destroy" do
    it "destroys the requested sector" do
      sector = Sector.create valid_attributes
      lambda {
        delete :destroy, {:id => sector.to_param}
      }.should change(Sector, :count).by(-1)
    end

  end
  
end