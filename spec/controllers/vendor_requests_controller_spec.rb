require 'spec_helper'

describe VendorRequestsController do
  before(:each) do
    @user = FactoryGirl.create(:admin)
    sign_in @user
    
    @service = FactoryGirl.create(:service)
  end
  
  def valid_attributes
    FactoryGirl.attributes_for(:vendor_request)
  end
  
  describe "GET 'new'" do
    it "returns http success" do
      get 'new', {:service_id => @service.id}
      response.should be_success
    end
  end

  describe "POST create" do
    describe "with valid params" do
      it "creates a new Request" do
        lambda {
          post :create, {:vendor_request => valid_attributes}
        }.should change(VendorRequest, :count).by(1)
      end
    end
  end


end
