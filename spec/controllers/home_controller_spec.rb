require 'spec_helper'

describe HomeController do
  before(:each) do
    @user = FactoryGirl.create(:admin)
    sign_in @user
  end
  
  describe "GET 'index'" do
    it "should redirect to login if user is not signed in" do
      (sign_out @user).should redirect_to(new_user_session_url)
    end
    it "should redirect to login if user is not signed in" do
      response.should redirect_to(requests_url)
    end
    
  end
  
  describe "GET search" do
    it "results shoudl equal count of requests and contacts" do
      rqst = FactoryGirl.create(:request)
      contact = FactoryGirl.create(:individual)
      
      assigns(:results).should eq([rqst, contact])
    end
  end

end
