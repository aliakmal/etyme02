require "spec_helper"

describe BillablePeriodsController do
  describe "routing" do

    it "routes to #index" do
      get("/billable_periods").should route_to("billable_periods#index")
    end

    it "routes to #new" do
      get("/billable_periods/new").should route_to("billable_periods#new")
    end

    it "routes to #show" do
      get("/billable_periods/1").should route_to("billable_periods#show", :id => "1")
    end

    it "routes to #edit" do
      get("/billable_periods/1/edit").should route_to("billable_periods#edit", :id => "1")
    end

    it "routes to #create" do
      post("/billable_periods").should route_to("billable_periods#create")
    end

    it "routes to #update" do
      put("/billable_periods/1").should route_to("billable_periods#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/billable_periods/1").should route_to("billable_periods#destroy", :id => "1")
    end

  end
end
