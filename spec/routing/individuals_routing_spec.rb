require "spec_helper"

describe IndividualsController do
  describe "routing" do

    it "routes to #index" do
      get("/individuals").should route_to("individuals#index")
    end

    it "routes to #new" do
      get("/individuals/new").should route_to("individuals#new")
    end

    it "routes to #show" do
      get("/individuals/1").should route_to("individuals#show", :id => "1")
    end

    it "routes to #edit" do
      get("/individuals/1/edit").should route_to("individuals#edit", :id => "1")
    end

    it "routes to #create" do
      post("/individuals").should route_to("individuals#create")
    end

    it "routes to #update" do
      put("/individuals/1").should route_to("individuals#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/individuals/1").should route_to("individuals#destroy", :id => "1")
    end

  end
end
