require "spec_helper"

describe AircraftsController do
  describe "routing" do

    it "routes to #index" do
      get("/aircrafts").should route_to("aircrafts#index")
    end

    it "routes to #new" do
      get("/aircrafts/new").should route_to("aircrafts#new")
    end

    it "routes to #show" do
      get("/aircrafts/1").should route_to("aircrafts#show", :id => "1")
    end

    it "routes to #edit" do
      get("/aircrafts/1/edit").should route_to("aircrafts#edit", :id => "1")
    end

    it "routes to #create" do
      post("/aircrafts").should route_to("aircrafts#create")
    end

    it "routes to #update" do
      put("/aircrafts/1").should route_to("aircrafts#update", :id => "1")
    end

    it "routes to #destroy" do
      delete("/aircrafts/1").should route_to("aircrafts#destroy", :id => "1")
    end

  end
end
