# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :service_type do
    account_ref "OS-OVF"
    desc "SYRIA Overflight"
  end
end
