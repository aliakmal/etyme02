# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :time_entry do
    contract_id 1
    desc "MyString"
    start_at "2013-10-18 21:06:56"
    end_at "2013-10-18 21:06:56"
    candidate_id 1
    total_minutes 1
  end
end
