# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contract do
    start_date "2013-07-11"
    end_date "2013-07-11"
    candidate_id 1
    job_id 1
  end
end
