# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :comment do
    comment 'This is a nice comment'
    user
    commentable { create :aircraft }
  end
end
