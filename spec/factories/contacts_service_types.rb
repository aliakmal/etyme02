# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contacts_service_type, :class => 'ContactsServiceTypes' do
    template
    service_type
    contact
  end
end
