# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :commission do
    commission_type "MyString"
    user nil
    fixed_commission 1
    hourly_commission "MyString"
  end
end
