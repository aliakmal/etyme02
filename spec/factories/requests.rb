# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :request do
    customer_name "Big Boss"
    customer
    association :customer_id, :factory => :company

    operator_name "Chang Ops."
    operator
    association :operator_id, :factory => :company 
    
    date_of_request "2013-01-06 09:58:39"
    date_due "2013-01-06 09:58:39"
    aircraft_reg 'GH111'
    schedule "12MAR2012 OMRK 1300 OMSJ 1600 FERRY"
    is_substitute "USE SUBSTITUTE"
  end
end
