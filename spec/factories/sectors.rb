# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :sector do
    desc "12MAR2012 OMRK 1300 OMSJ 1600 FERRY"
    flt_no "FLT1200"
    request
  end
end
