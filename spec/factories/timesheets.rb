# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :timesheet do
    contract_id 1
    type ""
    starts "2013-11-13"
    ends "2013-11-13"
    minutes 1
    state "MyString"
  end
end
