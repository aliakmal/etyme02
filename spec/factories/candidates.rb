# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :candidate do
    first_name "MyString"
    last_name "MyString"
    designation "MyString"
    best_time_to_call "MyString"
    website "MyString"
    current_employer "MyString"
    date_available "2013-06-18"
    current_pay "MyString"
    desired_pay "MyString"
    can_relocate false
  end
end
