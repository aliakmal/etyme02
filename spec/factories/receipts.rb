# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :receipt do
    total_salary 1.5
    total_time 1
    contract nil
    tenant_id 1
    profit 1.5
    rate 1.5
  end
end
