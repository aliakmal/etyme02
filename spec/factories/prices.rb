# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :price do
    price_type "MyString"
    cost_price 1.5
    sale_price 1.5
    start_date "2014-04-07 11:52:48"
    end_date "2014-04-07 11:52:48"
    master_services_agreement "MyString"
    purchase_orders "MyString"
    non_compete_agreement "MyString"
    contract nil
  end
end
