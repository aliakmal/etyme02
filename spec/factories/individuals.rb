# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :individual do
    first_name "MyString"
    last_name "MyString"
    about "MyText"
    designation "MyString"
    salutation "MyString"
    company_id 1
  end
end
