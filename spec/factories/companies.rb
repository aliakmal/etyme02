# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :company, :aliases => [:operator, :customer] do
    name "Company Name"
    company_name "Company Name"
    description "MyText"
  end


end
