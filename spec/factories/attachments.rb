# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :attachment do
    name "MyString"
    attachment "MyString"
    contract nil
    tenant nil
  end
end
