# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :address_detail do
    address "MyText"
    city "MyString"
    state "MyString"
    country "MyString"
    zip "MyString"
  end
end
