# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :documentable do
    documentable_id 1
    documentable_type "MyString"
  end
end
