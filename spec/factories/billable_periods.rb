# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :billable_period do
    start_date "2013-07-16 00:03:48"
    end_date "2013-07-16 00:03:48"
    rate 1.5
    contract_id 1
  end
end
