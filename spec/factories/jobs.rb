# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :job do
    title "MyString"
    city "MyString"
    state "MyString"
    zip "MyString"
    salary "MyString"
    start_date "2013-06-18"
    duration "MyString"
    maximum_rate "MyString"
    job_type "MyString"
    openings 1
    company_job_id "MyString"
  end
end
