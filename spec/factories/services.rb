# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :service do
    details "China"
    notes "Some notes go here"
    sector
    service_type
  end
end
