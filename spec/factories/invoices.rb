# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :invoice do
    ref "MyString"
    state "MyString"
    total 1.5
  end
end
