# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact do
    name "Acme Company"
    about "Acme company is a nice company."
    contact_type "Company"
  end
end
