# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :aircraft do
    ac_reg "GH123"
    ac_type "Boeing 727"
    mtow 1000
    operator_name "Mr Operator guy"
    
    association :operator_id, :factory => :company

  end
end
