# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :document do
    title "MyString"
    filename "MyString"
    desc "MyText"
    issue_date "2013-11-09"
    expiry_date "2013-11-09"
    filesize 1
    file "MyText"
  end
end
