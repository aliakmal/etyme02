# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :contact_detail do
    type "Phone"
    location "work"
    details "12121212222"
    contact_id 1
  end
end
