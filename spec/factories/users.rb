# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    #username 'dude'
    #name 'Test User'
    #email 'example@example.com'
    sequence(:username) {|n| "UserName_#{n}" }
    sequence(:name) {|n| "Users Name #{n}" }
     
    sequence(:email) {|n| "email#{n}-#{Time.new.to_time.to_i.to_s}@factory.com" }
    
    password 'please123'
    password_confirmation 'please123'
    # required if the Devise Confirmable module is used
    # confirmed_at Time.now
  end
  
  factory :admin,:parent => :user do
    after(:create) { |u| u.add_role 'admin' }
  end
  
end