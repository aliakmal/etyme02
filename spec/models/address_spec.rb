require 'spec_helper'

describe Address do
  before(:each) do
    c = FactoryGirl.create(:company)
    @attr = { 
      :contact_id => c.id
    }
  end
  
  it "should respond to city" do
    address = Address.new(@attr)
    address.should respond_to(:city)
  end

  it "should respond to address" do
    address = Address.new(@attr)
    address.should respond_to(:address)
  end

  it "should respond to zip" do
    address = Address.new(@attr)
    address.should respond_to(:zip)
  end

  it "should respond to state" do
    address = Address.new(@attr)
    address.should respond_to(:state)
  end

  it "should respond to country" do
    address = Address.new(@attr)
    address.should respond_to(:country)
  end
  
  describe "should have an address detail object" do
    it "when empty address is called" do
      address = Address.new()
      address.address
      address.address_detail.should_not eq(nil)
    end
    it "when empty zip is called" do
      address = Address.new()
      address.zip
      address.address_detail.should_not eq(nil)
    end
    it "when empty city is called" do
      address = Address.new()
      address.city
      address.address_detail.should_not eq(nil)
    end
    it "when empty state is called" do
      address = Address.new()
      address.state
      address.address_detail.should_not eq(nil)
    end
    it "when empty country is called" do
      address = Address.new()
      address.country
      address.address_detail.should_not eq(nil)
    end
    
  end


  describe "on setting address " do
    it "on setting address should have an address detail object" do
      address = Address.new(@attr.merge(:address=>'101 Street Road'))
      
      address.address.should eq('101 Street Road')
    end
    
  
    it "on setting city should have an address detail object" do
      address = Address.new(@attr.merge(:city=>'One City'))
      address.city.should eq('One City')
    end
    
    it "on setting zip should have an address detail object" do
      address = Address.new(@attr.merge(:zip => '90210'))
      address.zip.should eq('90210')
    end
    
    it "on setting state should have an address detail object" do
      address = Address.new(@attr.merge(:state => 'Arizona'))
      address.state.should eq('Arizona')
    end
  
    it "on setting country should have an address detail object" do
      address = Address.new(@attr.merge(:country=>'Ireland'))
      address.country.should eq('Ireland')
    end
  end
  
  it "should create an address_detail object when saved as a tuple" do
    lambda{
      address = Address.create(@attr.merge(:address=>'101 Street Road', :city=>'One City', :zip => '90210', :state => 'Arizona', :country=>'Ireland'))
      address.assign_tuple
    }.should change(AddressDetail, :count).by(1)
  end
  it "should create an empty address_detail object when assign_tuple is called" do
    address = Address.create(@attr)#@attr.merge(:address=>'101 Street Road', :city=>'One City', :zip => '90210', :state => 'Arizona', :country=>'Ireland'))
    address.assign_tuple
    address.address_detail.should_not eq(nil)
  end
end
