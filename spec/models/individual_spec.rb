require 'spec_helper'

describe Individual do
  it "should respond to tag_list" do
    person = FactoryGirl.create(:individual)
    person.should respond_to(:tag_list)
  end
  it "should assign tags to tag_list" do
    person = FactoryGirl.create(:individual)
    tags = 'Home,Boat,Ribbons'
    person.tag_list = tags
    person.tag_list.should eq('Home,Boat,Ribbons')
  end

  it "should respond to tags_as_text" do
    person = FactoryGirl.create(:individual)
    person.should respond_to(:tags_as_text)
  end
  it "should assign tags as text to tags_as_text" do
    person = FactoryGirl.create(:individual)
    tags = 'Home,Boat,Ribbons'
    person.tags_as_text = tags
    person.tags_as_text.should eq(tags)
  end

  it "should respond to company_name" do
    person = FactoryGirl.create(:individual)
    person.should respond_to(:company_name)
  end
  
  it "should assign a company name to it" do
    person = FactoryGirl.create(:individual)
    person.company_name = 'Dudley Co.'
    person.company_id.should_not eq(0)
  end
  
  it "should be assigned to the existing company if the names are the same on initialization" do
    company = FactoryGirl.create(:company)
    person = FactoryGirl.create(:individual)
    person.company_name = company[:company_name]
    person.company_id.should eq(company[:id])    
    
  end
  
  it "by default the company_id must be zero" do
    person = FactoryGirl.create(:individual, :company_id=>0)
    person.company_id.should eq(0)
  end
  
end
