require 'spec_helper'

describe Service do
  it "if service_name does not correspond to an existing service then a service will not be entered" do
    ServiceType.delete_all
    service_type = FactoryGirl.create(:service_type, :desc=>'INDIA OVF')
    
    unvalid_service = Service.new(:service_type_name => 'NOT EXISTING SERVICE TYPE', :sector_id=>2)
    unvalid_service.should_not be_valid
    
  end
  
  it "service status can be changed from new to pending" do
    service = FactoryGirl.create(:service, :service_status=>'new')
    service.new_to_pending
    service.reload
    service[:service_status].should eq('pending')
  end  
  
  it "service status can be changed from new to cancelled" do
    service = FactoryGirl.create(:service, :service_status=>'new')
    service.new_to_cancelled
    service.reload
    service[:service_status].should eq('cancelled')
  end  
  
  it "service status can be changed from pending to cancelled" do
    service = FactoryGirl.create(:service, :service_status=>'pending')
    service.pending_to_cancelled
    service.reload
    service[:service_status].should eq('cancelled')
  end  
  
  it "service status can be changed from pending to new" do
    service = FactoryGirl.create(:service, :service_status=>'pending')
    service.pending_to_new
    service.reload
    service[:service_status].should eq('new')
  end    
    
  it "service status can be changed from pending to complete" do
    service = FactoryGirl.create(:service, :service_status=>'pending')
    service.pending_to_complete
    service.reload
    service[:service_status].should eq('done')
  end 
  
  it "service status can be changed from cancelled to pending" do
    service = FactoryGirl.create(:service, :service_status=>'cancelled')
    service.cancelled_to_pending
    service.reload
    service[:service_status].should eq('pending')
  end 
  
    
  it "service status can be changed from done to pending" do
    service = FactoryGirl.create(:service, :service_status=>'done')
    service.complete_to_pending
    service.reload
    service[:service_status].should eq('pending')
  end 
  
  it "service status can be changed from pending to stalled" do
    service = FactoryGirl.create(:service, :service_status=>'pending')
    service.pending_to_stalled
    service.reload
    service[:service_status].should eq('stalled')
  end    
  
  it "service status can be changed from new to stalled" do
    service = FactoryGirl.create(:service, :service_status=>'new')
    service.new_to_stalled
    service.reload
    service[:service_status].should eq('stalled')
  end    

    it "service status can be changed from stalled to pending" do
    service = FactoryGirl.create(:service, :service_status=>'stalled')
    service.stalled_to_pending
    service.reload
    service[:service_status].should eq('pending')
  end    
  
  it "service status can be changed from stalled to new" do
    service = FactoryGirl.create(:service, :service_status=>'stalled')
    service.stalled_to_new
    service.reload
    service[:service_status].should eq('new')
  end    
  
  it "not_done_yet returns services that are either pending or new" do
    service_stalled = FactoryGirl.create(:service, :service_status=>'stalled')
    service_new_01 = FactoryGirl.create(:service, :service_status=>'new')
    service_new_02 = FactoryGirl.create(:service, :service_status=>'new')
    Service.not_done_yet.should eq([service_new_01, service_new_02])
  end

end