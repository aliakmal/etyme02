require 'spec_helper'

describe Contact do
  before(:each) do
    @attr = { 
      :name => "Acme Company",
      :about => "Description about Acme Company, our best client is Wil E. Coyote!",
      :contact_type => "company"
    }
  end  
  
  it "by_name returns a list of all contacts with matching names as per search term" do
    search_contact_1 = FactoryGirl.create(:company, :company_name=>'ABC Company')
    search_contact_2 = FactoryGirl.create(:company, :company_name=>'ABD Company')
    no_search_contact = FactoryGirl.create(:company, :company_name=>'ZXD Company')
    Contact.by_name('AB').should eq([search_contact_1, search_contact_2])
  end
  
  it "set_token shoudl set the token" do
    contact = FactoryGirl.create(:individual)

    contact.set_token
    contact[:token].should_not eq(nil)
  end
  
  it "get_token should get the token" do
    contact = FactoryGirl.create(:individual)
    contact.set_token
    contact.get_token.should eq(contact[:token])
  end
  it "get_token should set a token even when set token is not explicitly called" do
    contact = FactoryGirl.create(:individual)
    contact.get_token.should_not eq(nil)
  end

  it "first_phone returns the first phone if available" do
    person = FactoryGirl.create(:individual)
    phone = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Phone')
    person.first_phone.should eq(phone.details)
  end
  it "first_phone returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.first_phone.should eq('')
  end

  it "second_phone returns the second phone if available" do
    person = FactoryGirl.create(:individual)
    phone1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Phone')
    phone2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Phone')
    person.second_phone.should eq(phone2.details)
  end
  it "second_phone returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.second_phone.should eq('')
  end

  it "third_phone returns the third phone if available" do
    person = FactoryGirl.create(:individual)
    phone1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Phone')
    phone2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Phone')
    phone3 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Phone')
    person.second_phone.should eq(phone3.details)
  end
  it "third_phone returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.third_phone.should eq('')
  end

  it "first_email returns the first email if available" do
    person = FactoryGirl.create(:individual)
    email = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Email')
    person.first_email.should eq(email.details)
  end
  it "first_email returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.first_email.should eq('')
  end

  it "second_email returns the second email if available" do
    person = FactoryGirl.create(:individual)
    email1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Email')
    email2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Email')
    person.second_email.should eq(email2.details)
  end
  it "second_email returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.second_email.should eq('')
  end

  it "third_email returns the third email if available" do
    person = FactoryGirl.create(:individual)
    email1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Email')
    email2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Email')
    email3 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Email')
    person.second_email.should eq(email3.details)
  end
  it "third_email returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.third_email.should eq('')
  end
  
  
  it "first_im returns the first im if available" do
    person = FactoryGirl.create(:individual)
    im = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    person.first_im.should eq(im.details)
  end
  
  
  it "first_im returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.first_im.should eq('')
  end

  it "second_im returns the second im if available" do
    person = FactoryGirl.create(:individual)
    im1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    im2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    person.second_im.should eq(im2.details)
  end
  it "second_im returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.second_im.should eq('')
  end

  it "third_im returns the third email if available" do
    person = FactoryGirl.create(:individual)
    im1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    im2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    im3 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    person.third_im.should eq(im3.details)
  end
  it "third_im returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.third_im.should eq('')
  end


  it "first_im_kind returns the first im if available" do
    person = FactoryGirl.create(:individual)
    im = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    person.first_im_kind.should eq(im.location)
  end
  
  
  it "first_im_kind returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.first_im_kind.should eq('')
  end

  it "second_im_kind returns the second im if available" do
    person = FactoryGirl.create(:individual)
    im1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    im2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    person.second_im_kind.should eq(im2.location)
  end
  it "second_im_kind returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.second_im_kind.should eq('')
  end

  it "third_im_kind returns the third email if available" do
    person = FactoryGirl.create(:individual)
    im1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    im2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    im3 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Im')
    person.third_im_kind.should eq(im3.location)
  end
  it "third_im_kind returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.third_im_kind.should eq('')
  end


  
  it "first_address returns the first address if available" do
    person = FactoryGirl.create(:individual)
    address = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Address')
    person.first_address.should_not eq('')
  end
  
  it "first_address returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.first_address.should eq('')
  end

  it "second_address returns the second address if available" do
    person = FactoryGirl.create(:individual)
    address1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Address')
    address2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Address')
    person.second_address.should_not eq('')
  end
  
  it "second_address returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.second_address.should eq('')
  end

  it "third_address returns the third address if available" do
    person = FactoryGirl.create(:individual)
    address1 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Address')
    address2 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Address')
    address3 = FactoryGirl.create(:contact_detail, :contact_id=>person.id, :type=>'Address')
    person.third_im.should eq(address3.details)
  end
  it "third_address returns nothing if not available" do
    person = FactoryGirl.create(:individual)
    person.third_address.should eq('')
  end
  
  
  describe "import_phone_attributes should associate correct attributes on" do
    it " mobile phone should be associated as cell" do
      row = {'Mobile Phone'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Cell',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end
    it " car phone should be associated as cell" do
      row = {'Car Phone'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Cell',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end
    
    it " other phone should be associated as other" do
      row = {'Other Phone'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Other',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    
    
    it " radio phone should be associated as cell" do
      row = {'Radio Phone'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Cell',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end
        
    it " primary phone should be associated as work" do
      row = {'Primary Phone'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Work',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    
    
    it " business phone should be associated as work" do
      row = {'Business Phone'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Work',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    
    
    it " company main phone should be associated as work" do
      row = {'Company Main Phone'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Work',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    
    
    it " business phone 2 should be associated as work" do
      row = {'Business Phone 2'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Work',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    
    
    it " pager phone should be associated as pager" do
      row = {'Pager'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Pager',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    
    
    it " business fax should be associated as work" do
      row = {'Business Fax'=>'100000'}
      expected = [{:type=>'Fax', :location=>'Work',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    
    
    it " home fax should be associated as home" do
      row = {'Home Fax'=>'100000'}
      expected = [{:type=>'Fax', :location=>'Home',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    
    
    it " other fax should be associated as other" do
      row = {'Other Fax'=>'100000'}
      expected = [{:type=>'Fax', :location=>'Other',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    

    it " home phone should be associated as home" do
      row = {'Home Phone'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Home',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    

    it " home phone 2 should be associated as home" do
      row = {'Home Phone 2'=>'100000'}
      expected = [{:type=>'Phone', :location=>'Home',  :details=>'100000'}]
      Contact.import_phone_attribs(row).should eq(expected)
    end    


  end

  describe "import_email_attributes should associate correct attributes on" do
    it " email address should be associated as work" do
      row = {'E-mail Address'=>'hi@email.com'}
      expected = [{:type=>'Email', :location=>'Work',  :details=>'hi@email.com'}]
      Contact.import_email_attribs(row).should eq(expected)
    end
    
    it " email address 2 should be associated as work" do
      row = {'E-mail 2 Address'=>'hi@email.com'}
      expected = [{:type=>'Email', :location=>'Work',  :details=>'hi@email.com'}]
      Contact.import_email_attribs(row).should eq(expected)
    end

    it " email address 3 should be associated as work" do
      row = {'E-mail 3 Address'=>'hi@email.com'}
      expected = [{:type=>'Email', :location=>'Work',  :details=>'hi@email.com'}]
      Contact.import_email_attribs(row).should eq(expected)
    end
    
  end


end
