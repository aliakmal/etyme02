require 'spec_helper'

describe Template do
  
  xit "parses {A/C REG} with the correct aircraft registration " do
    attr = {
              :title=>'Basic Template', 
              :details=>'ATTN....{AIRCRAFT_REG}...'
            }
    template = Template.create(attr)
    aircraft = FactoryGirl.create(:aircraft)
    template.parse(aircraft)
    template.parsed_text.should eq('ATTN....'+aircraft[:ac_reg].to_s+'...')
  end
  
end
