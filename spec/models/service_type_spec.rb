require 'spec_helper'

describe ServiceType do
  it "by_name returns a list of all service_types with matching desc as per search term" do
    search_service_type_1 = FactoryGirl.create(:service_type, :desc=>'SYRIA OVF')
    search_service_type_2 = FactoryGirl.create(:service_type, :desc=>'SYRIA GROUND HANDLING')
    no_search_search_service_type = FactoryGirl.create(:service_type, :desc=>'CHINA OVF')
    
    ServiceType.by_name('SYRIA').should eq([search_service_type_1, search_service_type_2])
  end
  
  it "returns true if it has a preferred contact" do
    contact = FactoryGirl.create(:company)
    search_service_type = FactoryGirl.create(:service_type, :desc=>'SYRIA OVF', :prefered_contact_id=>contact.id)
    search_service_type.has_prefered_contact.should eq(true)
  end
  
  it "returns false if it has no preferred contact" do
    search_service_type = FactoryGirl.create(:service_type, :desc=>'SYRIA OVF', :prefered_contact_id=>nil)
    search_service_type.has_prefered_contact.should eq(false)
  end
  
  it "returns the prefered contact name if it has a preferred contact" do
    contact = FactoryGirl.create(:company)
    search_service_type = FactoryGirl.create(:service_type, :desc=>'SYRIA OVF', :prefered_contact_id=>contact.id)
    search_service_type.get_prefered_contact_name.should eq(contact[:company_name])
  end
  
  it "returns a message if it has no preferred contact" do
    search_service_type = FactoryGirl.create(:service_type, :desc=>'SYRIA OVF', :prefered_contact_id=>nil)
    search_service_type.get_prefered_contact_name.should eq('No Prefered Contact')
  end
end
