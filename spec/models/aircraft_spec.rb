require 'spec_helper'

describe Aircraft do
  before(:each) do
    @attr = { 
      :ac_reg => "GB7612",
      :ac_type => "BOEING 747",
      :mtow => 10000
    }
  end
  
  
  it "can create an operator from it" do
    aircraft = Aircraft.new(@attr)
    aircraft.build_operator
  end
  
  it "associates with an opertor when the id of that operator is referred to it" do
    one_operator = FactoryGirl.create(:operator)
    one_aircraft = Aircraft.create(@attr.merge(:operator_id => one_operator.id))
    one_aircraft.operator.should eq(one_operator)
  end
  
  it "by_owner returns all aircrafts owned by a certain operator" do
    one_operator = FactoryGirl.create(:operator)
    operators_aircraft_1 = Aircraft.create(@attr.merge(:operator_id => one_operator.id))
    operators_aircraft_2 = Aircraft.create(@attr.merge(:operator_id => one_operator.id))
    
    not_operators_aircraft = Aircraft.create(@attr)
    
    Aircraft.by_owner(one_operator.id).should eq([operators_aircraft_1, operators_aircraft_2])
        
  end
  
  it "by_registration returns the list of aircrafts with matching registrations to the search term" do
    one_operator = FactoryGirl.create(:operator)

    search_aircraft_1 = Aircraft.create(@attr.merge(:operator_id => one_operator.id, :ac_reg=>'ABD111'))
    search_aircraft_2 = Aircraft.create(@attr.merge(:operator_id => one_operator.id, :ac_reg=>'ABC111'))
    no_search_aircraft = Aircraft.create(@attr.merge(:operator_id => one_operator.id, :ac_reg=>'ZXC111'))

    Aircraft.by_reg('AB').should eq([search_aircraft_1, search_aircraft_2])
  end
  
end
