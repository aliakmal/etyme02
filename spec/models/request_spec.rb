require 'spec_helper'

describe Request do
  
  before(:each) do
    @customer = FactoryGirl.create(:company)
    @operator = FactoryGirl.create(:company)
    @schedule = "12MAY2012 FLTNO OMRK 1200 OMDB 2300
    13MAY2012 FLTNO OMRK 1200 OMDB 2300
    14MAY2012 FLTNO OMRK 1200 OMDB 2300
    15MAY2012 FLTNO OMRK 1200 OMDB 2300"

    @attr = {
      :date_of_request => Date.today, 
      :date_due => Date.tomorrow,
      :schedule => @schedule, 
      :aircraft_reg => 'Sh123', 
      :is_substitute =>'USE SUBSTITUTE',
      :customer_id => @customer[:id], 
      :operator_id => @operator[:id]
      }
  end
  it "should create a new instance of a request" do
    lambda{
      Request.create(@attr)
    }.should change(Request, :count)
  end
    
  it "on save sets ref equivalent to 0000<ID>/<CURRENT YEAR>" do
    Request.delete_all
    @request_01 = Request.new(@attr)
    @request_01.save
    
    request = Request.create(@attr)
    request[:ref].should eq("REQ-000002")
  end
  
    it "on save sets the ref equivalent to the largest ref value of latest added request even if there has been a deletion" do
    Request.delete_all
    @request_01 = Request.create(@attr)
    @request_02 = Request.create(@attr)
    Request.delete(@request_02[:id])
    
    request = Request.create(@attr)
    request[:ref].should eq("REQ-000002")
  end

  
  it "for the first record ever inserted on save sets ref equivalent to 000001" do
    Request.delete_all
    request = Request.create(@attr)
    request.ref.should eq("REQ-000001")
  end
  it "setting contact_name with a name of a non existing contact creates a new instance of a contact with the same name" do
     request = Request.create(@attr.merge(:customer_name=>'Road Runner Co.'))   
     request.customer.name.should eq('Road Runner Co.')     
  end
  
  it "default status of the request should be current" do
    request = Request.create(@attr)
    request.request_status.should eq('current')
  end
  describe "schedule generation" do
    it "when saving a schedule splits the schedule string into lines and creates a sector for each line in the string" do
      lambda{
        
        attr = @attr.merge(:schedule => @schedule)
        request_with_sectors = Request.create(attr)
        request_with_sectors.generate_sectors
      }.should change(Sector, :count).by(4)
    end  
  end
  
  it "services returns all services available for a request" do
    _request = Request.create(@attr)
    _request.sectors.build(FactoryGirl.attributes_for(:sector))
    one_service = FactoryGirl.create(:service, :sector_id=>_request.sectors.first[:id], :service_status=>'done')
    two_service = FactoryGirl.create(:service, :sector_id=>_request.sectors.first[:id])
    three_service = FactoryGirl.create(:service, :sector_id=>_request.sectors.first[:id])
    _request.services.should eq([one_service, two_service, three_service])
  end
  
  it "num_services returns number of all services available for a request" do
    _request = Request.create(@attr)
    _request.sectors.build(FactoryGirl.attributes_for(:sector))
    one_service = FactoryGirl.create(:service, :sector_id=>_request.sectors[0][:id], :service_status=>'done')
    two_service = FactoryGirl.create(:service, :sector_id=>_request.sectors[0][:id])
    three_service = FactoryGirl.create(:service, :sector_id=>_request.sectors[0][:id])
    _request.num_services.should eq(3)
  end

  it "num_services_done returns the number of done services for a request" do
    _request = Request.create(@attr)
    _request.sectors.build(FactoryGirl.attributes_for(:sector))
    one_service = FactoryGirl.create(:service, :sector_id=>_request.sectors[0][:id], :service_status=>'done')
    two_service = FactoryGirl.create(:service, :sector_id=>_request.sectors[0][:id])
    three_service = FactoryGirl.create(:service, :sector_id=>_request.sectors[0][:id])
    _request.num_services_done.should eq(1)
  end

  it "when entering the registration of a non existing aircraft must create a new aircraft" do
    lambda{
      request = Request.create(@attr.merge(:aircraft_reg=>'NEWGH7161'))   
    }.should change(Aircraft, :count)
  end
  it "when entering the name of a new operator should create a new operator" do
    lambda{
      pp @attr.merge(:operator_name=>'GHANA BASED OPS', :operator_id => nil)
      request = Request.create(@attr.merge(:operator_name=>'GHANA BASED OPS', :operator_id => ''))   
    }.should change(Company, :count)
  end
  
  it "when entering the name of an existing operator should associate with that operator" do
    operator = FactoryGirl.create(:company, :company_name=>'CUSTOMER NAME 03', :name=>'CUSTOMER NAME 03')
    request = Request.create!(@attr.merge(:customer_name=>'Road Runner Co.', :operator_name=>'CUSTOMER NAME 03', :aircraft_reg=>'GH7161'))   
    request.operator.should eq(operator)
  end

  it "when saving a request and adding a new aircraft - the aircraft should be associated with the operator if it is an existing operator or a new one" do
    request = Request.create(@attr)
    #request.reload
    request.operator.should eq(request.aircraft.operator)
  end
  
  it "when the name of a contact is provided in customer name on save the contact should equal the customer" do
    customer = FactoryGirl.create(:company, :company_name=>'CUSTOMER NAME 02', :name=>'CUSTOMER NAME 02')
    request = Request.create(@attr.merge(:customer_name=>customer.name))
    request.customer.should eq(customer)
  end
  
    it "when the name of a contact is provided in customer name on initialize the contact should equal the customer" do
    customer = FactoryGirl.create(:company, :company_name=>'CUSTOMER NAME 01', :name=>'CUSTOMER NAME 01')
    request = Request.new(@attr.merge(:customer_name=>customer.name))
    request.customer.should eq(customer)
  end

  it "request status can be changed from current to stalled" do
    request = Request.create(@attr)
    request.current_to_stalled
    request.reload
    request[:request_status].should eq('stalled')
  end    
  
  it "request status can be changed from awaiting_movement to stalled" do
    request = Request.create(@attr.merge(:request_status=>'awaiting_movement'))
    request.awaiting_movement_to_stalled
    request.reload
    request[:request_status].should eq('stalled')
  end    

  it "request status can be changed from stalled to awaiting_movement" do
    request = Request.create(@attr.merge(:request_status=>'stalled'))
    request.stalled_to_awaiting_movement
    request.reload
    request[:request_status].should eq('awaiting_movement')
  end    
  it "request status can be changed from stalled to current" do
    request = Request.create(@attr.merge(:request_status=>'stalled'))
    request.stalled_to_current
    request.reload
    request[:request_status].should eq('current')
  end  
end
