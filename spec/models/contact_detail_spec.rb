require 'spec_helper'

describe ContactDetail do
  it "address should have an instance of an address detail " do
    company = Company.new
    address = company.addresses.build
    pp address.address_detail
    address.should respond_to(:address_detail)
  end
end
