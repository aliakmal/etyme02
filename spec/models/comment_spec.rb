require 'spec_helper'

describe Comment do

  
  describe 'validation' do
    it { should validate_presence_of :comment}
    it { should validate_presence_of :user_id}
  end

  describe "associations" do
    it { should belong_to :user}
  end

  describe "commentables" do
    [:aircraft,:request].each do |model|
      let(:commentable) { FactoryGirl.create model }
      subject{ commentable }

      it "#{model} should be commentable" do
        comment = subject.comments.create(comment: 'this is a comment')
        subject.comments.should eq([comment])
      end      

    end
  end

end
