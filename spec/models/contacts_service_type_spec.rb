require 'spec_helper'

describe ContactsServiceType do
  it "return the title of the template if there is a template associated" do
    template = FactoryGirl.create(:template)
    contact_service_type = FactoryGirl.create(:contacts_service_type, :template_id => template.id)
    contact_service_type.template.should eq(template)
  end
end
