require "spec_helper"

describe ServicesHelper do
  
  describe "show_links_based_on_status if link is cancel" do

    it "should return false if status is cancelled" do
      show_links_based_on_status('cancelled', 'cancel').should eq(false)
    end
    
    it "should return false if status is done" do
      show_links_based_on_status('done', 'cancel').should eq(false)
    end
    
  end

  describe "show_links_based_on_status if link is complete" do

    it "should return false if status is new" do
      show_links_based_on_status('new', 'complete').should eq(false)
    end
    
    it "should return false if status is done" do
      show_links_based_on_status('done', 'complete').should eq(false)
    end
    
  end
  
  describe "show_links_based_on_status if link is pending" do

    it "should return false if status is cancelled" do
      show_links_based_on_status('cancelled', 'pending').should eq(false)
    end
    
  end
  
    it "should return true otherwise" do
      show_links_based_on_status('cancelled', 'done').should eq(true)
    end
end