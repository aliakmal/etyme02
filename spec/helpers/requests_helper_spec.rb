require "spec_helper"

describe RequestsHelper do
  describe 'css_class_from_status' do
    it "should return nothing if status is current" do
      css_class_from_status('current').should eq(nil)
    end
    
    it "should return warning if status is awaiting_movement" do
      css_class_from_status('awaiting_movement').should eq('warning')
    end
    
    it "should return success if status is done" do
      css_class_from_status('done').should eq('success')
    end

    it "should return error if status is cancelled" do
      css_class_from_status('cancelled').should eq('danger')
    end
  end
end