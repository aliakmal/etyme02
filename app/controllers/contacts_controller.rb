class ContactsController < ApplicationController
  # GET /contacts
  # GET /contacts.json
 load_and_authorize_resource
  has_scope :starts_with
  has_scope :by_name
  has_scope :by_type
  has_scope :by_company_type
  has_scope :by_services, :type => :array

  # uncomment once you get the caching figured out here
  
  #caches_action :index #, :cache_path => Proc.new { |c| c.params }
  #cache_sweeper :contact_sweeper

  def index
    @tenant = current_tenant
    @acontacts = Contact.where(:tenant_id => current_tenant.id)
    @contacts = apply_scopes(@acontacts).reorder(sort_column + ' ' + sort_direction).page(params[:page]).per(6)
    @all_contacts = apply_scopes(@acontacts).reorder(sort_column + ' ' + sort_direction).all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contacts }
      format.csv { send_data Contact.to_csv(@all_contacts) }
      format.js
    end
  end

  def send_email
    ids = params[:contact_ids]
    subject = params[:subject]
    message = params[:message_body].first
    sms = params[:virtual]
    username = current_user.username
    email = current_user.email
    file = params[:uploaded_file]
    ids.each do |contact|
      con = Contact.find(contact)
      if con != nil
        if con.type =='Individual'
          if con.user_id != nil
            address = con.user.email
            ClientMailer.etyme_mail(address,subject,message,username,email,file).deliver
          elsif con.user_id == nil and (con.ind_email != nil or con.ind_email != '')
            address = con.ind_email
            ClientMailer.etyme_mail(address,subject,message,username,email,file).deliver
          else
            flash[:notice] = "Some of the contacts does not have any email associated"
          end
        elsif con.type == 'Company'
          if !con.email.empty?
            address = con.email.first.details
            ClientMailer.etyme_mail(address,subject,message,username,email,file).deliver
          else
            flash[:notice] = "Some of the contacts does not have any email associated"
          end
        end
      else
        flash[:notice] = "No email found for selected contacts "
      end
    end
     redirect_to contacts_path
  end

  def get_activity_stream
    @contact = Contact.find(params[:id])

    @history = @contact.activities + @contact.activities_as_recipient
    @history = @history.sort_by { |hsh| hsh[:created_at] }
    @history = @history.reverse

    if request.xhr?
      render :partial=>'layouts/activity_stream', :locals => { :history => @history }, :layout => false
    end
  end

  def search
    @contacts = Contact.by_name(params[:term])
    respond_to do |format|
      format.json {render :json => @contacts.as_json(:only => [:id, :name]) }
    end
  end

  def import
    expire_action :action => :index  
  
    Contact.import(params[:file])
    redirect_to contacts_url, notice: "Contacts imported."
  end

  # GET /contacts/new
  # GET /contacts/new.json
  def new
    @contact = Contact.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contact }
    end
  end

  # GET /contacts/1/edit
  def edit
    @contact = Contact.find(params[:id])
  end

  # POST /contacts
  # POST /contacts.json
  def create
    expire_action :action => :index  
    @contact = Contact.new(params[:contact])

    respond_to do |format|
      if @contact.save
        format.html { redirect_to @contact, notice: 'Contact was successfully created.' }
        format.json { render json: @contact, status: :created, location: @contact }
      else
        format.html { render action: "new" }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /contacts/1
  # PUT /contacts/1.json
  def update
    expire_action :action => :index  
    @contact = Contact.find(params[:id])

    respond_to do |format|
      if @contact.update_attributes(params[:contact])
        format.html { redirect_to @contact, notice: 'Contact was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contact.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contacts/1
  # DELETE /contacts/1.json
  def destroy
    expire_action :action => :index  
    @contact = Contact.find(params[:id])
    @contact.destroy

    respond_to do |format|
      format.html { redirect_to contacts_url }
      format.json { head :no_content }
    end
  end
  
  # GET /requests/1/current_to_awaiting_movement
  def send_form_for_requesting_update
    expire_action :action => :index  
    contact = Contact.find(params[:id])
    #require 'net/http'

    respnse = true

    begin
      contact.set_token

      if contact[:type] == 'Operator' then
        ClientMailer.request_operator_update(contact.email.map(&:details), contact, current_user).deliver
      elsif contact[:type] == 'Company' then
        ClientMailer.request_contact_update(contact.email.map(&:details), contact, current_user).deliver
      elsif contact[:type] == 'Individual' then
        ClientMailer.request_individual_update(contact.email.map(&:details), contact, current_user).deliver
      end
    rescue Net::SMTPFatalError => e
      respnse = false
      contact.delete_token

      if request.xhr?
        respond_to do |format|
          format.json { head :no_content, status: :unprocessable_entity }
        end
        return
      end
    end

    if request.xhr?
      respond_to do |format|
        format.html {render :partial=>'contacts/pending_client_update_nubbin', :layout => false, status: :created, notice: 'Email has been sent to client'}
      end
    end
  end

  # GET /requests/1/current_to_awaiting_movement
  def send_form_for_requesting_multiple_update
    expire_action :action => :index  
    contacts = Contact.find(params[:all_ids].split(','))

    contacts.each do |contact|
      begin
        contact.set_token

        if contact[:type] == 'Operator' then
          ClientMailer.request_operator_update(contact.email.map(&:details), contact, current_user).deliver
        elsif contact[:type] == 'Company' then
          ClientMailer.request_contact_update(contact.email.map(&:details), contact, current_user).deliver
        elsif contact[:type] == 'Individual' then
          ClientMailer.request_individual_update(contact.email.map(&:details), contact, current_user).deliver
        end
      rescue Net::SMTPFatalError => e
        contact.delete_token
      end
    end

    respond_to do |format|
      format.html { redirect_to :back, notice: 'Emails have been sent to contacts.' }
    end

  end
  def vendor_companies
    @contacts = Company.unscoped.where(:company_type => company_type,:tenant_id => current_tenant.id)
  end
  
  private  
  helper_method :sort_column, :sort_direction  
  def sort_column
    params[:sort] || "name"  
  end

  def sort_direction
    %w[asc desc].include?(params[:direction])? params[:direction]:'asc'
  end

end
