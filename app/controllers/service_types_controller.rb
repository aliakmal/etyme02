class ServiceTypesController < ApplicationController
  # GET /service_types
  # GET /service_types.json
  #authorize_resource
  load_and_authorize_resource
  
  has_scope :search

  
  def index
    @service_types = apply_scopes(ServiceType).page(params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @service_types }
    end
  end
  def search
    @service_types = ServiceType.by_name(params[:term])
    respond_to do |format|
      format.json {render :json => @service_types.as_json(:only => [:id, :desc]) }
    end 

  end
  # GET /service_types/1
  # GET /service_types/1.json
  def show
    @service_type = ServiceType.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @service_type }
    end
  end

  # GET /service_types/new
  # GET /service_types/new.json
  def new
    @service_type = ServiceType.new
    @contacts = []
   

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @service_type }
    end
  end

  # GET /service_types/1/edit
  def edit
    @service_type = ServiceType.find(params[:id])
    @contacts = Contact.by_services([@service_type.id])
  end

  # POST /service_types
  # POST /service_types.json
  def create
    @service_type = ServiceType.new(params[:service_type])
    @contacts = []

    respond_to do |format|
      if @service_type.save
        format.html { redirect_to @service_type, notice: 'Service type was successfully created.' }
        format.json { render json: @service_type, status: :created, location: @service_type }
      else
        format.html { render action: "new" }
        format.json { render json: @service_type.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /service_types/1
  # PUT /service_types/1.json
  def update
    @service_type = ServiceType.find(params[:id])
    @contacts = Contact.by_services([@service_type.id])
    
    respond_to do |format|
      if @service_type.update_attributes(params[:service_type])
        format.html { redirect_to @service_type, notice: 'Service type was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @service_type.errors, status: :unprocessable_entity }
      end
    end
  end
  
  def assign_templates
    @service_types = ServiceType.find(params[:service_type_ids])
    @service_types.each do |one_service|
      one_service.update_attributes({:id=>one_service[:id], :default_template_id=>params[:default_template_id]})
    end

    respond_to do |format|
      format.html { redirect_to :back, notice: 'Service Types Default templates have been set.' }
    end
  end

  # DELETE /service_types/1
  # DELETE /service_types/1.json
  def destroy
    @service_type = ServiceType.find(params[:id])
    @service_type.destroy

    respond_to do |format|
      format.html { redirect_to service_types_url }
      format.json { head :no_content }
    end
  end
  
  private  
  helper_method :sort_column, :sort_direction  
  def sort_column
    params[:sort] || "desc"  
  end

  def sort_direction
    %w[asc desc].include?(params[:direction])? params[:direction]:'asc'
  end
end
