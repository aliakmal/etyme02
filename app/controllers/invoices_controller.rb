class InvoicesController < ApplicationController
  # GET /invoices
  # GET /invoices.json
  load_and_authorize_resource
  def index
    # auto_invoicing
    @invoices = Invoice.page(params[:page]).per(10)
    

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @invoices }
    end
  end

  # GET /invoices/1
  # GET /invoices/1.json
  def show
    @invoice = Invoice.find(params[:id])
    @contract = @invoice.timesheets.first.contract

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @invoice }
    end
  end

  # GET /invoices/new
  # GET /invoices/new.json
  def new

    @timesheets = Timesheet.find(params[:tid])
    @contract = @timesheets.first.contract
    @invoice = Invoice.new(:timesheets=>@timesheets)
    
    @invoice.startup

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @invoice }
    end
  end

  # GET /invoices/1/edit
  def edit
    @invoice = Invoice.find(params[:id])
  end

  # POST /invoices
  # POST /invoices.json
  def create
    @invoice = Invoice.new(params[:invoice])
    @contract = Contract.find(params[:invoice][:con_id])
    if @contract.due_invoice_day != nil
      @invoice.due_date = (Time.current.beginning_of_month + (@contract.due_invoice_day.days - 1.days)) + @contract.payment_terms.to_i.days
    end
    total_mins = 0
    timesheets= Timesheet.where(:contract_id => @contract.id,:state => 'approved')
    timesheets.each do |timesheet|
      total_mins += timesheet.minutes
    end
    @contract.customer_price.each do |price|
      if Time.now <= price.end_date
          @total_rate = (total_mins.to_i / (60)) * price.sale_price
      else
        next
      end
    end
    @invoice.setup_rate(@total_rate)
    @invoice.change_state_of_timesheets
    @invoice.state = 'invoiced'
    @invoice.total = total_mins
    @invoice.contract_id = @contract.id
    respond_to do |format|
      if @contract.update_attributes(params[:contract])
        if @invoice.save
          format.html { redirect_to @invoice, notice: 'Invoice was successfully created.' }
          format.json { render json: @invoice, status: :created, location: @invoice }
        else
          format.html { render action: "new" }
          format.json { render json: @invoice.errors, status: :unprocessable_entity }
        end
      end
    end
  end

  # PUT /invoices/1
  # PUT /invoices/1.json
  def update
    @invoice = Invoice.find(params[:id])

    respond_to do |format|
      if @invoice.update_attributes(params[:invoice])
        format.html { redirect_to @invoice, notice: 'Invoice was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @invoice.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /invoices/1
  # DELETE /invoices/1.json
  def destroy
    @invoice = Invoice.find(params[:id])
    @invoice.destroy

    respond_to do |format|
      format.html { redirect_to invoices_url }
      format.json { head :no_content }
    end
  end
  def clear_invoices
    @invoices = Invoice.all
    @invoices.each do |invoice|
      if invoice.state == 'pending'
        invoice.state = 'invoiced'
        invoice.save
      else
        next
      end
    end
    redirect_to invoices_path, :notice => 'All pending invoices cleared'
  end
  def auto_invoicing
    @contracts = Contract.all
    @charge_customer = 0
    total_minutes = 0
    @invoiced_timesheets = Array.new
    @contracts.each do |contract|
        if contract.due_invoice_day != nil and (Time.now <= contract.customer_price.last.end_date)
         if Time.now.to_date >= (Time.current.beginning_of_month + (contract.due_invoice_day.days-1.days)).to_date
          timesheets= Timesheet.where(:contract_id => contract.id,:state => 'approved')
          if !timesheets.empty?
            timesheets.each do |timesheet|
              timesheet.time_entries.each do |time_entry|
                time_entry.rate = time_entry.charge_customer
                @charge_customer += time_entry.charge_customer
                time_entry.save
              end
              timesheet.invoiceit
              @invoiced_timesheets << timesheet
              timesheet.save
              
            end
             invoice = Invoice.create(:total_rate => @charge_customer, :state => 'pending')
             @invoiced_timesheets.each do |itimesheet|
              # binding.pry
                itimesheet.invoice_id = invoice.id
                total_minutes += itimesheet.minutes
                # binding.pry
                itimesheet.save
                # binding.pry
             end
            if contract.due_invoice_day != nil
              invoice.due_date = (Time.current.beginning_of_month + (contract.due_invoice_day.days-1.days)) + contract.payment_terms.to_i.days
            end
             invoice.contract_id = contract.id
             invoice.total = total_minutes
             # binding.pry
             invoice.save
             @charge_customer = 0
             total_minutes =0
             @invoiced_timesheets.clear
          else
            next
          end
        else
          next
        end
      end
    end
    redirect_to invoices_path, :notice => 'All pending invoices generated'
  end
end
