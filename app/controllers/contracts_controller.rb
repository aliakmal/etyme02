class ContractsController < ApplicationController

  include Calendrier::EventExtension
  authorize_resource :class => false
  skip_authorize_resource :only => [:my_contracts,:show]
  # GET /contracts
  # GET /contracts.json
  def index
    @contracts = Contract.page(params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @contracts }
    end
  end

  def my_contracts
    @contracts = Contract.by_candidate(current_user).page(params[:page])

    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render json: @contracts }
    end
  end

  # GET /contracts/1
  # GET /contracts/1.json
  def show
    @contract = Contract.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @contract }
    end
  end

  # GET /contracts/new
  # GET /contracts/new.json
  def new
    @contract = Contract.new(:candidate_id=>params[:candidate_id], :job_id => params[:job_id])
    @contract.billable_periods.build
    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @contract }
    end
  end

  # GET /contracts/1/edit
  def edit
    @contract = Contract.find(params[:id])
    commission_users_list
    @client_companies = Company.where(:company_type => 'Client')
    @vendor_companies = Company.where(:company_type => 'Vendor')
    @acandidates = Individual.all
    @ajobs = Job.all
    if @contract.job.company_id != nil
      @comp = Company.find(@contract.job.company_id)
    else
      @comp
    end
    @cand = Array.new
    if @contract.job_id != nil
      @job = Job.find(@contract.job_id)
      @job.inquiries.each do |inquiry|
        if inquiry.individual.is_hired_for_job(@job)
          @cand << inquiry.individual
        end
      end
    else
      @cand
    end
  end

  # POST /contracts
  # POST /contracts.json30
  def create
    @contract = Contract.new(params[:contract])
    # @contract.last_payment_date = @contract.contract_start_date
    # if current_tenant.salary_terms == 'Weekly'
    #   @contract.due_payment_date = (@contract.last_payment_date + 7.days)
    # elsif  current_tenant.salary_terms == 'Monthly'
    #   @contract.due_payment_date = (@contract.last_payment_date + 30.days)
    # end 
    # @contract.prices.each do |price|   
    #   if @contract.job.company.company_type == 'Client' and price.sale_price != nil
    #     price.price_type = 'Customer'
    #   elsif @contract.individual.company == nil and price.cost_price != nil
    #     price.price_type = 'Employee'
    #   elsif @contract.individual.company.company_type == 'Vendor' and price.cost_price != nil
    #     price.price_type = 'Vendor'
    #   else
    #     price.price_type = 'Unknown vendor/employee'
    #   end
    # end
    # binding.pry
    respond_to do |format|
      if @contract.save
        format.html { redirect_to @contract, notice: 'Contract was successfully created.' }
        format.json { render json: @contract, status: :created, location: @contract }
      else
        @acandidates = Individual.all
        @ajobs = Job.all
        commission_users_list
        inquiry_candidates
        # binding.pry
        format.html { render action: "new_contract", notice: 'Cannot create a new contract #{@contracts.errors[:base].to_s}'}
        format.json { render json: @contract.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /contracts/1
  # PUT /contracts/1.json
  def update
    @contract = Contract.find(params[:id])

    respond_to do |format|
      if @contract.update_attributes(params[:contract])
        format.html { redirect_to @contract, notice: 'Contract was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @contract.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /contracts/1
  # DELETE /contracts/1.json
  def destroy
    @contract = Contract.find(params[:id])
    # associated_commissions = @contract.commissions.all
    @contract.destroy
    # if !associated_commissions.empty?
    #   associated_commissions.each do |commission|
    #     Commission.destroy(commission.id)
    #   end
    # end
    respond_to do |format|
      format.html { redirect_to contracts_url }
      format.json { head :no_content }
    end
  end

  def new_contract
    if current_tenant.salary_terms != nil
      @contract = Contract.new(:candidate_id=>params[:candidate_id], :job_id => params[:job_id])
      commission_users_list
      # binding.pry
      # @contract.billable_periods.build
      # @contract.commissions.build
      # @contract.prices.build
      # @contract.attachments.build
      @client_companies = Company.where(:company_type => 'Client')
      @vendor_companies = Company.where(:company_type => 'Vendor')
      @acandidates = Individual.all
      @ajobs = Job.all
      if params[:company_id] != nil
        @comp = Company.find(params[:company_id])
      else
        @comp
      end
      @cand = Array.new
      if params[:job_id] != nil
        @job = Job.find(params[:job_id])
        @job.inquiries.each do |inquiry|
          if !inquiry.individual.is_hired_for_job(@job)
            @cand << inquiry.individual
          end
        end
      else
        @cand
      end
      
      respond_to do |format|
        format.html # new.html.erb
        format.js
        format.json { render json: @contract }
      end
    else
      redirect_to tenants_path, notice: 'No salary terms.Please set the Salary Terms first'
    end
  end
  def get_candidates
    @job = Job.find(params[:id])
    @names = Array.new
    @job.inquiries.each do |inquiry|
      if !inquiry.individual.is_hired_for_job(@job)
        @names << inquiry.individual
      end
    end
    respond_to do |format|
      format.json { render json: @names }
    end
  end
  def get_customer
    @job = Job.find(params[:id])
    @customer = Company.where(:id => @job.company_id)
    respond_to do |format|
      format.json { render json: @customer }
    end
  end
  def cancel_contract
    contract = Contract.find(params[:id])
    if contract.employee_type == 'employee_w2'
      contract.customer_price.last.update_attributes(:end_date => Time.now)
      contract.employee_price.last.update_attributes(:end_date => Time.now)
      contract.update_attributes(:status => 'Cancelled')
    else
      contract.customer_price.last.update_attributes(:end_date => Time.now)
      contract.vendor_price.last.update_attributes(:end_date => Time.now)
      contract.update_attributes(:status => 'Cancelled')
      # contract.save
    end
    redirect_to :back, :notice => 'Contract Cancelled'
  end

  private
  helper_method :commission_users_list, :inquiry_candidates
  def commission_users_list
    roles = Role.find(:all, :conditions => ["name != \'admin\' AND name != \'candidate\' AND name != \'trainee\' AND resource_id = #{current_tenant.id}"])
    commission_users = Array.new
    roles.each do |role|
       user = User.with_role role.name, current_tenant
       if !user.empty?
        commission_users.concat user
      end
    end
    @names = Array.new
    all_individuals = Individual.all
    
    commission_users.each do |user|
       all_individuals.each do |individual|
        if user.id == individual.user_id
          @names << individual
        end
      end
    end
    @names.uniq!
  end
  def inquiry_candidates
    @cand = Array.new
    if params[:job_id] != nil
      @job = Job.find(params[:job_id])
      @job.inquiries.each do |inquiry|
        if !inquiry.individual.is_hired_for_job(@job)
          @cand << inquiry.individual
        end
      end
    else
      @cand = Individual.all
    end
  end
end
