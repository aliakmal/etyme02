class MessagesController < ApplicationController
  authorize_resource :class => false
  skip_authorize_resource :only => [:new,:create]
  skip_before_filter :authenticate_tenant!, :only => [:new,:create]
  skip_before_filter :authenticate_user!, :only => [:new,:create]
  def new
    @message = Message.new
  end

  def create
    @message = Message.new(params[:message])
      if @message.valid?
        # TODO send message here
        name = params[:message][:name]
        email = params[:message][:email]
        subject = params[:message][:subject]
        message = params[:message][:message]
        ClientMailer.drop_email(subject,message,name,email).deliver
        flash[:notice] = "Message sent! Thank you for contacting us."
        redirect_to root_url
      else
        render :layout => 'main'
      end
  end
end
