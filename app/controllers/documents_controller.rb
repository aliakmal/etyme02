class DocumentsController < ApplicationController
  # GET /documents
  # GET /documents.json
  load_and_authorize_resource
  def index
    @documents = Document.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @documents }
    end
  end

  def listings
    @documents = Document.not_by_documentable(Documentable.new(:documentable_type=>params[:dType], :documentable_id=>params[:dId]))

    respond_to do |format|
      if request.xhr?
        format.html { render :layout => false}
      else
        format.html # index.html.erb
        format.json { render json: @documents }
      end
    end
  end
  
  def attach
    @documents = Document.find(params[:document_ids])
    @documents.each do |one_document|
      one_document.attach(params[:dType], params[:dId])
    end

    respond_to do |format|
      if request.xhr?
        #format.html { render :layout => false}
        format.js
      else
        format.html # index.html.erb
        format.json { render json: @documents }
      end      
      #format.html { redirect_to :back, notice: 'Service Types Default templates have been set.' }
    end
  end

  def detach
    document = Documentable.find(params[:id])
    document.destroy

    respond_to do |format|
      if request.xhr?
      format.json { head :no_content }
      else
        format.html # index.html.erb
        format.json { render json: @documents }
      end
    end
  end

  # GET /documents/1
  # GET /documents/1.json
  def show
    @document = Document.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @document }
    end
  end

  # GET /documents/new
  # GET /documents/new.json
  def new
    @document = Document.new

    if !params[:dType].blank? then
      @document.documentables.build(:documentable_type => params[:dType], :documentable_id => params[:dId])
    end

    respond_to do |format|
      if request.xhr?
        format.html { render :layout => false}
      else
        format.html # new.html.erb
        format.json { render json: @document }
      end
    end
  end

  # GET /documents/1/edit
  def edit
    @document = Document.find(params[:id])
    respond_to do |format|
      if request.xhr?
        format.html { render :layout => false}
      else
        format.html # new.html.erb
        format.json { render json: @document }
      end
    end
  end

  # POST /documents
  # POST /documents.json
  def create
    @document = Document.new(params[:document])

    respond_to do |format|
      if @document.save
        if request.xhr? || remotipart_submitted?
          #format.html{render :layout => false}#, :template => (params[:template] == 'escape' ? 'comments/escape_test' : 'comments/create'), :status => (@comment.errors.any? ? :unprocessable_entity : :ok)
          format.js
        else
          format.html { redirect_to @document, notice: 'Document was successfully created.' }
          format.json { render json: @document, status: :created, location: @document }
        end
      else
        format.html { render action: "new" }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /documents/1
  # PUT /documents/1.json
  def update
    @document = Document.find(params[:id])

    respond_to do |format|
      if @document.update_attributes(params[:document])
        if request.xhr? || remotipart_submitted?
          format.js
        else
          format.html { redirect_to @document, notice: 'Document was successfully updated.' }
          format.json { head :no_content }
        end
      else
        format.html { render action: "edit" }
        format.json { render json: @document.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /documents/1
  # DELETE /documents/1.json
  def destroy
    @document = Document.find(params[:id])
    @document.destroy

    respond_to do |format|
      format.html { redirect_to documents_url }
      format.json { head :no_content }
    end
  end
end