class BillablePeriodsController < ApplicationController
  # GET /billable_periods
  # GET /billable_periods.json
  load_and_authorize_resource
  def index
    @billable_periods = BillablePeriod.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @billable_periods }
    end
  end

  # GET /billable_periods/1
  # GET /billable_periods/1.json
  def show
    @billable_period = BillablePeriod.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @billable_period }
    end
  end

  # GET /billable_periods/new
  # GET /billable_periods/new.json
  def new
    @billable_period = BillablePeriod.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @billable_period }
    end
  end

  # GET /billable_periods/1/edit
  def edit
    @billable_period = BillablePeriod.find(params[:id])
  end

  # POST /billable_periods
  # POST /billable_periods.json
  def create
    @billable_period = BillablePeriod.new(params[:billable_period])

    respond_to do |format|
      if @billable_period.save
        format.html { redirect_to @billable_period, notice: 'Billable period was successfully created.' }
        format.json { render json: @billable_period, status: :created, location: @billable_period }
      else
        format.html { render action: "new" }
        format.json { render json: @billable_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /billable_periods/1
  # PUT /billable_periods/1.json
  def update
    @billable_period = BillablePeriod.find(params[:id])

    respond_to do |format|
      if @billable_period.update_attributes(params[:billable_period])
        format.html { redirect_to @billable_period, notice: 'Billable period was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @billable_period.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /billable_periods/1
  # DELETE /billable_periods/1.json
  def destroy
    @billable_period = BillablePeriod.find(params[:id])
    @billable_period.destroy

    respond_to do |format|
      format.html { redirect_to billable_periods_url }
      format.json { head :no_content }
    end
  end
end
