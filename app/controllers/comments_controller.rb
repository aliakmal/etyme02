class CommentsController < ApplicationController
  load_and_authorize_resource

  def index
    @tenants = current_user.tenants
    @comments = Comment.where("commentable_id = ? and commentable_type = ? and created_at > ?",
                              params[:resource_id],
                              params[:resource_type],
                              Time.at(params[:after].to_i + 1))

  end
  # POST /comments
  def create
    begin
      @resource = eval(params[:resource][:type]).find(params[:resource][:id])
      @comment = @resource.comments.new(params[:comment].merge(user_id: current_user.id))
    rescue
      @comment = Comment.new
      @comment.valid?
    end

    respond_to do |format|
      if @comment.save
        format.html do
          if request.xhr?
            render :partial =>'comments/comment', :locals => { :comment => @comment }, :layout => false
          else
             redirect_to :back, notice: 'Comment was successfully created.' 
          end
        end
      else
        redirect_to (@resource || :back), notice: "Comment #{@comment.errors.messages[:comment].first}" 
      end
    end
  end
  
  def edit
    @comment = Comment.find(params[:id])

    respond_to do |format|
      format.html do
        if request.xhr?
          render :partial=>'ajax_form', :locals => { :comment => @comment }, :layout => false
        else
          format.json { render json: @comment }
        end
      end
    end
  end
  
  def update
    @comment = Comment.find(params[:id])

    respond_to do |format|
      
      if @comment.update_attributes(params[:comment])
        format.html do
          if request.xhr?
            render :partial=>'comment', :locals => { :comment => @comment }, :layout => false
          else
            format.html { redirect_to :back, notice: 'Comment was successfully updated.' }
            format.json { head :no_content }
          end
        end
      else
        if request.xhr?
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        else
          format.html { render action: "edit" }
          format.json { render json: @comment.errors, status: :unprocessable_entity }
        end
      end
    end
  end
  
  def set_visibility
    @comment = Comment.find(params[:id])
    @comment.update_attributes({:visible_to=>params[:visible_to]})
    render :partial=>'comment', :locals => { :comment => @comment }, :layout => false
  end
  
  
  # DELETE /comments/1
  # DELETE /comments/1.json
  def destroy
    @comment = Comment.find(params[:id])
    @comment.destroy

    respond_to do |format|
      format.html { redirect_to :back, notice: 'Comment has been deleted' }
      format.json { head :no_content }
    end
  end
end
