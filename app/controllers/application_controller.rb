class ApplicationController < ActionController::Base
  protect_from_forgery
  #check_authorization
  rescue_from CanCan::AccessDenied do |exception|
    raise 1.inspect
    redirect_to root_path, :alert => exception.message
  end
  rescue_from ActionController::RoutingError do |exception|
    render text: "404 Not Found", status: 404
  end
  layout :layout_by_resource
  before_filter :authenticate_tenant!
  helper_method :current_subdomain
  helper_method :current_tenant
  helper_method :auto_invoicing

  def layout_by_resource
    if devise_controller? && resource_name == :user && action_name == 'new'
      "devise"
    elsif home_controller? && action_name == 'index'
      "home"
    else
      if current_user && (current_user.has_role? :candidate, current_tenant) && (current_user.has_role? :trainee, current_tenant)
        "candidate"
      elsif current_user && (current_user.has_role? :candidate, current_tenant)
        "candidate"
      else
        "application"
      end
    end
  end

  # def after_sign_in_path_for(resource)
  #   if resource.has_role? :admin, current_tenant
  #      time_diff = Time.now - resource.created_at
  #      trial_period_remaining = resource.trial_period_days.seconds
  #      if time_diff >= trial_period_remaining
  #       puts "Trial Period Ends"
  #     else
  #       puts "remaining days #{time_diff}"
  #     end
  #   end
  # end
  
  def auto_salary
      contracts = Contract.all
      total_mins = 0
      @paid_sheets = Array.new
      contracts.each do |contract|
        till_date_pay_salary = Time.now - (current_tenant.salary_gap_terms + current_tenant.salary_payment_day).days
        binding.pry
        timesheets = Timesheet.find(:all, :conditions => ["ends <=\'#{till_date_pay_salary.to_date}\' AND contract_id = #{contract.id} AND salary_payment_status = \'unpaid\' AND (state = \'approved\' OR state = \'invoiced\')"] )
        binding.pry
        # timesheets= Timesheet.where(:contract_id => contract.id,:state => 'approved')
        if !timesheets.empty?
          timesheets.each do |timesheet|
            total_mins += timesheet.minutes
            @paid_sheets << timesheet
          end
          if contract.employee_type == 'employee_w2'
            contract.employee_price.each do |price|
              if Time.now <= price.end_date
                  @total_salary = (total_mins.to_i / (60)) * price.cost_price
              else
                next
              end
            end
          else
            contract.vendor_price.each do |price|
              binding.pry
              if Time.now <= price.end_date
                  @total_salary = (total_mins.to_i / (60)) * price.cost_price
              else
                next
              end
            end
          end
          contract.customer_price.each do |price|
            if Time.now <= price.end_date
                @total_rate = (total_mins.to_i / (60)) * price.sale_price
            else
              next
            end
          end
          binding.pry
          if @total_salary != nil and @total_rate != nil
            rate = @total_salary / (total_mins/60)
            profit = @total_rate - @total_salary
            binding.pry
            Receipt.create(:total_salary =>@total_salary,:total_time => total_mins,:rate => rate,:profit => profit, :contract_id => contract.id)
            @paid_sheets.each do |psheet|
              psheet.salary_payment_status = 'paid'
              psheet.save
            end
          end
          binding.pry
          @total_salary = nil
          @total_rate = nil
          total_mins = 0
          @paid_sheets.clear
        else
          next
        end
      end
  end
  def current_subdomain
    request.subdomains.first
  end

  def current_tenant
    Tenant.find_by_id(current_user.roles.first.resource_id)
  end

  def authenticate_trial
    if current_user.roles.count == 0 and current_user.subscription_status != true
      current_user.add_role :exp_user, current_user.tenants.first
    elsif current_user.roles.count == 0 and current_user.subscription_status == true
      current_user.add_role :admin, current_user.tenants.first
    elsif current_user.has_role? :admin, current_tenant and current_user.subscription_status != true
      time_diff_in_sec = (Time.now.utc - current_user.created_at.to_time).to_i
      total_trial_period = current_user.trial_period_days.days.seconds
      @remaining_trial = (total_trial_period - time_diff_in_sec)/ 1.day
      if time_diff_in_sec >= total_trial_period
        puts  "Trail period Ends"
        current_user.add_role :exp_user, current_tenant
        current_user.remove_role 'admin', current_tenant
        #redirect_to payment_index_path
      else
        puts "Trail period #{@remaining_trial}"
      end
   end
 end

 def set_tenant
   tenant = Tenant.find_by_subdomain(current_subdomain)
   set_current_tenant(tenant.id)
 end

  def authenticate_tenant!
    unless authenticate_user!
      # Dont get any real benefit from these two lines
      #email = ( params.nil? || params[:user].nil? ? "" : " as: " + params[:user][:username] )
      #flash[:notice] = "cannot sign you in #{email}; check email/password and try again"
      return false
    end
    raise SecurityError, "*** invalid sign-in ***" unless user_signed_in?
    unless current_subdomain.nil?
      t = Tenant.find_by_subdomain(current_subdomain)
      unless t.nil?
        set_current_tenant(t.id)
      else
        #display tenants to choose from
        #set_current_tenant
        redirect_to user_accounts_path and return
      end
    else
      #display tenants to choose from
      #set_current_tenant
      redirect_to user_accounts_path and return
    end
    true
  end

  def klass_option_obj(klass, option_obj)
    return option_obj if option_obj.instance_of?(klass)
    option_obj ||= {}
    return klass.send(:new, option_obj)
  end

  def prep_signup_view(tenant=nil, user=nil, coupon=nil)
    @user   = klass_option_obj(User, user)
    @tenant = klass_option_obj(Tenant, tenant)
  end
end
