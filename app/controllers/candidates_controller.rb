class CandidatesController < ApplicationController
  # GET /candidates
  # GET /candidates.json
  authorize_resource :class => false
  has_scope :starts_with
  has_scope :by_name
  skip_before_filter :authenticate_tenant!, :only => [:join,:register,:login,:authenticate]
  skip_authorize_resource :only => [:join,:register,:login,:authenticate]
  skip_before_filter :authenticate_user, :only => [:join]

  def index
    @candidates = apply_scopes(Individual).only_candidates.reorder(sort_column + ' ' + sort_direction).page(params[:page])
    @all_candidates = apply_scopes(Individual).only_candidates.reorder(sort_column + ' ' + sort_direction).all
    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @candidates }
      format.csv { send_data Individual.to_csv(@all_candidates) }
    end

  end

  def list
    @candidates = apply_scopes(Individual).only_candidates.all
    if !params[:job_id].blank?
      @job = Job.find(params[:job_id])
      @swapper = 1
    end
    if !params[:training_id].blank?
      @training = Training.find(params[:training_id])
      @swapper = 2
    end
      respond_to do |format|
      if request.xhr?
        format.html { render :layout => false}
      else
        format.html # index.html.erb
        format.json { render json: @documents }
      end
    end
  end

  def login
    @user = User.new
    if params[:swap] == '1'
      @swapper = 1
    end
    if params[:swap] == '2'
      @swapper = 2
    end 
  end

  def authenticate

    @user = User.find_by_username(params[:username])
    respond_to do |format|
      if @user && @user.valid_password?(params[:password])
        sign_in :user, @user
        if !params[:job_id].blank? then
          format.html { redirect_to view_job_path(:id=>params[:job_id]), notice: 'Welcome.' }
        else
          # format.html { redirect_to portal_job_path, notice: 'Welcome.' }
        end

        if !params[:training_id].blank? then
          format.html { redirect_to view_training_path(:id=>params[:training_id]), notice: 'Welcome.' }
        else
          # format.html { redirect_to portal_training_path, notice: 'Welcome.' }
        end

        format.json { render json: @user, status: :created }
      else
        format.html { render action: "login", notice: 'Cannot log in invalid credentials.' }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end

  end

  def join
    @user = User.new
    @user.build_individual
    if params[:swap] == '1'
      @swapper = 1
      @job = params[:job_id]
    end
    if params[:swap] == '2'
      @swapper = 2
      @training = params[:training_id]
    end
  end

  def register
    user_params = params[:user]

    @user = User.new(user_params)

    respond_to do |format|
      if @user.save
        @user.add_role :candidate, Tenant.find_by_subdomain(current_subdomain)
        sign_in :user, @user
        if !params[:job_id].blank? then
          format.html { redirect_to view_job_path(:id=>params[:job_id]), notice: 'Candidate was successfully created.' }
        else
          # format.html { redirect_to portal_job_path, notice: 'Candidate was successfully created.' }
        end

        if !params[:training_id].blank? then
          format.html { redirect_to view_training_path(:id=>params[:training_id]), notice: 'Candidate was successfully created.' }
        else
          # format.html { redirect_to portal_training_path, notice: 'Candidate was successfully created.' }
        end
        format.html { redirect_to portal_job_path, notice: 'Candidate was successfully created.' }
        format.json { render json: @user, status: :created }
      else
        format.html { render action: "join" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  # GET /candidates/1
  # GET /candidates/1.json
  def show
    @candidate = User.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @candidate }
    end
  end

  # GET /candidates/new
  # GET /candidates/new.json
  def new
    @acompanies = Contact.where(:tenant_id => current_tenant.id, :type => 'Company')
    @candidate = User.new
    @candidate.build_individual
    if !params[:job_id].blank? then
      @candidate.individual.inquiries.build(:job_id=>params[:job_id])
#      @candidate.individual.inquiries.build(:job_id=>params[:job_id])
      @job = Job.find(params[:job_id])
      @swapper = 1
    end

    if !params[:training_id].blank? then
      @candidate.individual.inquiries.build(:training_id=>params[:training_id])
      @training = Training.find(params[:training_id])
      @swapper = 2
    end

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @candidate }
    end
  end

  def new_candidate_signup
    @candidate = User.new
    @candidate.build_individual
    #if !params[:job_id].blank? then
    #  @candidate.candidate.inquiries.build(:job_id=>params[:job_id])
    #  @job = Job.find(params[:job_id])
    #end

    if request.xhr?
      render :partial=>'new_candidate_signup', :locals => { :user => @candidate }, :layout => false
    end

  end

  # GET /candidates/1/edit
  def edit
    @candidate = User.find(params[:id])
    @acompanies = Contact.where(:tenant_id => current_tenant.id, :type => 'Company')
  end

  # POST /candidates
  # POST /candidates.json
  def create
    # @candidate = User.invite!(:email => params[:user][:email], :username => params[:user][:username])
    
    @candidate = User.new(params[:user])
    respond_to do |format|
      if @candidate.save
        @candidate.add_role :candidate, current_tenant
        @candidate.invite!()
        if !@candidate.individual.inquiries.blank? then
          if !params[:user][:individual_attributes][:inquiries_attributes]["0"][:training_id].blank? then
            if !@candidate.has_role? :trainer, current_tenant then
              @candidate.add_role :trainee, current_tenant
            end
          end
        end

        format.html { redirect_to @candidate, notice: 'Candidate was successfully created.' }
        format.json { render json: @candidate, status: :created, location: @candidate }
      else
        @acompanies = Contact.where(:tenant_id => current_tenant.id, :type => 'Company')
        format.html { render action: "new" }
        format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /candidates/1
  # PUT /candidates/1.json
  def update
    @candidate = User.find(params[:id])

    respond_to do |format|
      if @candidate.update_attributes(params[:user])
        format.html { redirect_to @candidate, notice: 'Candidate was successfully updated.' }
        format.json { head :no_content }
      else
        @acompanies = Contact.where(:tenant_id => current_tenant.id, :type => 'Company')
        format.html { render action: "edit" }
        format.json { render json: @candidate.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /candidates/1
  # DELETE /candidates/1.json
  def destroy
    @candidate = User.find(params[:id])
    @candidate.destroy

    respond_to do |format|
      format.html { redirect_to candidates_url }
      format.json { head :no_content }
    end
  end


  private
  helper_method :sort_column, :sort_direction
  def sort_column
    params[:sort] || "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction])? params[:direction]:'asc'
  end

end
