class CommissionsController < ApplicationController
  load_and_authorize_resource
  def index
  	@commissions = Commission.where(:tenant_id => current_tenant.id)
  end
  def commission_payment
  	@contract = Contract.find(params[:contract_id])
    # @price = Price.all
    @commission = Commission.find(params[:id])
  	# @timesheets= Timesheet.where(:contract_id => @contract.id,:state => 'approved')
  	@total_mins = 0
  	@contract.timesheets.each do |timesheet|
      if timesheet.state == 'approved' or timesheet.state == 'invoiced'
        @total_mins += timesheet.minutes
      else
        next
      end
    end
    # @price.each do |price|
    #   if @contract.id == price.priceable_id and (price.price_type == 'Employee' or price.price_type == 'Vendor')
    #     if  Time.now <= price.end_date
    #       @total_salary = (@total_mins.to_i / (60)) * price.cost_price
    #     else
    #       next
    #     end
    #   else
    #     next
    #   end
    # end
  	# @current_bill = @contract.billable_periods.first
  	# @total_salary = (@total_mins.to_i / (60)) * @current_bill.salary
  	if @commission.commission_type == 'Hourly'
      @commission.total_commission = ((@total_mins.to_i / (60))* @commission.hourly_commission)
      @commission.save
    elsif @commission.commission_type == 'Fixed'
      @commission.fixed_commission
      @commission.status = 'paid'
      @commission.save
    elsif @commission.commission_type == 'Monthly'
      no_of_months = Time.now.month - @contract.contract_start_date.month
      @commission.total_commission = @commission.monthly_commission * no_of_months
      @commission.save
    end

  	# binding.pry
  end
  def edit
    @commission = Commission.find(params[:id])
  end
  def update
    @commission = Commission.find(params[:id])

    respond_to do |format|
      if @commission.update_attributes(params[:commission])
        format.html { redirect_to commissions_path, notice: 'Commission was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @commission.errors, status: :unprocessable_entity }
      end
    end
  end
  # def show
  #   @commission = Commission.find(params[:id])
  #   respond_to do |format|
  #     format.html # show.html.erb
  #     format.json { render json: @commission }
  #   end
  # end
  def destroy
    @commission = Commission.find(params[:id])
    @commission.destroy
    respond_to do |format|
      format.html { redirect_to commissions_url }
      format.json { head :no_content }
    end
  end
end
