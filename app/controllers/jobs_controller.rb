class JobsController < ApplicationController
  # GET /jobs
  # GET /jobs.json
  load_and_authorize_resource

  has_scope :starts_with
  has_scope :by_title
  skip_before_filter :authenticate_tenant!, :only => [:portal,:view, :feed]
  skip_load_and_authorize_resource :only => [:portal,:view,:feed]
  skip_before_filter :authenticate_user, :only => [:portal,:view,:feed]
  def index
    @jobs = apply_scopes(Job).reorder(sort_column + ' ' + sort_direction).page(params[:page]).per(7)
    @tenant = current_tenant
    respond_to do |format|
      format.html # index.html.erb
      format.csv { send_data @jobs.to_csv }
      format.json { render json: @jobs }
    end
  end

  def my_jobs
    @jobs = apply_scopes(Job).by_candidate(current_user).reorder(sort_column + ' ' + sort_direction).page(params[:page])
    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render json: @jobs }
    end
  end

  def portal
    if(Tenant.find_by_subdomain(current_subdomain) != nil)
      set_tenant
      @jobs = Job.published.reorder(sort_column + ' ' + sort_direction).page(params[:page]).per(10)
    else
     raise ActionController::RoutingError.new('Requested Subdomain Not Found')
    end
    # @all_jobs = Job.find_by_sql "SELECT * from jobs where status = \'Active\'"
    # @jobs=  Kaminari.paginate_array(@all_jobs).page(params[:page]).per(10)
   #  @connection = ActiveRecord::Base.establish_connection(
   #    :adapter => "postgresql",
   #    :host => "localhost",
   #    :database => "etyme_development",
   #    :username => "postgres",
   #    :password => ""
   #    )

   #  sql = "SELECT * from jobs where status = \'Active\'"
   #  @result = @connection.connection.execute(sql);
   #  @jobs = Array.new
   #  @result.each(:as => :hash) do |row|
   #   puts row
   # end
   render :layout => 'portal'
 end

  # GET /jobs/1
  # GET /jobs/1.json
  def show
    @job = Job.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @job }
    end
  end

  def view
    if(Tenant.find_by_subdomain(current_subdomain) != nil)
      set_tenant
      @job = Job.find(params[:id])
      if user_signed_in?
          @job.inquiries.each do |inquiry|
            if inquiry.individual_id == current_user.individual.id
              @check_applied =1
            else
              next
            end
          end
          render :layout => 'candidate'
      else
        render :layout => 'portal'
      end
    else
      raise ActionController::RoutingError.new('Requested Subdomain Not Found')
    end
  end

  def apply
    @job = Job.find(params[:id])
    @user = current_user
    #@user.individual.phone.build
    #@user.individual.email.build
    @inquiry = Inquiry.new(:job_id=>@job.id, :individual_id => @current_user.individual.id)
    render :layout => 'portal'
  end

  def applied
    @user = User.find(current_user[:id])#candidate = User.new(params[:candidate])
    @job = Job.find(params[:id])
    @inquiry = Inquiry.new(params[:inquiry])

    respond_to do |format|
      if @inquiry.save
        format.html { redirect_to view_job_path(@job), notice: 'Your application has been recieved.' }
        format.json { render json: @inquiry, status: :created, location: @user }
      else
        format.html { render action: "apply", :layout => 'portal' }
        format.json { render json: @inquiry.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_candidates
    params[:ids].each do |one_id|
      Inquiry.create(:job_id=>params[:job_id], :individual_id=>one_id)
    end

    redirect_to(:back)
  end
  # GET /jobs/new
  # GET /jobs/new.json
  def new
    @job = Job.new
    client_comp = Array.new
    @client_comp = Company.where(:company_type => 'Client')
    if !@client_comp.empty?
      respond_to do |format|
        format.html # new.html.erb
        format.json { render json: @job }
      end
    else
      redirect_to new_company_path, :notice => 'First create a New Client Company for the job'
    end
  end

  # GET /jobs/1/edit
  def edit
    @job = Job.find(params[:id])
    @client_comp = Company.where(:company_type => 'Client')
  end

  # POST /jobs
  # POST /jobs.json
  def create
    @job = Job.new(params[:job])

    respond_to do |format|
      if @job.save
        format.html { redirect_to @job, notice: 'Job was successfully created.' }
        format.json { render json: @job, status: :created, location: @job }
      else
        @client_comp = Company.where(:company_type => 'Client')
        format.html { render action: "new" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /jobs/1
  # PUT /jobs/1.json
  def update
    @job = Job.find(params[:id])

    respond_to do |format|
      if @job.update_attributes(params[:job])
        format.html { redirect_to @job, notice: 'Job was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /jobs/1
  # DELETE /jobs/1.json
  def destroy
    @job = Job.find(params[:id])
    @job.destroy

    respond_to do |format|
      format.html { redirect_to jobs_url }
      format.json { head :no_content }
    end
  end
  def feed
    if(Tenant.find_by_subdomain(current_subdomain) != nil)
      set_tenant
      @jobs = Job.all(:order => "created_at DESC", :limit => 20) 
      respond_to do |format|
         format.rss { render :layout => false }
      end
    else
     raise ActionController::RoutingError.new('Requested Subdomain Not Found')
    end
  end
  def set_job_status_to_active
    @job = Job.find(params[:id])
    @job.status = 'Activated'
    @job.published = true
    respond_to do |format|
      if @job.update_attributes(params[:job])
        format.html { redirect_to jobs_url, notice: 'Job was successfully activated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end
  def set_job_status_to_inactive
    @job = Job.find(params[:id])
    @job.status = 'Deactivated'
    @job.published = false
    respond_to do |format|
      if @job.update_attributes(params[:job])
        format.html { redirect_to jobs_url, notice: 'Job was successfully deactivated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @job.errors, status: :unprocessable_entity }
      end
    end
  end


  private
  helper_method :sort_column, :sort_direction
  def sort_column
    params[:sort] || "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction])? params[:direction]:'asc'
  end
end
