class UsersController < ApplicationController
  #before_filter :authenticate_user!
  load_and_authorize_resource
  def index
    @tenant = current_tenant
    @users = Array.new
    #authorize! :index, @user, :message => 'Not authorized as an administrator.'
    all_users = User.all
    all_users.each do |user|
      if user.roles.count > 0
        if(user.roles.first.resource_id == current_tenant.id)
          @users << user
        end
      else
        next
      end
    end
  end

  def new
    @user = User.new
  end

  def create
    @user = User.new(params[:user])
    if @user.save
      @user.add_role :user, current_tenant
      flash[:notice] = "#{ @user.name } created."
      redirect_to users_path
    else
      render :action => 'new'
    end
  end

  def show
    @user = User.find(params[:id])
  end

  def edit
    @user = User.find(params[:id])
  end

  def change_my_password
    @user = User.find(current_user.id)
  end

  def update_my_password
    @user = User.find(current_user.id)
    #raise params.inspect
    #raise @user.inspect
    if @user.update_attributes(params[:user])
      sign_in @user, :bypass => true
      redirect_to change_my_password_user_path, :notice => "User Password updated."
    else
      #sign_in @user, :bypass => true
      render action: "change_my_password", :alert => "Unable to update user."
    end
  end

  def change_password
    @user = User.find(params[:id])
  end

  def update_password
    @user = User.find(params[:id])
    #raise @user.inspect
    if @user.update_attributes(params[:user])
      sign_in @user, :bypass => true
      redirect_to users_path, :notice => "User Password updated."
    else
      render action: "change_password", :alert => "Unable to update user."
    end
  end


  def update
    #authorize! :update, @user, :message => 'Not authorized as an administrator.'
    @user = User.find(params[:id])
    # @trimmed_params = params[:user][:role_ids]
    # @trimmed_params.delete("")
    # roles = Role.find(@trimmed_params)
    # #binding.pry
    # p = params[:user].delete(:role_ids)

    if @user.update_attributes(params[:user], :as => :admin)
      #binding.pry
      #  roles.each do |new_role|
      #  # binding.pry
      #   @user.roles.each do |role|
      #   #  binding.pry
      #     if new_role != role
      #      @user.add_role new_role.name, current_tenant
      #    #  binding.pry
      #      true
      #    else
      #    # binding.pry
      #     false
      #     end
      #   end
      # end
      redirect_to users_path, :notice => "User updated."
    else
      redirect_to users_path, :alert => "Unable to update user."
    end
  end

  def destroy
    authorize! :destroy, @user, :message => 'Not authorized as an administrator.'
    user = User.find(params[:id])
    unless user == current_user
      user.destroy
      redirect_to users_path, :notice => "User deleted."
    else
      redirect_to users_path, :notice => "Can't delete yourself."
    end
  end
end
