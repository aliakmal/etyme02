class TimesheetsController < ApplicationController
  # GET /timesheets
  # GET /timesheets.json
  load_and_authorize_resource
  def index
    # auto_salary
    @timesheets = Timesheet.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @timesheets }
    end
  end

  def contract
    @timesheets = Timesheet.by_contract_id(params[:contract_id])
    @contract = Contract.find(params[:contract_id])
  end
  def my_timesheets
    @contracts = Contract.by_candidate(current_user).page(params[:page])
    @my_timesheets = Array.new
    @contracts.each do |contract|
      contract.timesheets.each do |timesheet|
        @my_timesheets << timesheet
      end
    end
    respond_to do |format|
      format.html
      format.json { render json: @my_timesheets }
    end
  end

  # GET /timesheets/1
  # GET /timesheets/1.json
  def show
    @timesheet = Timesheet.find(params[:id])
    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @timesheet }
    end
  end

  # GET /timesheets/new
  # GET /timesheets/new.json
  def new
    @contract = Contract.find(params[:contract_id])
    # get the current timesheet please
    if @contract.employee_type == 'employee_w2'
      if Time.now <= @contract.customer_price.last.end_date or Time.now <= @contract.employee_price.last.end_date 
        @timesheet = Timesheet.get_current_timesheet(params[:contract_id])
        redirect_to edit_timesheet_path(@timesheet)
      else
        redirect_to contract_path(@contract), :notice => 'Your contract has been expired'
      end
    else
      if Time.now <= @contract.customer_price.last.end_date or Time.now <= @contract.vendor_price.last.end_date 
        @timesheet = Timesheet.get_current_timesheet(params[:contract_id])
        redirect_to edit_timesheet_path(@timesheet)
      else
        redirect_to contract_path(@contract), :notice => 'Your contract has been expired'
      end
    end
  end

  # GET /timesheets/1/edit
  def edit
    @timesheet = Timesheet.find(params[:id])
  end

  # POST /timesheets
  # POST /timesheets.json
  def create
    @timesheet = Timesheet.new(params[:timesheet])

    respond_to do |format|
      if @timesheet.save
        @timesheet.set_total_minutes
        format.html { redirect_to @timesheet, notice: 'Timesheet was successfully created.' }
        format.json { render json: @timesheet, status: :created, location: @timesheet }
      else
        format.html { render action: "new" }
        format.json { render json: @timesheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # PUT /timesheets/1
  # PUT /timesheets/1.json
  def update
    @timesheet = Timesheet.find(params[:id])

    if @timesheet.type == 'MonthlyTimesheet' then 
      prms = params[:monthly_timesheet]
    elsif @timesheet.type == 'WeeklyTimesheet' then
      prms = params[:weekly_timesheet]

    end

    respond_to do |format|
      if @timesheet.update_attributes(prms)
        @timesheet.set_total_minutes

        format.html { redirect_to @timesheet, notice: 'Timesheet was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @timesheet.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /timesheets/1
  # DELETE /timesheets/1.json
  def destroy
    @timesheet = Timesheet.find(params[:id])
    @timesheet.destroy

    respond_to do |format|
      format.html { redirect_to timesheets_url }
      format.json { head :no_content }
    end
  end


  def unapproved_to_pending
    timesheet = Timesheet.find(params[:id])
    timesheet.pending

    redirect_to timesheet, notice: 'Timesheet was successfully updated.'

  end

  def pending_to_approved
    timesheet = Timesheet.find(params[:id])
    timesheet.approve

    redirect_to timesheet, notice: 'Timesheet was successfully updated.'
  end

  def approved_to_invoiced
    timesheet = Timesheet.find(params[:id])
    timesheet.approve

    redirect_to timesheet, notice: 'Timesheet was successfully updated.'
  end

  def approved_to_unapproved
    timesheet = Timesheet.find(params[:id])
    timesheet.unapprove

    redirect_to timesheet, notice: 'Timesheet was successfully updated.'
  end

  def pending_to_rejected
    timesheet = Timesheet.find(params[:id])
    timesheet.reject

    redirect_to timesheet, notice: 'Timesheet was successfully updated.'
  end

  def rejected_to_unapproved
    timesheet = Timesheet.find(params[:id])
    timesheet.unreject

    redirect_to timesheet, notice: 'Timesheet was successfully updated.'
  end

end
