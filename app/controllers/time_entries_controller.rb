class TimeEntriesController < ApplicationController
  # GET /time_entries
  # GET /time_entries.json
  load_and_authorize_resource
  def index
    @time_entries = TimeEntry.all

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @time_entries }
    end
  end

  # GET /time_entries/1
  # GET /time_entries/1.json
  def show
    @time_entry = TimeEntry.find(params[:id])

    respond_to do |format|
      format.html # show.html.erb
      format.json { render json: @time_entry }
    end
  end

  # GET /time_entries/new
  # GET /time_entries/new.json
  def new
    @time_entry = TimeEntry.new

    respond_to do |format|
      format.html # new.html.erb
      format.json { render json: @time_entry }
    end
  end

  # GET /time_entries/1/edit
  def edit
    @time_entry = TimeEntry.find(params[:id])
  end

  def get_monthly_view
    @contract = Contract.find(params[:cid])
    @month = params[:month]
    @year = params[:year]

    render :partial=>'monthly_entry_view', :locals => { :contract => @contract, :month => @month, :year=> @year }, :layout => false
  end

  def get_monthly_entry_form
    @contract = Contract.find(params[:cid])
    @month = params[:month]
    @year = params[:year]

    render :partial=>'monthly_entry_form', :locals => { :contract => @contract, :month => @month, :year=> @year }, :layout => false
  end

  # POST /time_entries
  # POST /time_entries.json
  def create
    @time_entry = TimeEntry.new(params[:time_entry])
    @time_entry[:candidate_id] = current_user.id

    respond_to do |format|
      if @time_entry.save
        format.html { redirect_to @time_entry.contract, notice: 'Time entry was successfully created.' }
        format.json { render json: @time_entry, status: :created, location: @time_entry }
      else
        format.html { redirect_to @time_entry.contract, error: 'Time entry was not created.' }
        #format.html { render action: "new" }
        format.json { render json: @time_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_multiple
    tm = params[:time_entries]
    tm.each do |index, dt|
      dt['contract_id'] = params['contract_id']
      if dt['id'].blank? then
        time_entry = TimeEntry.new(dt)
        time_entry[:contract_id] = params[:contract_id]
      else
        time_entry = TimeEntry.find(dt[:id])
        time_entry.update_attributes(dt)
      end
      time_entry.save
    end
    redirect_to Contract.find(params[:contract_id]), notice: 'Time entries was successfully updated.'
  end

  # PUT /time_entries/1
  # PUT /time_entries/1.json
  def update
    @time_entry = TimeEntry.find(params[:id])

    respond_to do |format|
      if @time_entry.update_attributes(params[:time_entry])
        format.html { redirect_to @time_entry, notice: 'Time entry was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @time_entry.errors, status: :unprocessable_entity }
      end
    end
  end

  # DELETE /time_entries/1
  # DELETE /time_entries/1.json
  def destroy
    @time_entry = TimeEntry.find(params[:id])
    @time_entry.destroy

    respond_to do |format|
      format.html { redirect_to time_entries_url }
      format.json { head :no_content }
    end
  end


    # GET /services/1/new_to_pending
  def unapproved_to_approved
    time_entry = TimeEntry.find(params[:id])
    time_entry.approve

    redirect_to time_entry.contract, notice: 'Time entry was successfully updated.'

  end

  def approved_to_unapproved
    time_entry = TimeEntry.find(params[:id])
    time_entry.unapprove

    redirect_to time_entry.contract, notice: 'Time entry was successfully updated.'
  end

  def unapproved_to_rejected
    time_entry = TimeEntry.find(params[:id])
    time_entry.reject

    redirect_to time_entry.contract, notice: 'Time entry was successfully updated.'
  end

  def rejected_to_unapproved
    time_entry = TimeEntry.find(params[:id])
    time_entry.unreject

    redirect_to time_entry.contract, notice: 'Time entry was successfully updated.'
  end




end
