class ReceiptsController < ApplicationController
  load_and_authorize_resource
  # skip_authorize_resource :only => [:my_contracts,:show]
  # GET /contracts
  # GET /contracts.json
  def index
    @receipts = Receipt.page(params[:page])

    respond_to do |format|
      format.html # index.html.erb
      format.json { render json: @receipts }
    end
  end
  def edit
    @receipt = Receipt.find(params[:id])
  end
  def update
    @receipt = Receipt.find(params[:id])
    @receipt.salary_pending = @receipt.total_salary - (params[:receipt][:payment_amount]).to_f
    @receipt.status = 'paid'
    binding.pry
    respond_to do |format|
      if @receipt.update_attributes(params[:receipt])
        format.html { redirect_to receipts_path, notice: 'Transaction was successfully Completed.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @receipt.errors, status: :unprocessable_entity }
      end
    end
  end
end
