class TenantsController < ApplicationController
  load_and_authorize_resource
  def index
  	@current_term = current_tenant
    @days = Array.new
    for i in 1..28
      @days << i.ordinalize
    end
  end
  def show
  	 @tenant = Tenant.find(params[:id])
    if @tenant.update_attributes(params[:tenant])
      redirect_to tenants_path, :notice => "Settings updated."
    else
      redirect_to users_path, :alert => "Unable to update settings."
    end
  end
end
