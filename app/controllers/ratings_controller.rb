class RatingsController < ApplicationController
	
	def create
	  @rating = Rating.new(params[:rating])
	  @training = Training.find(params[:rating][:training_id])

	  respond_to do |format|
	      if @rating.save
	        format.json { render :json => { :avg_rating => @training.avg_rating } }
	      else
	        format.json { render :json => @rating.errors, :status => :unprocessable_entity }
	      end
	  end
	end

	def update
	  @rating = Rating.find(params[:id])
	  @training = Training.find(params[:rating][:training_id])
	  @rating.update_attributes(params[:rating])
	                
	  respond_to do |format|
	    if @rating.save
	      format.json { render :json => { :avg_rating => @training.avg_rating } }
	    else
	      format.json { render :json => @rating.errors, :status => :unprocessable_entity }
	    end
	  end
	end

end
