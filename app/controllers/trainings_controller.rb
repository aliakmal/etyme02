class TrainingsController < ApplicationController
  
  load_and_authorize_resource
  has_scope :starts_with
  has_scope :by_name


  skip_before_filter :authenticate_tenant!, :only => [:portal,:view,:feed]
  skip_load_and_authorize_resource :only => [:portal,:view,:apply,:feed]
  skip_before_filter :authenticate_user, :only => [:portal,:view,:feed]
  def index
    @trainings = apply_scopes(Training).reorder(sort_column + ' '+ sort_direction).page(params[:page])
    respond_to do |format|
      format.html
      format.json { render json: @trainings }
    end
  end

  def my_trainings
    @trainings = apply_scopes(Training).by_candidate(current_user).reorder(sort_column + ' ' + sort_direction).page(params[:page])
    respond_to do |format|
      format.html { render action: 'index' }
      format.json { render json: @trainings }
    end
  end

  def portal
    if(Tenant.find_by_subdomain(current_subdomain) != nil)
      set_tenant
      @trainings = Training.published.reorder(sort_column + ' ' + sort_direction).page(params[:page])
    else
     raise ActionController::RoutingError.new('Requested Subdomain Not Found')
    end
    render :layout => 'portal'
  end

  def view
    if(Tenant.find_by_subdomain(current_subdomain) != nil)
      set_tenant
      @training = Training.find(params[:id])
      if user_signed_in?
          @training.inquiries.each do |inquiry|
            if inquiry.individual_id == current_user.individual.id
              @check_applied =1
            else
              next
            end
          end
          render :layout => 'candidate'
      else
        render :layout => 'portal'
      end
    else
      raise ActionController::RoutingError.new('Requested Subdomain Not Found')
    end
  end


  def apply
    @training = Training.find(params[:id])
    @user = current_user
    @inquiry = Inquiry.new(:training_id=>@training.id, :individual_id => @current_user.individual.id)
    render :layout => 'portal'
  end

  def applied
    @user = User.find(current_user[:id])#candidate = User.new(params[:candidate])
    @training = Training.find(params[:id])
    # @inquiry = Inquiry.new(params[:inquiry])
    @user.add_role :trainee, Tenant.find_by_subdomain(current_subdomain)
    @inquiry = Inquiry.create(:training_id=>@training.id, :individual_id=>current_user.individual.id)

    respond_to do |format|
      if @inquiry.save
        format.html { redirect_to view_training_path(:id => @training.id, :swap => 1), notice: 'Your application has been recieved.' }
        format.json { render json: @inquiry, status: :created, location: @user }
      else
        format.html { render action: "apply", :layout => 'portal' }
        format.json { render json: @inquiry.errors, status: :unprocessable_entity }
      end
    end
  end

  def add_candidates
    params[:ids].each do |one_id|
      inquiry = Inquiry.create(:training_id=>params[:training_id], :individual_id=>one_id)
      if !inquiry.individual.user.has_role? :trainer, current_tenant then
        inquiry.individual.user.add_role :trainee, current_tenant
      end
    end

    redirect_to(:back)
  end

  def accepted_candidates
    @training = Training.find(params[:id])
    respond_to do |format|
      format.html 
      format.json { render json: @training }
    end
  end
  
  def show
    @training = Training.find(params[:id])
    t = @training.ratings
    ratrs = []
    t.each do |r|
      ratrs << User.find(r.user_id)
    end
    @rators = ratrs.map(&:username).join("<br />")
    respond_to do |format|
      format.html 
      format.json { render json: @training }
    end
  end

  def new
    @training = Training.new
    @training.inquiries.build
    trainer_list

    respond_to do |format|
      format.html 
      format.json { render json: @training }
    end
  end
  
  def create
    @training = Training.new(params[:training])
    individual_check = params[:training][:inquiries_attributes]["0"][:individual_id]
    respond_to do |format|
      if !individual_check.empty?
        if @training.save
          format.html { redirect_to @training, notice: 'training was successfully created.' }
          format.json { render json: @training, status: :created, location: @training }
        else
          trainer_list
          format.html { render action: "new" }
          format.json { render json: @training.errors, status: :unprocessable_entity }
        end
      else
        @training.errors.add :base, 'No trainer selected'
        trainer_list
          format.html { render action: "new" }
          format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end

  def edit
    @training = Training.find(params[:id])
    trainer_list
  end

  def update
    @training = Training.find(params[:id])
    individual_check = params[:training][:inquiries_attributes]["0"][:individual_id]
    respond_to do |format|
      if !individual_check.empty?
          if @training.update_attributes(params[:training])
            format.html { redirect_to @training, notice: 'training was successfully updated.' }
            format.json { head :no_content }
          else
            format.html { render action: "edit" }
            format.json { render json: @training.errors, status: :unprocessable_entity }
          end
      else
        @training.errors.add :base, 'No trainer selected'
        trainer_list
        format.html { render action: "edit" }
        format.json { render json: @traininng.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @training = Training.find(params[:id])
    @training.destroy

    respond_to do |format|
      format.html { redirect_to trainings_url }
      format.json { head :no_content }
    end
  end
  def set_training_status_to_active
    @training = Training.find(params[:id])
    @training.status = 'Activated'
    @training.published = true
    respond_to do |format|
      if @training.update_attributes(params[:training])
        format.html { redirect_to trainings_url, notice: 'Training was successfully activated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end
  def set_training_status_to_inactive
    @training = Training.find(params[:id])
    @training.status = 'Deactivated'
    @training.published = false
    respond_to do |format|
      if @training.update_attributes(params[:training])
        format.html { redirect_to trainings_url, notice: 'Training was successfully deactivated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @training.errors, status: :unprocessable_entity }
      end
    end
  end
  def feed
    if(Tenant.find_by_subdomain(current_subdomain) != nil)
      set_tenant
      @trainings = Training.all(:order => "created_at DESC", :limit => 20) 
      respond_to do |format|
         format.rss { render :layout => false }
      end
    else
     raise ActionController::RoutingError.new('Requested Subdomain Not Found')
    end
  end
  
  private
  helper_method :sort_column, :sort_direction, :trainer_list
  def sort_column
    params[:sort] || "id"
  end

  def sort_direction
    %w[asc desc].include?(params[:direction])? params[:direction]:'asc'
  end
  def trainer_list
    trainers = []
      @individuals = Individual.all
      @individuals.each do |individual|
        if individual.user_id != nil
          if individual.user.has_role? :trainer, current_tenant
            trainers << individual
          end
        else
          next
        end
      end
      @individual = trainers
  end

end
