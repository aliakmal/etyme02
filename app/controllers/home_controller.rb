class HomeController < ApplicationController
  authorize_resource :class => false
  skip_authorize_resource :only => [:index,:main, :checkname,:send_email]
  skip_before_filter :authenticate_tenant!, :only => [:main, :checkname, :accounts,:index,:send_email]
  skip_before_filter :authenticate_user!, :only => [:main, :checkname,:send_email]
  before_filter :authenticate_user!, :only =>[:index,:accounts]
  before_filter :authenticate_trial, :only =>[:index]
  
  def index
    @subd = Tenant.find_by_id(current_user.roles.first.resource_id)
    @jobs = Job.unscoped.where(:tenant_id => current_tenant.id)
    @train = Array.new
    trainings = Training.unscoped.where('start_date > ?',(Date.today))
    trainings.each do |training|
      if training.tenant_id == current_tenant.id
        @train << training
      else
        next
      end
    end
    if user_signed_in?
     @tenants = current_user.tenants
     if current_user.has_role? :admin, current_tenant and current_subdomain.nil?
      # time_diff_in_sec = (Time.now.utc - current_user.created_at.to_time).to_i
      # total_trial_period = current_user.trial_period_days.days.seconds
      # @remaining_trial = (total_trial_period - time_diff_in_sec)/ 1.day
      #  if time_diff_in_sec >= total_trial_period
      #   puts "Trial Period Ends"
      # else
      #   puts "remaining days #{@remaining_trial}"
      # end
       redirect_to root_url(subdomain: current_tenant.subdomain )
     elsif current_user.has_role? :candidate, current_tenant and @tenants.count > 1
       redirect_to user_accounts_path
     elsif current_user.has_role? :candidate, current_tenant and @tenants.count <= 1 and current_subdomain.nil?
      redirect_to root_url(subdomain: current_tenant.subdomain )
     elsif current_user.has_role? :exp_user, current_tenant and current_user.trial_period_active == true
      puts "redirecting to payment_index_path"
      redirect_to payment_index_path
     end
    end
  end

  def main
    # render text: 'There should be a home page here'
    @message = Message.new
    render :layout => 'main'
  end
  def send_email
    name = params[:name]
    email = params[:email]
    subject = params[:subject]
    message = params[:message]
    # ClientMailer.drop_email(subject,message,name,email).deliver
    render action: 'main',:layout => 'sign_in_up', :notice => 'Thankyou for your Feedback'
  end

  def accounts
   @tenants = current_user.tenants
  end

  def checkname
    if Tenant.where('subdomain = ?', params[:subdomain]).count == 0
      render nothing: true, status: 200
    else
      render nothing: true, status: 409
    end
    return
  end

  def search
    @cntacts = Contact.search(params[:term].to_s + '*')

    respond_to do |format|
      format.js do
        render :json =>  @cntacts
      end
    end
  end

end
