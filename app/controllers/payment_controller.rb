class PaymentController < ApplicationController
  before_filter :authenticate_user!
  authorize_resource :class => false
  skip_before_filter :authenticate_tenant!, :only => [:index,:processing]
  #skip_before_filter :authenticate_trial, :only =>[:index,:processing]
  def index
  	if user_signed_in?
     @resource = current_user
    end
  end
  def processing
    if current_user.has_role? :exp_user, current_tenant and current_user.subscription_status != true
      if current_user.update_attributes(params[:user])
       if current_user.stripe_payment
        current_user.add_role :admin, current_tenant
        current_user.remove_role 'exp_user', current_tenant
        
        flash[:notice] = " Your Payment is successful."
        redirect_to root_url(subdomain: current_tenant.subdomain)
      else
        flash[:notice] = "Sorry. Your Payment is not successful. Please try again"
        redirect_to payment_index_path
      end
     else
       flash[:notice] = "Sorry. Unable to update user information. Please try again"
       redirect_to payment_index_path
     end
   elsif current_user.has_role? :admin , current_tenant and current_user.subscription_status != true
    if current_user.update_attributes(params[:user])
       if current_user.stripe_payment
        flash[:notice] = " Your Payment is successful."
        redirect_to root_url
      else
        flash[:notice] = "Sorry. Your Payment is not successful. Please try again"
        redirect_to payment_index_path
      end
     else
       flash[:notice] = "Sorry. Unable to update user information. Please try again"
       redirect_to payment_index_path
     end
   end
  end
end
