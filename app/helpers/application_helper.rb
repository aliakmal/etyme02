module ApplicationHelper

  def activity_icon(activity)
    if ['service.status_change_pending_to_new','service.status_change_stalled_to_new','request.status_change_stalled_to_current','request.status_change_awaiting_movement_to_current','request.status_change_done_to_current','request.status_change_cancelled_to_current'].include? activity.key then
        '<span class="label label-info"><i class="icon icon-blank"/></span>'.html_safe
    elsif ['service.status_change_new_to_pending','service.status_change_stalled_to_pending','service.status_change_cancelled_to_pending','request.status_change_current_to_awaiting_movement','request.status_change_stalled_to_awaiting_movement'].include? activity.key then
        '<span class="label label-warning"><i class="icon icon-blank"/></span>'.html_safe
    elsif ['request.status_change_current_to_cancelled','request.status_change_awaiting_movement_to_cancelled','service.status_change_new_to_cancelled','service.status_change_pending_to_cancelled'].include? activity.key then
        '<span class="label label-important"><i class="icon icon-blank"/></span>'.html_safe
    elsif ['request.status_change_current_to_stalled','request.status_change_awaiting_movement_to_stalled','service.status_change_new_to_stalled','service.status_change_pending_to_stalled'].include? activity.key then
        '<span class="label label-inverse"><i class="icon icon-blank"/></span>'.html_safe
    elsif ['request.status_change_current_to_stalled','request.status_change_awaiting_movement_to_stalled','service.status_change_new_to_stalled','service.status_change_pending_to_stalled'].include? activity.key then
        '<span class="label label-inverse"><i class="icon icon-blank"/></span>'.html_safe
    elsif ['request.status_change_awaiting_movement_to_done','request.status_change_archive_to_done','service.status_change_pending_to_complete'].include? activity.key then
        '<span class="label label-success"><i class="icon icon-blank"/></span>'.html_safe
    elsif ['request.status_change_done_to_archive'].include? activity.key then
        '<span class="label"><i class="icon icon-blank"/></span>'.html_safe
    elsif ['request.assigned_operator','request.assigned_customer'].include? activity.key then
        '<span class="label"><i class="icon icon-briefcase" /></span>'.html_safe
    elsif ['service.create','request.create'].include? activity.key then
        '<span class="label"><i class="icon icon-star" /></span>'.html_safe
    elsif ['aircraft.create'].include? activity.key then
        '<span class="label"><i class="icon icon-plane" /></span>'.html_safe
    elsif ['service.edit_note'].include? activity.key then
        '<span class="label"><i class="icon icon-comment" /></span>'.html_safe
    elsif ['vendor_request.create'].include? activity.key then
        '<span class="label label-inverse"><i class="icon icon-white icon-star" /></span>'.html_safe
    elsif ['vendor_request.update'].include? activity.key then
        '<span class="label label-inverse"><i class="icon icon-white icon-pencil" /></span>'.html_safe
    elsif ['vendor_request.sent_to_vendor'].include? activity.key then
        '<span class="label label-info"><i class="icon icon-white icon-share-alt" /></span>'.html_safe
    end
  end

  def display_base_errors resource
    return '' if (resource.errors.empty?) or (resource.errors[:base].empty?)
    messages = resource.errors[:base].map { |msg| content_tag(:p, msg) }.join
    html = <<-HTML
    <div class="alert alert-error alert-block">
      <button type="button" class="close" data-dismiss="alert">&#215;</button>
      #{messages}
    </div>
    HTML
    html.html_safe
  end
  
  def sortable(column, title = nil)  
    title ||= column.titleize  
    css_class = column == params[:sort] ? "current sorting_#{sort_direction}" : nil  
    direction = column == params[:sort] && sort_direction == "asc" ? "desc" : "asc"  
    if direction == 'asc' then
      html = <<-HTML
      #{title}
   
      <i class="icon-circle-arrow-up"></i>
      HTML
    else
      html = <<-HTML
      #{title}
      <i class="icon-circle-arrow-down"></i>
      HTML
    end
    
    prms = {}
    
    params.each do |index, value|
      prms[index] = value
    end
    
    prms['sort'] = column
    prms['direction'] = direction
    
    link_to raw(html), prms, {:class => css_class}
  end  
  
  
  def link_to_remove_fields(name, f)
        f.hidden_field(:_destroy) + link_to(name,'#', onclick:'remove_fields(this)')
  end
  
  def link_to_add_contact_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    #new_object[:type] = association
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render('contact_details/'+association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")", :style=>'margin-top:6px; display:inline-block;')
    
  end
  
  def link_to_add_address_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    new_object.build_address_detail

    #new_object[:type] = association
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render("contact_details/address_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")", :style=>'margin-top:6px; display:inline-block;')
  end
  
  def link_to_add_service_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    #new_object[:type] = association
    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render("service_types/service_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")", :style=>'margin-top:6px; display:inline-block;')
  end
  def link_to_add_commission_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    #new_object[:type] = association

    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render('contracts/'+association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  def link_to_add_customer_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    #new_object[:type] = association

    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render('contracts/'+association.to_s.singularize + "_fields", :f => builder)
    end
    link_to_function(name, "add_fields_for_customer(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  def link_to_add_vendor_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    #new_object[:type] = association

    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render('contracts/vendor_fields', :f => builder)
    end
    link_to_function(name, "add_fields_for_vendor(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  def link_to_add_employee_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    #new_object[:type] = association

    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render('contracts/employee_fields', :f => builder)
    end
    link_to_function(name, "add_fields_for_employee(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  def link_to_add_attachment_detail_fields(name, f, association)
    new_object = f.object.class.reflect_on_association(association).klass.new
    #new_object[:type] = association

    fields = f.fields_for(association, new_object, :child_index => "new_#{association}") do |builder|
      render('contracts/attachment_fields', :f => builder)
    end
    link_to_function(name, "add_fields(this, \"#{association}\", \"#{escape_javascript(fields)}\")")
  end
  
  def application_environment
    if request.host == 'lvh.me' || request.host == 'localhost' then
      'localhost'
    elsif request.host == 'test.fltctr.com' then
      'test server'
    else
      false
    end
  end

  # For generating time tags calculated using jquery.timeago
  def timeago(time, options = {})
    options[:class] ||= "timeago"
    content_tag(:abbr, time.to_s, options.merge(:title => time.getutc.iso8601)) if time
  end

  # Shortcut for outputing proper ownership of objects,
  # depending on who is looking
  def whose?(user, object)
    case object
      when Post
        owner = object.author
      when Comment
        owner = object.user
      else
        owner = nil
    end
    if user and owner
      if user.id == owner.id
        "his"
      else
        "#{owner.name}'s"
      end
    else
      ""
    end
  end

  # Check if object still exists in the database and display a link to it,
  # otherwise display a proper message about it.
  # This is used in activities that can refer to
  # objects which no longer exist, like removed posts.
  def link_to_trackable(object, object_type)
    if object
      link_to object.to_string, object
    else
      "a #{object_type.downcase} which does not exist anymore"
    end
  end
  def trial res
      time_diff_in_sec = (Time.now.utc - res.created_at.to_time).to_i
      total_trial_period = res.trial_period_days.days.seconds
      @remaining_trial = (total_trial_period - time_diff_in_sec)/ 1.day
      @remaining_trial
  end
end
