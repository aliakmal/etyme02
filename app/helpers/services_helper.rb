module ServicesHelper
  def show_links_based_on_status(status, link)
    case link
      when 'cancel'
        if status =='cancelled' then
          return false
        end
        if status == 'done' then
         return  false
        end
      when 'complete'
        if status=='new' then
         return  false
        end
        if status=='done' then
         return  false
        end
      when 'pending'
        if status=='cancelled' then
          return false
        end
    else
      return true
    end
    
  end
end
