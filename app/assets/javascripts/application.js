// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// the compiled file.
//
// WARNING: THE FIRST BLANK LINE MARKS THE END OF WHAT'S TO BE PROCESSED, ANY BLANK LINE SHOULD
// GO AFTER THE REQUIRES BELOW.
//
//= require jquery
//= require jquery_ujs
//= require bootsy
//= require jquery-ui
//= require jquery.purr
//= require best_in_place
//= require best_in_place.purr
//= require app_wide/timepicker/jquery-ui-timepicker-addon
//= require bootstrap
//= require app_wide/bootbox/bootbox
//= require app_wide/bootmultiselect/bootstrap-multiselect
//= require bootstrap-wysiwyg
//= require app_wide/rating
//= require app_wide/bootstrap-tagmanager
//= require /app_wide/*

function remove_fields(link) {
  $(link).prev("input[type=hidden]").val("1");
  $(link).closest(".controls").hide();
}

function add_fields(link, association, content) {
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  console.log(content.replace(regexp, new_id));
  console.log(regexp);
  $(content.replace(regexp, new_id)).insertBefore($(link).parents('.controls').first());
}
function add_fields_for_customer(link, association, content) {
  var end_year_value = $('.customer .controls .contract_customer_price_end_date:last').find('select:first').val();
  var end_month_value = $('.customer .controls .contract_customer_price_end_date:last').find('select:first').next().val();
  var end_day_value = $('.customer .controls .contract_customer_price_end_date:last').find('select:last').val();
  if (end_day_value != undefined){
     // alert(end_year_value)
     // alert(end_month_value);
     // alert(end_day_value);
   }
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  console.log(content.replace(regexp, new_id));
  console.log(regexp);
  $(content.replace(regexp, new_id)).insertBefore($(link).parents('.controls').first());
  var start_year_value = $('.customer .controls .contract_customer_price_start_date:last').find('select:first').val();
  var start_month_value = $('.customer .controls .contract_customer_price_start_date:last').find('select:first').next().val();
  var start_day_value = $('.customer .controls .contract_customer_price_start_date:last').find('select:last').val();
  if (start_day_value != undefined && end_day_value != undefined){
    // alert(start_month_value);
    // alert(start_day_value);
    $('.customer .controls .contract_customer_price_start_date:last').find('select:first').val(end_year_value);
    $('.customer .controls .contract_customer_price_start_date:last').find('select:first').next().val(end_month_value);
    $('.customer .controls .contract_customer_price_start_date:last').find('select:last').val(parseInt(end_day_value)+1);
    $('.customer .controls .contract_customer_price_end_date:last').find('select:first').val(end_year_value);
    $('.customer .controls .contract_customer_price_end_date:last').find('select:first').next().val(end_month_value);
    $('.customer .controls .contract_customer_price_end_date:last').find('select:last').val(parseInt(end_day_value)+2);
  }
}
function add_fields_for_vendor(link, association, content) {
  var end_year_value = $('.vendor .controls .contract_vendor_price_end_date:last').find('select:first').val();
  var end_month_value = $('.vendor .controls .contract_vendor_price_end_date:last').find('select:first').next().val();
  var end_day_value = $('.vendor .controls .contract_vendor_price_end_date:last').find('select:last').val();
  if (end_day_value != undefined){
     // alert(end_year_value)
     // alert(end_month_value);
     // alert(end_day_value);
   }
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  console.log(content.replace(regexp, new_id));
  console.log(regexp);
  $(content.replace(regexp, new_id)).insertBefore($(link).parents('.controls').first());
  var start_year_value = $('.vendor .controls .contract_vendor_price_start_date:last').find('select:first').val();
  var start_month_value = $('.vendor .controls .contract_vendor_price_start_date:last').find('select:first').next().val();
  var start_day_value = $('.vendor .controls .contract_vendor_price_start_date:last').find('select:last').val();
  if (start_day_value != undefined && end_day_value != undefined){
    // alert(start_month_value);
    // alert(start_day_value);
    $('.vendor .controls .contract_vendor_price_start_date:last').find('select:first').val(end_year_value);
    $('.vendor .controls .contract_vendor_price_start_date:last').find('select:first').next().val(end_month_value);
    $('.vendor .controls .contract_vendor_price_start_date:last').find('select:last').val(parseInt(end_day_value)+1);
    $('.vendor .controls .contract_vendor_price_end_date:last').find('select:first').val(end_year_value);
    $('.vendor .controls .contract_vendor_price_end_date:last').find('select:first').next().val(end_month_value);
    $('.vendor .controls .contract_vendor_price_end_date:last').find('select:last').val(parseInt(end_day_value)+2);
  }
}
function add_fields_for_employee(link, association, content) {
  var end_year_value = $('.employee .controls .contract_employee_price_end_date:last').find('select:first').val();
  var end_month_value = $('.employee .controls .contract_employee_price_end_date:last').find('select:first').next().val();
  var end_day_value = $('.employee .controls .contract_employee_price_end_date:last').find('select:last').val();
  if (end_day_value != undefined){
     // alert(end_year_value)
     // alert(end_month_value);
     // alert(end_day_value);
   }
  var new_id = new Date().getTime();
  var regexp = new RegExp("new_" + association, "g")
  console.log(content.replace(regexp, new_id));
  console.log(regexp);
  $(content.replace(regexp, new_id)).insertBefore($(link).parents('.controls').first());
  var start_year_value = $('.employee .controls .contract_employee_price_start_date:last').find('select:first').val();
  var start_month_value = $('.employee .controls .contract_employee_price_start_date:last').find('select:first').next().val();
  var start_day_value = $('.employee .controls .contract_employee_price_start_date:last').find('select:last').val();
  if (start_day_value != undefined && end_day_value != undefined){
    // alert(start_month_value);
    // alert(start_day_value);
    $('.employee .controls .contract_employee_price_start_date:last').find('select:first').val(end_year_value);
    $('.employee .controls .contract_employee_price_start_date:last').find('select:first').next().val(end_month_value);
    $('.employee .controls .contract_employee_price_start_date:last').find('select:last').val(parseInt(end_day_value)+1);
    $('.employee .controls .contract_employee_price_end_date:last').find('select:first').val(end_year_value);
    $('.employee .controls .contract_employee_price_end_date:last').find('select:first').next().val(end_month_value);
    $('.employee .controls .contract_employee_price_end_date:last').find('select:last').val(parseInt(end_day_value)+2);
  }
}



//Returns true if it is a DOM node
function isNode(o){
  return (
    typeof Node === "object" ? o instanceof Node :
    o && typeof o === "object" && typeof o.nodeType === "number" && typeof o.nodeName==="string"
  );
}

//Returns true if it is a DOM element
function isElement(o){
  return (
    typeof HTMLElement === "object" ? o instanceof HTMLElement : //DOM2
    o && typeof o === "object" && o.nodeType === 1 && typeof o.nodeName==="string"
);
}

 $(document).ready(function() {
  $('.multiselect').multiselect({
    buttonClass: 'btn',
    buttonWidth: 'auto',
    buttonContainer: '<div class="btn-group" />',
    maxHeight: '300',
    buttonText: function(options) {
      if (options.length == 0) {
        return 'Select Services <b class="caret"></b>';
      }else if (options.length > 1) {
        return options.length + ' selected <b class="caret"></b>';
      }else {
        var selected = '';
        options.each(function() {
          selected += $(this).text() + ', ';
        });
        return selected.substr(0, selected.length -1) + ' <b class="caret"></b>';
      }
    }
  });
});
