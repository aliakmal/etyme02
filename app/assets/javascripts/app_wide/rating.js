$(function() {
  $('.rating_star').click(function() {
    $star = $(this);
    form_id = $star.attr("data-form-id");
    stars = $star.attr("data-stars");
    $('#' + form_id + '_stars').val(stars);

    $.ajax({
      type: "post",
      url: $('.edit_rating').attr('action') + '.json',
      data: $('form').serialize(),
      success: function(response){
        console.log(response);
        update_stars();
        if(response["avg_rating"]){
          $('#average_rating').text('Average rating: ' + response["avg_rating"] + ' stars');
          $('#success').text('Thanks for your feedback');
          }
        }
      })
  });        
});

function update_stars(){
  $('.rating_star').each(function() {
    form_id = $(this).attr('data-form-id');
    set_stars(form_id, $('#' + form_id + '_stars').val());
  });
}
function set_stars(form_id, stars) {
  for(i = 1; i <= 5; i++){
    if(i <= stars){
      $('#' + form_id + '_' + i).addClass("on");
    } else {
      $('#' + form_id + '_' + i).removeClass("on");
    }
  }
}