# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
$ ->
  $('#tenant_subdomain').keyup ->
    $this = $(this)
    $.get(
      $this.data('vali'),
      subdomain: $this.val()
    )
    .success ->
      $('#dupcompany').css("color", "")
      $('#dupcompany').text('')
      $('input[type=submit]').attr(disabled: false)
    .error ->
      $('#dupcompany').css("color", "red")
      $('#dupcompany').text('This subdomain name is already taken')
      $('input[type=submit]').attr(disabled: true)
