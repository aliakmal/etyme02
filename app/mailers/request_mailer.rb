class RequestMailer < ActionMailer::Base
  default from: Rails.configuration.action_mailer.smtp_settings[:user_name]
  
  def update_to_client(tos, rquest, from_user)
    
    #tos = ['mohammad.ali.akmal@gmail.com', 'it@ramjet.aero']
    subject = 'Update on your request # ' + rquest[:ref].to_s
    @request = rquest
    @current_user = from_user
    cc = Rails.configuration.action_mailer.smtp_settings[:user_name]
    mail(:to=>tos, :cc=>cc, :subject=>subject)
    
  end
  
  def email_to_vendor(tos, vendor_request, from_user)
    #tos = ['mohammad.ali.akmal@gmail.com', 'it@ramjet.aero']
    subject = vendor_request.title
    @vendor_request = vendor_request
    @current_user = from_user
    cc = Rails.configuration.action_mailer.smtp_settings[:user_name]
    mail(:to=>tos, :cc=>cc, :subject=>subject)
  
  end
end
