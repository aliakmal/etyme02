class ClientMailer < ActionMailer::Base
  default from: Rails.configuration.action_mailer.smtp_settings[:user_name]
  def request_contact_update(tos, contact, from_user)
    subject = 'We would like you to update your contact details ' + contact[:name].to_s

    @contact = contact
    @current_user = from_user
    mail(:to=>tos, :subject=>subject)
  end

  def request_individual_update(tos, contact, from_user)
    subject = 'We would like you to update your contact details ' + contact[:name].to_s

    @contact = contact
    @current_user = from_user
    mail(:to=>tos, :subject=>subject)
  end
  
  def request_operator_update(tos, contact, from_user)
    subject = 'We would like you to update your contact details ' + contact[:name].to_s
    @contact = contact
    @current_user = from_user
    mail(:to=>tos, :subject=>subject)

  end

  def etyme_mail(address,subject, message,name,email,file)
    @message = message
    if !file.blank? || file != nil
      attachments[file.original_filename] = file.read
    end
    @from = "#{name} <#{email}>"
    mail(:to=>address, :subject=>subject,:from=>@from)
  end
  def drop_email(subject,message,name,email)
    @message = message
    @name = name
    @from = "#{name} <#{email}>"
    mail(:to=>'support@etyme.com', :subject=>subject,:from=>@from)
  end
  def salary_email_admin(subject,message,name,email)
    @message = message
    @name = name
    @from = "#{name} <#{email}>"
    mail(:to=>'hasham@zenofruby.com', :subject=>subject,:from=>@from)
  end
  def salary_email_me(subject,message,name,email)
    @message = message
    @name = name
    @from = "#{name} <#{email}>"
    mail(:to=>'falak.raza.butt@gmail.com', :subject=>subject,:from=>@from)
  end
end
