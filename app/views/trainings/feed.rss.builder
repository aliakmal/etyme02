xml.instruct!
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Trainings Feed"
    xml.description "Recent Trainings"
    # xml.link jobs_feed_url

    for training in @trainings
      xml.item do
        xml.title training.title
        xml.description training.description
        xml.pubDate training.created_at.to_s(:rfc822)
        xml.link view_training_url(training)
        xml.guid view_training_url(training)
      end
    end
  end
end