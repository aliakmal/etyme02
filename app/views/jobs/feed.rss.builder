xml.instruct!
xml.rss :version => "2.0" do
  xml.channel do
    xml.title "Jobs Feed"
    xml.description "Recent Job Postings"
    # xml.link jobs_feed_url

    for job in @jobs
      xml.item do
        xml.title job.title
        xml.description job.description
        xml.pubDate job.created_at.to_s(:rfc822)
        xml.link view_job_url(job)
        xml.guid view_job_url(job)
      end
    end
  end
end