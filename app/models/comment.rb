class Comment < ActiveRecord::Base

  include ActsAsCommentable::Comment
  include Rails.application.routes.url_helpers
   
  belongs_to :commentable, :polymorphic => true
  acts_as_tenant
  #has_attached_file :attachment

  default_scope :order => 'created_at DESC'

  attr_accessible :commentable, :visible_to, :title, :comment, :user_id #, :attachment

  belongs_to :user

  validates :comment, presence: true
  validates :user_id, presence: true
  #after_create :comment_notifications
  protected
  
  
  def get_url_to_commentable(obj)
    url_for(obj)
  end
  
  module Scopes
    def visible_to_client
      where(:visible_to=>'team_and_client')
    end
  end
  
  extend Scopes

=begin
  def comment_notifications
    if commentable.respond_to?(:creator) && commentable.creator.present?
      unless user == commentable.creator
        Notification.create! do |n|
          n.recipient = commentable.creator
          n.sender = user
          n.subject = "someone commented on #{commentable.class.name} you have created"
          n.object_path = get_url_to_commentable commentable
          n.body = "#{user.name_or_email} just commented on #{commentable.class.name} you have created"
        end
      end
    end

    if commentable.respond_to?(:updater) && commentable.updater.present?
      unless commentable.creator == commentable.updater || user == commentable.updater
        Notification.create! do |n|
          n.recipient = commentable.updater
          n.sender = user
          n.subject = "someone commented on #{commentable.class.name} you have updated"
          n.object_path =  get_url_to_commentable commentable
          n.body = "#{user.name_or_email} just commented on #{commentable.class.name} you have updated"
        end
      end
    end
    #other commentors
    other_comments = commentable.comments.reject { |c| c.id == self.id }
    other_user_ids = other_comments.collect { |c| c.user.id }.reject { |id| id == user.id }
    commenters = User.find(other_user_ids)
    commenters.each do |commenter|
      Notification.create! do |n|
        n.recipient = commenter
        n.sender = user
        n.subject = "someone commented on #{commentable.class.name} you commented on as well"
        n.object_path = get_url_to_commentable commentable
        n.body = "#{user.name_or_email} just commented on #{commentable.class.name} you have commented on as well"
      end
    end
  end
=end
end
