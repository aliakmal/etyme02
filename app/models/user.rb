class User < ActiveRecord::Base
  rolify 
  # :before_add => :check_current_roles
  before_create :set_trial_period
  
  def remove_current_roles(role)
      # do something before it gets added
      if self.roles.count >0 then
        if self.has_role? :admin, Tenant.find(self.roles.first.resource_id)
          self.remove_role 'admin', Tenant.find(self.roles.first.resource_id)
          true
        elsif self.has_role? :exp_user, Tenant.find(self.roles.first.resource_id)
          self.remove_role 'exp_user', Tenant.find(self.roles.first.resource_id)
          true
        else
          false
        end
    end
  end
  # Include default devise modules. Others available are:
  # :token_authenticatable, :confirmable,
  # :lockable, :timeoutable and :omniauthable, :registerable,
  devise :invitable,:database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :confirmable

  
  #attr_protected :username, :name, :email, :remember_me, :password, :password_confirmation
  # validates :username, :presence => true
  validates_uniqueness_of :username
  validates_presence_of :username, :email
  validates :email, :format => { :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/ ,
                     :message => 'Invalid e-mail format! Please provide a valid e-mail address'}
  validates_uniqueness_of :email
  acts_as_universal_and_determines_account
  has_many :jobs_as_recruiter, :class_name=>'Job', :foreign_key=>'recruiter_id'
  has_many :jobs_as_owner, :class_name=>'Job', :foreign_key=>'owner_id'
  has_many :ratings
  has_one :individual, :dependent => :destroy

  has_many :inquiries, :through =>:individual

  accepts_nested_attributes_for :individual, :inquiries
  # accepts_nested_attributes_for :individual, :reject_if => proc { |attributes| attributes['email'].blank? }
  #  accepts_nested_attributes_for :inquiries
  # Setup accessible (or protected) attributes for your model
  attr_accessible :role_ids, :password, :password_confirmation, :username, :stripe_card_token, :name, :email, :recruiter_id, :as => :admin
  attr_accessible :password, :password_confirmation, :username, :name, :email, :stripe_card_token, :remember_me, :candidate_attributes, :inquiries_attributes

  attr_accessible :contact_details_attributes, :phone_attributes, :fax_attributes, :email_attributes, :im_attributes
  attr_accessible :address_detail_attributes, :addresses_attributes, :individual_attributes
  attr_accessible :phone, :fax, :email, :inquiries_attributes
  
  attr_accessor :stripe_card_token

  def is_candidate?
    self.has_role? :candidate
  end
  def set_trial_period
    self.trial_period_active = true
    self.trial_period_days = 30
    #self.subscription_status = false
  end
  def stripe_payment
    if valid?
      customer = Stripe::Customer.create(description: email, plan: 'pro', card: stripe_card_token)
      self.stripe_customer_token = customer.id
      self.subscription_end_date = Time.at(customer.subscriptions.first.trial_end).to_datetime.utc
      self.trial_period_active = false
      self.subscription_status = true
      save!
    end
  rescue Stripe::APIError => e
    logger.error "Stripe Authentication error while updating user: #{e.message}"
    errors.add :base, "Our system is temporarily unable to process credit cards"
    false
  end

end
