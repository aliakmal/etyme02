class Training < ActiveRecord::Base
  attr_accessible :description, :title, :duration, :location, :start_date, :end_date, :full_time , :published, :inquiries_attributes, :individuals_attributes, :status
  acts_as_tenant
  acts_as_commentable
  validates :title, presence: true
  validates :description, presence: true
  validates_uniqueness_of :title, :scope => :tenant_id



  belongs_to :trainer, class_name: 'User'

  has_many :ratings
  has_many :inquiries
  has_many :individuals, :through =>:inquiries
  accepts_nested_attributes_for :individuals, :inquiries

  module Scopes
    def by_name(prefix)
      where("title LIKE :prefix", prefix: "#{prefix}%")
    end
  end

  
  scope :starts_with, proc { |letter| where("title LIKE :letter", letter: "#{letter}%") }
  scope :by_name, proc { |search| where("title LIKE :search", search: "#{search}%") }
  scope :by_title, proc { |search| where("title LIKE :search", search: "#{search}%") }
  scope :published,  where("published IS TRUE")
  extend Scopes

  def location
  	self[:location]
  end

  def excerpt
  	strip_tags(self.description[0,240])
  end

  scope :by_candidate, lambda{ |user|
    joins(:inquiries).where("inquiries.individual_id=?", user.individual.id) 
  }

  def has_candidate_applied?(candidate_id)
    rs = self.inquiries.where(:individual_id => candidate_id)
    rs.count > 0
  end

  def avg_rating
    average_rating = 0.0
    count = 0
    ratings.each do |rating| 
      average_rating += rating.stars
      count += 1
    end
                  
    if count != 0
      (average_rating / count)
    else
      count
    end
  end

end
