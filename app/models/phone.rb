class Phone < ContactDetail
  belongs_to :contact
  attr_accessible :type
end