class Invoice < ActiveRecord::Base
  attr_accessible :ref, :state, :total, :timesheets, :timesheets_attributes, :time_entries_attributes, :total_rate, :total_salary, :due_date, :contract_id
  has_many :timesheets, :dependent => :destroy
  has_many :time_entries, :through => :timesheets, :dependent => :destroy
  belongs_to :contract
  accepts_nested_attributes_for :timesheets, :time_entries
  acts_as_tenant
  # after_save :auto_invoicing
  # after_create :setup_totals
  # after_create :change_state_of_timesheets
  attr_accessor :con_id,:tid
  
  def startup
    self.timesheets.each do |timesheet|
      timesheet.time_entries.each do |time_entry|
        time_entry.rate = time_entry.charge_customer
      end

    end
  end

  def setup_totals
    self.total_rate = self.compute_total_rate
    self.save
  end
  def setup_rate(total_rate)
    self.total_rate = total_rate
    self.save
  end

  def change_state_of_timesheets

    self.timesheets.each do |timesheet|
      timesheet.invoiceit
      timesheet.save
    end
  end

  def compute_total_rate
    self.time_entries.sum(:rate)
  end
  def compute_total_salary
    self.time_entries.sum(:salary)
  end
end
