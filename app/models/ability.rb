class Ability
  include CanCan::Ability

  def initialize(user)
    user ||= User.new # guest user (not logged in)
   if user.roles.first.resource_type == 'Tenant'
      #tenant = user.roles.first.resource_id
      tenant = Tenant.find(user.roles.first.resource_id)
    end
    #tenant ||= Tenant.new
    if user.has_role? :admin, tenant
      can :manage, :all
    end
    
    if user.has_role? :candidate, tenant
      #can :manage, Request
      #can :manage, Aircraft
      #can :manage, Operator
      #can :manage, Sector
      #can :manage, Service
      can :manage, Comment
      can :manage, Inquiry
      can :manage, Invoice
      can :manage, Timesheet
      can :manage, TimeEntry
      can [:index,:accounts], :home
      can :manage, :candidates
      can [:apply,:portal,:applied,:my_jobs,:show], Job
      can [:apply,:portal,:applied,:my_trainings,:show], Training
      #can :manage, ServiceType
      #can :manage, User
    end
    if user.has_role? :exp_user, tenant
      can [:index,:processing], :payment
      can [:index], :home
    end
    # Define abilities for the passed in user here. For example:
    #
    #   user ||= User.new # guest user (not logged in)
    #   if user.admin?
    #     can :manage, :all
    #   else
    #     can :read, :all
    #   end
    #
    # The first argument to `can` is the action you are giving the user permission to do.
    # If you pass :manage it will apply to every action. Other common actions here are
    # :read, :create, :update and :destroy.
    #
    # The second argument is the resource the user can perform the action on. If you pass
    # :all it will apply to every resource. Otherwise pass a Ruby class of the resource.
    #
    # The third argument is an optional hash of conditions to further filter the objects.
    # For example, here the user can only update published articles.
    #
    #   can :update, Article, :published => true
    #
    # See the wiki for details: https://github.com/ryanb/cancan/wiki/Defining-Abilities
  end
end
