class TimeEntry < ActiveRecord::Base
  attr_accessible :id, :timesheet_id, :state, :total_minutes, :for_date, :attachment, :rate, :salary

  #validates_numericality_of :total_minutes
  #:candidate_id, :contract_id, :desc, :end_at, :start_at,  
  belongs_to :timesheet
  default_scope :order => 'for_date ASC'
  acts_as_tenant
  mount_uploader :attachment, FileUploader


  state_machine :state, :initial => :unapproved do

    event :approve do
      transition :unapproved => :approved
    end

    event :unapprove do
      transition :approved => :unapproved
    end

    event :unreject do
      transition :rejected => :unapproved
    end


    event :reject do
      transition :unapproved => :rejected
    end
  end

  scope :on_date, proc { |date| where( :for_date => date.to_date.to_s ) }

  def minutes_in_hours
    minutes = self.total_minutes.to_i % 60
    hours = self.total_minutes.to_i / (60)
    format("%02d hrs %02d mts", hours, minutes )

  end

  def charge_customer
    self.timesheet.contract.customer_price.each do |price|
        if self.timesheet.ends <= price.end_date.to_date
          @cc = (self.total_minutes.to_i / (60)) * price.sale_price
        else
          next
        end
      end
      @cc
  end

  def pay_employee
    self.timesheet.contract.employee_price.each do |price|
      if self.timesheet.ends <= price.end_date.to_date
        @pe = (self.total_minutes.to_i / (60)) * price.cost_price
      else
        next
      end
    end
    @pe
  end
  def pay_vendor
    self.timesheet.contract.vendor_price.each do |price|
      if self.timesheet.ends <= price.end_date.to_date
        @pv = (self.total_minutes.to_i / (60)) * price.cost_price
      else
        next
      end
    end
    @pv
  end
end
