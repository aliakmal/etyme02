class Rating < ActiveRecord::Base
  
  belongs_to :training
  belongs_to :user
  attr_accessible :stars, :training_id, :user_id
  
end
