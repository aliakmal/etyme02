class MonthlyTimesheet < Timesheet
  attr_accessible :type
  after_create :set_start_end_dates
  def set_start_end_dates
    if self.starts.blank? then
      self.starts = Date.today.beginning_of_month
    end

    self.ends = self.starts.end_of_month
    ((self.starts.to_date)...(self.ends.to_date)).each do|date|
      self.time_entries.create(:for_date => date)
    end

    self.save
  end
end
