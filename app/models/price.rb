class Price < ActiveRecord::Base
  acts_as_tenant
  belongs_to :contract
  belongs_to :priceable, :polymorphic => true
  attr_accessible :cost_price, :end_date, :master_services_agreement, :non_compete_agreement, :price_type, :purchase_orders, :sale_price, :start_date, :is_price_displayed, :check_cost_price, :check_sale_price, :type
  mount_uploader :master_services_agreement, FileUploader
  mount_uploader :non_compete_agreement, FileUploader
  mount_uploader :purchase_orders, FileUploader
  attr_accessor :check_cost_price, :check_sale_price
  validates_presence_of :cost_price, :if => :check_cost_price
  validates_presence_of :sale_price, :if => :check_sale_price
  
end
