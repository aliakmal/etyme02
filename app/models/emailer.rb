class Emailer< ActiveRecord::Base

  def self.columns() @columns ||= []; end
 
  def self.column(name, sql_type = nil, default = nil, null = true)
    columns << ActiveRecord::ConnectionAdapters::Column.new(name.to_s, default, sql_type.to_s, null)
  end
  
  column :from, :string
  column :to, :text
  column :to_emails, :text
  column :body, :text  
  column :emailable_type, :string
  column :emailable_id, :integer

  attr_accessible :from, :to, :to_emails, :body, :emailable_type, :emailable_id
  
  def self.initialize_from_request(request)
    Emailer.new(:to=>request.customer.email.map(&:details), :from=>'ops@ramjet.aero', :body=>'asdasdasddas', :emailable_type=>'Request', :emailable_id=>request[:id] )
  end

  def self.initialize_for_vendor(vendor_request, vendor)
    
    Emailer.new(:to=>vendor.email.map(&:details), :from=>'ops@ramjet.aero', :body=>'test', :emailable_type=>'VendorRequest', :emailable_id=>vendor_request[:id] )
  end


  def get_recipients
    recipients = self.to + self.to_emails.split(',')
    recipients.reject!(&:empty?).collect(&:strip)
  end
  
  validate :must_have_atleast_one_email
  validate :emails_should_all_be_valid
  
  def must_have_atleast_one_email
    if self.get_recipients.count == 0 then
      errors.add(:to_emails, 'Need atleast one email')
    end
  end
  
  def emails_should_all_be_valid
    self.get_recipients.each do |email|
      unless email.match(/^([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})$/i)
        errors.add(:to_emails, "#{email} is not a valid email address.")
      end
    end
  end

end