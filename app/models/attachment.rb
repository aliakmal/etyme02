class Attachment < ActiveRecord::Base
  belongs_to :contract
  acts_as_tenant
  attr_accessible :attachment, :name
  mount_uploader :attachment, FileUploader
end
