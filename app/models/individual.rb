class Individual < Contact
  acts_as_citier
  acts_as_commentable
  acts_as_tenant
  audited
  has_associated_audits
  #rolify
  belongs_to :company
  attr_accessible :description, :company_id, :designation, :first_name, :last_name, :salutation, :type, :ind_email
  #, :counter_cache => :individuals_count
  attr_accessible :contact_detail_attributes, :phone_attributes, :fax_attributes, :email_attributes, :im_attributes
  attr_accessible :address_detail_attributes, :addresses_attributes
  attr_accessible :phone, :fax, :email, :cv, :check_email, :type
  attr_accessor :check_email

  attr_accessible :company_name
  
  attr_accessible :tags_as_text, :tag_list
  attr_taggable :tags
  before_save :set_parent_attributes
  
  validates_presence_of :first_name, :last_name
  validates_presence_of :ind_email, :if => :check_email
  validates_uniqueness_of :ind_email, :if => :check_email, :scope => :tenant_id
  validates :ind_email, :if => :check_email, :format => { :with => /\A[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Za-z]+\z/ ,
                     :message => 'Invalid e-mail! Please provide a valid e-mail address'}

  belongs_to :user
  has_many :inquiries, :foreign_key => "individual_id", :class_name=>'Inquiry', :dependent => :destroy
  has_many :jobs, :through =>:inquiries
  has_many :trainings, :through =>:inquiries


  has_many :contracts, :class_name=>'Contract', :foreign_key=>:candidate_id
  accepts_nested_attributes_for  :inquiries
  has_many :commissions

  attr_accessible :inquiries_attributes

  mount_uploader :cv, FileUploader

  #virtual attributes
  def tag_list=(tag_list)
    @tag_list = tag_list
    self.tags = @tag_list.split(',')
  end

  def tag_list
    @tag_list = self.tags.join(',')
    @tag_list
  end
  def check_validation(params)
    params.has_key?
  end
  
  def tags_as_text=(tags_as_text)
    @tags_as_text = tags_as_text
  end

  def tags_as_text
    @tags_as_text
  end

  def is_candidate?
    self.user.has_role? :candidate
  end

  
  def company_name=(name)
    @company_name = name
    if !name.blank? then
      self.company = Company.find_or_initialize_by_company_name(name)

      if self.company.id.blank? || self.company.id == 0 then
        self.company_id = nil
      else
        self.company_id = self.company.id
      end
    else
      self.company_id = 0
    end
  end

  def company_name
    if !self.company.blank? then
      @company_name = self.company.company_name
    end
    @company_name
  end  
  
  def set_parent_attributes
    self[:name] = self[:first_name]+ ' ' + self[:last_name]
    #self[:about] = self[:description]
  end
  
  def is_hired_for_job(job)
    self.contracts.by_job_id(job.id).count > 0 
  end
  
  scope :starts_with, proc { |letter| where("name LIKE :letter", letter: "#{letter}%") }
  scope :by_name, proc { |search| where("name LIKE :search", search: "#{search}%") }
  scope :only_candidates, lambda {  joins(:user=>:roles).where('roles.name = ?', 'candidate')}

  
end