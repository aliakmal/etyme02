class Inquiry < ActiveRecord::Base
  belongs_to :individual 
  belongs_to :job
  belongs_to :training
  attr_accessible :job_id, :individual_id, :training_id, :state, :cv, :best_time_to_call, :website, :current_employer
  attr_accessible :date_available, :current_pay, :desired_pay,  :can_relocate
  acts_as_tenant
  mount_uploader :cv, FileUploader
  scope :are_accepted, proc { where(:state => 'accepted') }

  state_machine :state, :initial => :pending do

    event :accept do
      transition :pending => :accepted
    end

    event :reject do
      transition :pending => :rejected
    end

    event :unaccept do
      transition :accepted => :pending
    end

    event :unreject do
      transition :rejected => :pending
    end
	end

  #has_many :users, :through =>:candidates
end
