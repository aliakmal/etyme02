class Commission < ActiveRecord::Base
  acts_as_tenant
  has_many :timesheets
  belongs_to :user
  belongs_to :individual
  belongs_to :contract
  attr_accessible :commission_type, :fixed_commission, :hourly_commission, :monthly_commission, :contract_id, :individual_id, :timesheets, :timesheets_attributes, :is_commission_displayed
  validates_presence_of :individual,:commission_type
end
