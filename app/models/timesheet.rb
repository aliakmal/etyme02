class Timesheet < ActiveRecord::Base
  attr_accessible :contract_id, :ends, :minutes, :starts, :state, :type, :attachment, :time_entries_attributes
  attr_accessible :invoice_id,:commission_id, :timesheet_document, :check_doc, :salary_payment_status
  attr_accessor :check_doc
  acts_as_tenant
  scope :by_contract_id, proc { |contract_id| where(:contract_id => contract_id) }
  scope :are_approved, proc { where(:state => 'approved') }
  scope :are_approved_or_invoiced, proc { where('state = ? OR state = ?', 'invoiced','approved') }
  mount_uploader :timesheet_document, FileUploader
  has_many :time_entries
  belongs_to :invoice
  belongs_to :commission
  belongs_to :contract
  validates_presence_of :timesheet_document, :if => :check_doc
  accepts_nested_attributes_for :time_entries
  has_many :documentables, :as => :documentable
  has_many :documents, :through => :documentables


  state_machine :state, :initial => :unapproved do

    event :pending do
      transition :unapproved => :pending
    end

    event :approve do
      transition :pending => :approved
    end

    event :invoiceit do
      transition :approved => :invoiced
    end

    event :unapprove do
      transition :approved => :unapproved
    end

    event :unreject do
      transition :rejected => :unapproved
    end


    event :reject do
      transition :pending => :rejected
    end
  end

  def self.get_current_timesheet(contract_id)
    tm = self.where('starts <= ? AND ends >= ? AND contract_id = ?', Date.today, Date.today, contract_id).first
    if tm.blank? then
      # check is it a weekly or monthly timesheet
      cntract = Contract.find(contract_id)


      if cntract.timesheet_type == 'Monthly' then
        tm = MonthlyTimesheet.create(:contract_id => contract_id)
      elsif cntract.timesheet_type == 'Weekly' then
        tm = WeeklyTimesheet.create(:contract_id => contract_id)
      end
    end
    tm
  end

  #after_save :set_total_minutes

  def set_total_minutes
    if self.time_entries.count >0 then
      minutes = self.time_entries.sum(:total_minutes)
    else
      minutes = 0
    end
    self.update_attributes(:minutes => minutes)
  end


  def minutes_in_hours
    minutes = self.minutes.to_i % 60
    hours = self.minutes.to_i / (60)
    format("%02d hours %02d minutes", hours, minutes )
  end

  def time_entry_on_date(adate)
    te = self.time_entries.on_date(adate)
    if te.blank? then
      TimeEntry.new
    else
      te.first
    end
  end

end
