class Contract < ActiveRecord::Base

  attr_accessible :candidate_id,  :job_id, :employee_type, :timesheet_type, :contract_start_date, :payment_terms, :due_invoice_day, :status
  attr_accessible :time_entries_attributes, :timesheets_attributes, :billable_periods_attributes, :commissions_attributes, :prices_attributes, :attachments_attributes, :customer_price_attributes, :employee_price_attributes, :vendor_price_attributes
  attr_accessor :comp
  validates_presence_of :timesheet_type, :contract_start_date, :payment_terms, :job, :individual, :due_invoice_day
  has_many :billable_periods
  has_many :time_entries
  has_many :timesheets, :dependent => :destroy
  has_many :invoices
  has_many :receipts
  has_many :commissions, :dependent => :destroy
  has_many :prices,:as => :priceable, :dependent => :destroy
  has_many :customer_price,:as => :priceable, :dependent => :destroy
  has_many :employee_price,:as => :priceable, :dependent => :destroy
  has_many :vendor_price,:as => :priceable, :dependent => :destroy
  has_many :attachments, :dependent => :destroy
  accepts_nested_attributes_for :billable_periods, :time_entries, :commissions, :prices, :attachments
  accepts_nested_attributes_for :customer_price, :employee_price, :vendor_price
  validate :validate_job_candidate, :on => :create
  belongs_to :job
  belongs_to :individual, :class_name=>'Individual', :foreign_key => :candidate_id
  has_many :documentables, :as => :documentable
  has_many :documents, :through => :documentables
  acts_as_tenant
  after_create :create_first_timesheet_for_candidate
  def contract_begins_on
  	self.order('contract_start_date ASC').contract_start_date
  end

  def create_first_timesheet_for_candidate
    if self.timesheet_type == 'Monthly' then
      MonthlyTimesheet.create(:contract_id => self.id, :starts => self.contract_start_date)
    elsif self.timesheet_type == 'Weekly' then
      WeeklyTimesheet.create(:contract_id => self.id, :starts => self.contract_start_date)
    end
  end

  def start_date
    self.contract_start_date
  end

  def current_salary
    self.billable_periods.first.salary
  end

  def current_rate
    self.billable_periods.first.rate
  end

  def hours_logged
    0
  end

  def minutes_on_date(adate)
    te = self.time_entries.on_date(adate)
    if te.blank? then
      ''
    else
      te.first[:total_minutes]
    end
  end

  def time_entry_on_date(adate)
    te = self.time_entries.on_date(adate)
    if te.blank? then
      ''
    else
      te
    end
  end

  scope :by_job_id, proc { |job_id| where(:job_id => job_id) }
  scope :by_candidate, lambda{ |user|
    where( :candidate_id=> user.individual.id) 
  }
  def validate_job_candidate
    if Contract.where(:candidate_id => candidate_id,:job_id => job_id ).count > 0
      self.errors.add :base, 'This candidate was already hired for this job..'
    end
  end

end
