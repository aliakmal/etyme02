class BillablePeriod < ActiveRecord::Base
  attr_accessible :contract_id, :rate, :salary, :start_date
  validates_presence_of :rate,:salary, :start_date
  validates_numericality_of  :salary, :rate
  scope :for_date, proc{|the_date| where('start_date <= ?', the_date).order('start_date DESC')}
  acts_as_tenant

end
