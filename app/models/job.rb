class Job < ActiveRecord::Base
  attr_accessible :city, :company_job_id, :duration, :job_type, :maximum_rate, :openings, :description
  attr_accessible :salary, :start_date, :state, :title, :zip, :recruiter_id, :owner_id, :status, :published, :service_type_ids
  acts_as_tenant
  validates_presence_of :start_date, :title, :description, :company
  validates_uniqueness_of :title, :scope => :tenant_id

  scope :starts_with, proc { |letter| where("title LIKE :letter", letter: "#{letter}%") }
  scope :by_title, proc { |search| where("title LIKE :search", search: "#{search}%") }
  scope :published,  where("published IS TRUE")
  has_many :inquiries, :dependent => :destroy

  has_many :individuals, :through =>:inquiries
  has_many :contracts
  belongs_to :recruiter, :class_name => 'User'
  belongs_to :owner, :class_name=>'User'
  belongs_to :company
  has_many :jobs_service_types

  has_many :service_type, :through => :jobs_service_types

  def location
  	self[:city]+','+self[:state]
  end

  def excerpt
  	strip_tags(self.description[0,240])
  end

  scope :by_candidate, lambda{ |user|
    joins(:inquiries).where("inquiries.individual_id=?", user.individual.id) 
  }

  def has_candidate_applied?(candidate_id)
    rs = self.inquiries.where(:individual_id => candidate_id)
    rs.count > 0
  end
  def self.to_csv(options = {})
  CSV.generate(options) do |csv|
    csv << column_names
    all.each do |job|
      csv << job.attributes.values_at(*column_names)
    end
  end
end

end
