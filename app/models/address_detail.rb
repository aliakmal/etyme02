class AddressDetail < ActiveRecord::Base
  attr_accessible :address, :city, :country, :state, :zip, :address_id
  belongs_to :address, :foreign_key => :address_id
  liquid_methods :city, :country, :state, :zip, :address
  acts_as_tenant
end
