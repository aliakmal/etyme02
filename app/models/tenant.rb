class Tenant < ActiveRecord::Base
  attr_accessible :cname, :company, :subdomain, :salary_terms, :salary_gap_terms, :salary_payment_day
  acts_as_universal_and_determines_tenant
  resourcify
  validates_uniqueness_of :subdomain
  validates_presence_of :subdomain

  def self.create_new_tenant(params)
    tenant = Tenant.new(cname: params[:user][:email], company: params[:tenant][:company], subdomain: params[:tenant][:subdomain].parameterize)
    if new_signups_not_permitted?(params)
      raise ::Milia::Control::MaxTenantExceeded, "Sorry, new accounts are not permitted at this time"
    else
      tenant.save
    end
    tenant
  end

  class << self
    def new_signups_not_permitted?(params)
      false
    end
  end

  def self.tenant_signup(user, tenant, other = nil)
    #TODO: nothing here yet
    user.add_role :admin, tenant
    user.add_role :employee, tenant
    user.add_role :hr_manager, tenant
    user.add_role :time_admin, tenant
    user.add_role :recruiter, tenant
    user.add_role :account_manager, tenant
    user.add_role :trainer, tenant
    user.add_role :trainee, tenant
  end
   # def new_roles
   #   current_user.add_role :employee, current_tenant
   #   current_user.add_role :hr_manager, current_tenant
   #   current_user.add_role :time_admin, current_tenant
   #   current_user.add_role :recruiter, current_tenant
   #   current_user.add_role :account_manager, current_tenant
   # end

end
