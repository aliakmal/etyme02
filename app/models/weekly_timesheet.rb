class WeeklyTimesheet < Timesheet
  attr_accessible :type
  after_create :set_start_end_dates
  def set_start_end_dates
    if self.starts.blank? then
      self.starts = Date.today.beginning_of_week
    end

    self.ends = self.starts.end_of_week
    ((self.starts.to_date)...(self.ends.to_date + 1.days)).each do|date|
      self.time_entries.create(:for_date => date)
    end

    self.save
  end

end
