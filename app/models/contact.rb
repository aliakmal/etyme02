class Contact < ActiveRecord::Base
  acts_as_citier
  #acts_as_tenant

  
  include PublicActivity::Model
  include Bootsy::Container
  tracked
  activist

  attr_accessible :name, :about, :type, :service_type_ids, :token
  #default_scope order('name ASC')
  # validates_presence_of :name
  # has_many :service_types_as_prefered_contact, :class_name=>'ServiceType'

  #has_many :requests_as_operator, :class_name=>'Request', :foreign_key=>'operator_id'
  #has_many :requests_as_customer, :class_name=>'Request', :foreign_key=>'customer_id'
  has_many :contact_details, :as => :contactable, :dependent => :destroy
  has_many :phone, :as => :contactable, :dependent => :destroy
  has_many :fax, :as => :contactable, :dependent => :destroy
  has_many :email, :as => :contactable, :dependent => :destroy
  has_many :im, :as => :contactable, :dependent => :destroy
  has_many :addresses, :as => :contactable, :dependent => :destroy
  has_and_belongs_to_many :service_types

  accepts_nested_attributes_for :phone, :fax, :email, :addresses, :im
  accepts_nested_attributes_for :contact_details

  has_many :contacts_service_types
  #has_many :service_types, :through => :contacts_service_types
  #liquid_methods :name
  
  # set the default type on the contact to company incase there is no value provided
  #define_index do
  #  indexes :name
  #  indexes :about
  #  indexes requests_as_operator(:ref)
  #  indexes requests_as_customer(:ref)
  #end
  before_save :set_default_type
  attr_taggable :tags

  def set_default_type
    
  end

  def delete_token
    self[:token] = nil
    self.save
  end

  def get_token
    if self[:token] == nil then
      self.set_token
    end
    self[:token]
  end

  def set_token
    self[:token] = SecureRandom.hex(12)
    self.save
  end

  def num_contacts
    #count(self.phone) + count(self.fax) + count(self.email) + count(self.aftn) + count(self.im) + count(self.typeb) + count(self.radio) + count(self.icao)
  end

  module Scopes
    def by_name(prefix)
      where("name LIKE :prefix", prefix: "#{prefix}%")
    end
  end



  scope :starts_with, proc { |letter| where("name LIKE :letter", letter: "#{letter}%") }
  scope :by_name, proc { |search| where("name LIKE :search", search: "#{search}%") }
  scope :by_type, proc { |type| where(:type=> type) }
  scope :by_company_type, proc { |company_type|joins('inner join companies on contacts.id=companies.id').where('companies.company_type = \''+company_type+'\'')  }
  scope :by_services, proc { |services| joins('inner join contacts_service_types on contacts.id=contacts_service_types.contact_id').where('contacts_service_types.service_type_id in ('+(services+[0]).join(',')+')')  }
  #scope :tagged , proc{|tags| tagged_with(tags)}
  extend Scopes
=begin
  
=end
  
  def first_phone
    if self.phone.count > 0 then
      self.phone.first.details
    else
      ''
    end
  end
  
  def second_phone
    if self.phone.count > 1 then
      self.phone[1].details
    else
      ''
    end
  end
  
  def third_phone
    if self.phone.count > 2 then
      self.phone[2].details
    else
      ''
    end
  end

  def first_fax
    if self.fax.count > 0 then
      self.fax.first.details
    else
      ''
    end
  end

  def first_email
    if self.email.count > 0 then
      self.email.first.details
    else
      ''
    end
  end
  
  def second_email
    if self.email.count > 1 then
      self.email[1].details
    else
      ''
    end
  end
  
  def third_email
    if self.email.count > 2 then
      self.email[2].details
    else
      ''
    end
  end
  
  def first_address
    if self.addresses.count > 0 then
      address = self.addresses.first
    else
      ''
    end
  end
  
  def second_address
    if self.addresses.count > 1 then
      address = self.addresses[1]
    else
      ''
    end
  end

  def third_address
    if self.addresses.count > 2 then
      address = self.addresses[2]
    else
      ''
    end
  end
  
  def first_im
    if self.im.count > 0 then
      self.im.first.details
    else
      ''
    end
  end
  
  def second_im
    if self.im.count > 1 then
      self.im[1].details
    else
      ''
    end
  end
  
  def third_im
    if self.im.count > 2 then
      self.im[2].details
    else
      ''
    end
  end
  def first_im_kind
    if self.im.count > 0 then
      self.im.first.location
    else
      ''
    end
  end
  
  def second_im_kind
    if self.im.count > 1 then
      self.im[1].location
    else
      ''
    end
  end
  
  def third_im_kind
    if self.im.count > 2 then
      self.im[2].location
    else
      ''
    end
  end
  
  def to_string
    self.name
  end

    
  
  
  def self.import_about_attribs(row)
    fields = ["Assistant's Name","Assistant's Phone", "Telex","Account","Billing Information","ISDN", "Gender","Government ID Number",
              "Hobby","Home Address PO Box","Initials","Internet Free Busy","Keywords","Language","Location","Manager's Name","Mileage","Notes","Office Location",
              "Organizational ID Number","Other Address PO Box","Priority","Private","Profession","Referred By","Sensitivity","Spouse","User 1","User 2","User 3","User 4",
              "Web Page"]
    about = ''
    fields.each do |index|
      if !row[index].blank? then
        about = about + index.to_s + ' : ' + row[index].to_s + "\n"
      end
    end
    about
  end
  
  def self.import_address_attribs(row)
    attribs = []

    if !row["Business Country/Region"].blank? then
      attribs.push({ :type=>'Address', :location=>'Work',:details=>'', 
          :address=>row["Business Street"] + ' ' + row["Business Street 2"] + ' ' + row["Business Street 3"],
          :city => row["Business City"], :state => row["Business State"], :zip => row["Business Postal Code"], 
          :country => row["Business Country/Region"]  })
    end

    if !row["Home Country/Region"].blank? then
      attribs.push({ :type=>'Address', :location=>'Work',:details=>'', 
          :address=>row["Home Street"] + ' ' + row["Home Street 2"] + ' ' + row["Home Street 3"],
          :city => row["Home City"], :state => row["Home State"], :zip => row["Home Postal Code"], 
          :country => row["Home Country/Region"]  })
    end

    if !row["Other Country/Region"].blank? then
      attribs.push({ :type=>'Address', :location=>'Work',:details=>'', 
          :address=>row["Other Street"] + ' ' + row["Other Street 2"] + ' ' + row["Other Street 3"],
          :city => row["Other City"], :state => row["Other State"], :zip => row["Other Postal Code"], 
          :country => row["Other Country/Region"]  })
    end
    attribs
  end
  
  def self.import_email_attribs(row)
    attribs = []
  
    if !row["E-mail Address"].blank? then
      attribs.push({ :type=>'Email', :location=>'Work',:details=>row["E-mail Address"]})
    end

    if !row["E-mail 2 Address"].blank? then
      attribs.push({:type=>'Email', :location=>'Work',:details=>row["E-mail 2 Address"]})
    end

    if !row["E-mail 3 Address"].blank? then
      attribs.push({ :type=>'Email', :location=>'Work',:details=>row["E-mail 3 Address"]})
    end
    attribs
  end


  def self.import_phone_attribs(row)

    attribs = []
    if !row["Mobile Phone"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Cell',:details=>row["Mobile Phone"]})
    end

    if !row["Car Phone"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Cell',:details=>row["Car Phone"]})
    end

    if !row["Other Phone"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Other',:details=>row["Other Phone"]})
    end

    if !row["Radio Phone"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Cell',:details=>row["Radio Phone"]})
    end

    if !row["Primary Phone"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Work',:details=>row["Primary Phone"]})
    end

    if !row["Business Phone"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Work',:details=>row["Business Phone"]})
    end

    if !row["Company Main Phone"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Work',:details=>row["Company Main Phone"]})
    end
        
    if !row["Business Phone 2"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Work',:details=>row["Business Phone 2"]})
    end

    if !row["Pager"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Pager',:details=>row["Pager"]})
    end

    if !row["Business Fax"].blank? then
      attribs.push({ :type=>'Fax', :location=>'Work',:details=>row["Business Fax"]})
    end

    if !row["Home Fax"].blank? then
      attribs.push({ :type=>'Fax', :location=>'Home', :details=>row["Home Fax"]})
    end

    if !row["Other Fax"].blank? then
      attribs.push({ :type=>'Fax', :location=>'Other', :details=>row["Other Fax"]})
    end

    if !row["Home Phone"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Home',:details=>row["Home Phone"]})
    end

    if !row["Home Phone 2"].blank? then
      attribs.push({ :type=>'Phone', :location=>'Home',:details=>row["Home Phone 2"]})
    end
    attribs
  end

  def self.import(file)
    CSV.foreach(file.path, headers: true) do |row|
        contact = {    'phone_attributes'=>[],
                       'email_attributes'=>[],
                       'addresses_attributes'=>[],
                       'im_attributes'=>[]}
        
        
      contact['phone_attributes'] = self.import_phone_attribs(row)
      contact['email_attributes'] = self.import_email_attribs(row)
      contact['addresses_attributes'] = self.import_address_attribs(row)
      contact['about'] = self.import_about_attribs(row)
        
        

      if row['Kind'] == 'Individual' || !row['First Name'].blank? then
        contact['first_name'], contact['last_name'], contact['designation'], contact[:company_name], contact[:tag_list] = row['First Name'], row['Last Name'], row['Title'], row['Company'], row['Categories'].to_s
        

        individual = Individual.new(contact)
        if individual.valid? then
          individual.save()
        end
 
      else
        contact[:company_name], contact[:tag_list] = row['Company'], row['Categories']

        company = Company.new(contact)
        if company.valid? then
          company.save()
        end
      end
      #Product.create! row.to_hash
    end
  end


  
  def self.to_csv(contacts)
    columns = ['ID','Kind','Name','First name','Last name','Company','Title','Background','Tags',
               'Address - Work Street','Address - Work City','Address - Work State','Address - Work Zip','Address - Work Country',
               'Address - Home Street','Address - Home City','Address - Home State','Address - Home Zip','Address - Home Country',
               'Address - Other Street','Address - Other City','Address - Other State','Address - Other Zip','Address - Other Country',
               'Phone number - Work','Phone number - Mobile','Phone number - Fax','Phone number - Pager','Phone number - Home','Phone number - Skype','Phone number - Other',
               'Email address - Work','Email address - Home','Email address - Other',
               'Web address - Work','Web address - Personal','Web address - Other',
               'Instant messenger kind - Work','Instant messenger - Work','Instant messenger kind - Personal','Instant messenger - Personal','Instant messenger kind - Other','Instant messenger - Other']
    
    export = CSV.generate do |csv|
      csv << columns
      
      contacts.each do |contact|
        
        if contact[:type] == 'Individual' then
          row = [ contact[:id], 'Individual', contact[:name], contact[:first_name],contact[:last_name], contact.company_name,contact[:designation],contact[:about], contact.tag_list,
                  (contact.first_address != '' ? contact.first_address.address : ''), (contact.first_address != '' ? contact.first_address.city : ''), (contact.first_address != '' ? contact.first_address.state : ''), (contact.first_address != '' ? contact.first_address.zip : ''), (contact.first_address != '' ? contact.first_address.country : ''),
                  (contact.second_address != '' ? contact.second_address.address : ''), (contact.second_address != '' ? contact.second_address.city : ''), (contact.second_address != '' ? contact.second_address.state : ''), (contact.second_address != '' ? contact.second_address.zip : ''), (contact.second_address != '' ? contact.second_address.country : ''),
                  (contact.third_address != '' ? contact.third_address.address : ''), (contact.third_address != '' ? contact.third_address.city : ''), (contact.third_address != '' ? contact.third_address.state : ''), (contact.third_address != '' ? contact.third_address.zip : ''), (contact.third_address != '' ? contact.third_address.country : ''),
                  contact.first_phone, contact.second_phone, contact.first_fax, '', contact.third_phone, '', '',
                  contact.first_email, contact.second_email, contact.third_email,
                  '','','',
                  contact.first_im_kind, contact.first_im, contact.second_im_kind, contact.second_im, contact.third_im_kind, contact.third_im ]
        else
          row = [ contact[:id], 'Company', contact[:name], '', '', contact[:company_name], '', contact[:about], contact.tag_list,
                  (contact.first_address != '' ? contact.first_address.address : ''), (contact.first_address != '' ? contact.first_address.city : ''), (contact.first_address != '' ? contact.first_address.state : ''), (contact.first_address != '' ? contact.first_address.zip : ''), (contact.first_address != '' ? contact.first_address.country : ''),
                  (contact.second_address != '' ? contact.second_address.address : ''), (contact.second_address != '' ? contact.second_address.city : ''), (contact.second_address != '' ? contact.second_address.state : ''), (contact.second_address != '' ? contact.second_address.zip : ''), (contact.second_address != '' ? contact.second_address.country : ''),
                  (contact.third_address != '' ? contact.third_address.address : ''), (contact.third_address != '' ? contact.third_address.city : ''), (contact.third_address != '' ? contact.third_address.state : ''), (contact.third_address != '' ? contact.third_address.zip : ''), (contact.third_address != '' ? contact.third_address.country : ''),
                  contact.first_phone, contact.second_phone, contact.first_fax, '', contact.third_phone, '', '',
                  contact.first_email, contact.second_email, contact.third_email,
                  '','','',
                  contact.first_im_kind, contact.first_im, contact.second_im_kind, contact.second_im, contact.third_im_kind, contact.third_im ]
        end
        csv << row
      end
      
    end
    export
  end
end
