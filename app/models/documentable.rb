class Documentable < ActiveRecord::Base
  attr_accessible :documentable_id, :documentable_type, :document_id
  belongs_to :documentable, :polymorphic => true
  belongs_to :document
end
