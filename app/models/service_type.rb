class ServiceType < ActiveRecord::Base
  attr_accessible :account_ref, :desc, :contact_id, :prefered_contact_id, :default_template_id, :fallback_template_id

  has_many :services
  has_many :contacts_service_types
  #has_many :contacts, :through => :contacts_service_type
  #belongs_to :contacts_service_type  
  has_many :jobs, :through => :jobs_service_types
  has_many :jobs_service_types
  acts_as_tenant
  #has_and_belongs_to_many :contacts
  validates_presence_of :account_ref, :desc
  validates_uniqueness_of :account_ref, :scope => :tenant_id
  scope :search, proc { |search| where("service_types.desc LIKE :search", search: "%#{search}%") }
  
  belongs_to :default_template, :class_name=>'Template', :foreign_key=>:default_template_id
  belongs_to :prefered_contact, :class_name=>'Contact', :foreign_key =>'prefered_contact_id'
  belongs_to :fallback_template, :class_name=>'Template', :foreign_key=>:fallback_template_id

  def has_prefered_contact
    if self.prefered_contact != nil then
      true
    else
      false
    end
  end

  def get_prefered_contact_name
    if has_prefered_contact then
      self.prefered_contact[:name]
    else
      'No Prefered Contact'
    end
  end

  #scopes
  module Scopes
    def by_name(prefix)
      where('`desc` LIKE :prefix', prefix: "#{prefix}%")
    end
  end

  extend Scopes
end
