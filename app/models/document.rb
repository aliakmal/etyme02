class Document < ActiveRecord::Base
  attr_accessible :desc, :expiry_date, :filename, :filesize, :issue_date, :title, :file, :documentables_attributes
  has_many :documentables
  #serialize :headers, :format=>:yaml
  accepts_nested_attributes_for :documentables
  mount_uploader :file, FileUploader
  acts_as_tenant
  def attach(dType, dId)
    self.documentables.create(:documentable_type => dType, :documentable_id => dId)
  end

  def is_expired?
    !self.expiry_date.blank? && (self.expiry_date < Date.today)
  end

  def close_to_expiry?
    !self.expiry_date.blank? && ((self.expiry_date - Date.today).to_i < 7) && ((self.expiry_date - Date.today).to_i > 0)
  end

  scope :not_by_documentable, lambda{ |documentable| 
  where("`documents`.`id` NOT IN 
      (select `documentables`.`document_id` from `documentables` where `documentables`.`documentable_type`= ? AND `documentables`.`documentable_id` = ?)", 
      documentable.documentable_type, documentable.documentable_id)
  }

end
