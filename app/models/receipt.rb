class Receipt < ActiveRecord::Base
  acts_as_tenant
  belongs_to :contract
  attr_accessible :profit, :rate, :tenant_id, :total_salary, :total_time, :contract_id, :expenses, :salary_pending, :payment_amount, :non_contract_expenses, :status
  validates_presence_of :expenses, :payment_amount, :non_contract_expenses
end
