class JobsServiceType < ActiveRecord::Base
  attr_accessible :service_type_id, :job_id
  belongs_to :service_type
  belongs_to :job
end
