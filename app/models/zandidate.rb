=begin
class Zandidate #ActiveRecord::Base
  attr_accessible :best_time_to_call, :can_relocate, :job_id,  :current_employer, :current_pay
  attr_accessible :cv, :date_available, :designation, :desired_pay, :first_name, :last_name, :website
  has_many :contact_details, :as => :contactable
  has_many :phone, :as => :contactable
  has_many :fax, :as => :contactable
  has_many :email, :as => :contactable
  has_many :im, :as => :contactable
  has_many :addresses, :as => :contactable
  acts_as_citier
  belongs_to :user
  has_many :inquiries, :foreign_key => "candidates_id", :class_name=>'Inquiry'
  has_many :jobs, :through =>:inquiries

  accepts_nested_attributes_for :phone, :fax, :email, :addresses, :im
  accepts_nested_attributes_for :contact_details, :inquiries
  attr_accessible :contact_details_attributes, :phone_attributes, :fax_attributes, :email_attributes, :im_attributes
  attr_accessible :address_detail_attributes, :addresses_attributes, :inquiries_attributes


  scope :starts_with, proc { |letter| where("first_name LIKE :letter", letter: "#{letter}%") }
  scope :by_name, proc { |search| where("first_name LIKE :search", search: "#{search}%") }
    
  def full_name
    self[:first_name] +' ' + self[:last_name]
  end

  def has_job?
    self.job_id.to_i != 0
  end

  def first_phone
    if self.phone.count > 0 then
      self.phone.first.details
    else
      ''
    end
  end
  
  def second_phone
    if self.phone.count > 1 then
      self.phone[1].details
    else
      ''
    end
  end
  
  def third_phone
    if self.phone.count > 2 then
      self.phone[2].details
    else
      ''
    end
  end

  def first_fax
    if self.fax.count > 0 then
      self.fax.first.details
    else
      ''
    end
  end

  def first_email
    if self.email.count > 0 then
      self.email.first.details
    else
      ''
    end
  end
  
  def second_email
    if self.email.count > 1 then
      self.email[1].details
    else
      ''
    end
  end
  
  def third_email
    if self.email.count > 2 then
      self.email[2].details
    else
      ''
    end
  end

  def first_address
    if self.addresses.count > 0 then
      address = self.addresses.first
    else
      ''
    end
  end
  
  def second_address
    if self.addresses.count > 1 then
      address = self.addresses[1]
    else
      ''
    end
  end

  def third_address
    if self.addresses.count > 2 then
      address = self.addresses[2]
    else
      ''
    end
  end
  
  def first_im
    if self.im.count > 0 then
      self.im.first.details
    else
      ''
    end
  end
  
  def second_im
    if self.im.count > 1 then
      self.im[1].details
    else
      ''
    end
  end
  
  def third_im
    if self.im.count > 2 then
      self.im[2].details
    else
      ''
    end
  end
  def first_im_kind
    if self.im.count > 0 then
      self.im.first.location
    else
      ''
    end
  end
  
  def second_im_kind
    if self.im.count > 1 then
      self.im[1].location
    else
      ''
    end
  end
  
  def third_im_kind
    if self.im.count > 2 then
      self.im[2].location
    else
      ''
    end
  end

  def self.to_csv(contacts)
    columns = ['ID','Kind','Name','First name','Last name','Company','Title','Background','Tags',
               'Address - Work Street','Address - Work City','Address - Work State','Address - Work Zip','Address - Work Country',
               'Address - Home Street','Address - Home City','Address - Home State','Address - Home Zip','Address - Home Country',
               'Address - Other Street','Address - Other City','Address - Other State','Address - Other Zip','Address - Other Country',
               'Phone number - Work','Phone number - Mobile','Phone number - Fax','Phone number - Pager','Phone number - Home','Phone number - Skype','Phone number - Other',
               'Email address - Work','Email address - Home','Email address - Other',
               'Web address - Work','Web address - Personal','Web address - Other',
               'Instant messenger kind - Work','Instant messenger - Work','Instant messenger kind - Personal','Instant messenger - Personal','Instant messenger kind - Other','Instant messenger - Other']
    
    export = CSV.generate do |csv|
      csv << columns
      
      contacts.each do |contact|
        
          row = [ contact[:id], 'Candidate', contact.full_name, contact[:first_name],contact[:last_name], contact.current_employer,contact[:designation],'', '',
                  (contact.first_address != '' ? contact.first_address.address : ''), (contact.first_address != '' ? contact.first_address.city : ''), (contact.first_address != '' ? contact.first_address.state : ''), (contact.first_address != '' ? contact.first_address.zip : ''), (contact.first_address != '' ? contact.first_address.country : ''),
                  (contact.second_address != '' ? contact.second_address.address : ''), (contact.second_address != '' ? contact.second_address.city : ''), (contact.second_address != '' ? contact.second_address.state : ''), (contact.second_address != '' ? contact.second_address.zip : ''), (contact.second_address != '' ? contact.second_address.country : ''),
                  (contact.third_address != '' ? contact.third_address.address : ''), (contact.third_address != '' ? contact.third_address.city : ''), (contact.third_address != '' ? contact.third_address.state : ''), (contact.third_address != '' ? contact.third_address.zip : ''), (contact.third_address != '' ? contact.third_address.country : ''),
                  contact.first_phone, contact.second_phone, contact.first_fax, '', contact.third_phone, '', '',
                  contact.first_email, contact.second_email, contact.third_email,
                  '','','',
                  contact.first_im_kind, contact.first_im, contact.second_im_kind, contact.second_im, contact.third_im_kind, contact.third_im ]
        csv << row
      end
      
    end
    export
  end
end
=end
