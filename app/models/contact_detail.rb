class ContactDetail < ActiveRecord::Base
  belongs_to :contactable, :polymorphic => true
  attr_accessible :contact_id, :type, :details, :location
  acts_as_tenant
end
