class Company < Contact
  acts_as_citier
  acts_as_tenant
  acts_as_commentable
  #attr_accessor :company_name, :description, :timezone, :website, :twenty_four_ops, :type, :company_type
  #attr_accessor :contact_details_attributes, :phone_attributes, :fax_attributes, :email_attributes, :im_attributes
  #attr_accessor :address_detail_attributes, :typeb_attributes, :aftn_attributes, :addresses_attributes, :radio_attributes, :icao_attributes
  attr_accessible :company_name, :description, :timezone, :website, :twenty_four_ops, :type, :company_type
  attr_accessible :contact_details_attributes, :phone_attributes, :fax_attributes, :email_attributes, :im_attributes
  attr_accessible :address_detail_attributes, :addresses_attributes
  attr_accessible :phone, :fax, :email
  attr_accessible :tags_as_text, :tag_list
  attr_taggable :tags

  #liquid_methods :company_name, :addresses
  
  #virtual attributes
  def tag_list=(tag_list)
    @tag_list = tag_list
    self.tags = @tag_list.split(',')
  end

  def tag_list
    @tag_list
  end

  def tags_as_text=(tags_as_text)
    @tags_as_text = tags_as_text
  end

  def tags_as_text
    @tags_as_text
  end

  def has_individuals?
    self.individuals.count>0
  end

  def num_individuals
    self.individuals.count
  end

  before_save :set_parent_attributes, :reset_null_to_empty
  #associations

  has_many :individuals
  has_many :jobs, :dependent => :destroy


  validates_presence_of :company_name, :description, :company_type
  validates_uniqueness_of :company_name, :scope => :tenant_id

  
  def reset_null_to_empty
    if self[:description] == nil then
      self[:description] = ""
    end
  end

  def set_parent_attributes
    self[:name] = self[:company_name]
    self[:about] = self[:description]
  end

end