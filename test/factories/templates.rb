# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :template do
    title "MyString"
    details "MyText"
    service_type_id 1
    contact_id 1
  end
end
